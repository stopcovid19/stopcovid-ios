// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  MaintenanceInfo.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 20/05/2020 - for the TousAntiCovid project.
//


import UIKit
import UseCases

struct MaintenanceInfo: Decodable {

    enum Mode: String, Decodable {
        case upgrade
        case maintenance
    }
    
    let isActive: Bool?
    let mode: Mode?
    let appAvailability: Bool?
    let minInfoBuild: Int?
    let smileysToPop: [String]?
    var upgradeMandatory: Bool {
        let currentBuildNumber: Int = Int(UIApplication.shared.buildNumber) ?? 0
        guard let minForcedBuildNumber else { return false }
        return minForcedBuildNumber > currentBuildNumber
    }

    var localizedTitle: String? { localizedValue(from: title) }
    var localizedMessage: String? { localizedValue(from: message) }
    var localizedButtonTitle: String? { localizedValue(from: buttonTitle) }
    var localizedButtonUrl: String? { localizedValue(from: buttonURL) }

    private let minRequiredBuildNumber: Int?
    private let minForcedBuildNumber: Int?
    private let title: [String: String]?
    private let message: [String: String]?
    private let buttonTitle: [String: String]?
    private let buttonURL: [String: String]?

    // TODO: Use Storage Data directly with the new architecture
    private var defaultAppLanguageCode: String { UseCases.defaultAppLanguage.rawValue }

    func shouldShow() -> Bool {
        let currentBuildNumber: Int = Int(UIApplication.shared.buildNumber) ?? 0
        return (isActive ?? false) && max(minRequiredBuildNumber ?? 0, minForcedBuildNumber ?? 0) > currentBuildNumber  || appAvailability == false
    }

    private func localizedValue(from: [String: String]?) -> String? {
        guard let from = from else { return nil }
        if let description = from[Locale.currentAppLanguageCode], !description.isEmpty {
            return description
        }

        return from[defaultAppLanguageCode]
    }
    
}
