// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  KeyFigureSeriesItem.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 20/01/2021 - for the TousAntiCovid project.
//

import Foundation

struct KeyFigureSeriesItem: Codable {
    
    let date: Double
    let value: Double
    let opacity: Double
    
    var normalizedOpacity: Double {
        opacity * 0.9 + 10
    }
    
}

extension KeyFigureSeriesItem: Hashable {
    func hash(into hasher: inout Hasher) {
        hasher.combine(date)
        hasher.combine(value)
        hasher.combine(opacity)
    }
    
    static func == (lhs: KeyFigureSeriesItem, rhs: KeyFigureSeriesItem) -> Bool {
        lhs.date == rhs.date && lhs.value == rhs.value && lhs.opacity == rhs.opacity
    }
}
