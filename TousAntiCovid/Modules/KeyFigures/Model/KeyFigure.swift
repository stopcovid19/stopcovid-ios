// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  KeyFigure.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 21/10/2020 - for the TousAntiCovid project.
//

import UIKit

struct KeyFigure: Codable {
    
    enum Category: String, Codable, CaseIterable {
        case vaccine
        case health
        case app
        case uncategorized
        
        var order: Int {
            switch self {
            case .vaccine: return 0
            case .health: return 1
            case .app: return 2
            case .uncategorized: return 3
            }
        }
    }
    
    enum ChartKind: String, Codable {
        case bars
        case line
    }

    enum FigType: String, Codable {
        case float = "FLOAT"
        case perck = "PERCK"
        case int = "INT"
        case perc = "PERC"
        case r = "R"
    }
    
    let labelKey: String
    let category: Category
    let valueGlobalToDisplay: String
    let valueGlobal: Double?
    let isFeatured: Bool
    let isHighlighted: Bool?
    let extractDate: Int
    let valuesDepartments: [KeyFigureDepartment]?
    let displayOnSameChart: Bool
    let avgSeries: [KeyFigureSeriesItem]?
    let limitLine: Double?
    let magnitude: UInt32
    let series: [KeyFigureSeriesItem]?
    let chartType: ChartKind?
    let figType: FigType?
    
    var showComparison: Bool {
        switch figType {
        case .float, .int, .perck, .perc, .r:
            return true
        case .some, .none:
            return false
        }
    }

    lazy var comparisonDescription: String? = {
        switch figType {
        case .float, .int, .perck:
            guard let dayMinusSevenValue else { return nil }
            return String(format: "keyFigureDetailController.section.comparison.description.float".localized,  evolutionInPercentCompare(to: dayMinusSevenValue.value).orEmpty, (valueGlobal?.toString(shortUnit: shortUnit)?.formattingValueWithThousandsSeparatorIfPossible()).orEmpty)
        case .perc:
            guard let dayMinusSevenValue else { return nil }
            guard let valueGlobal else { return nil }
            let evolutionValue: Double = valueGlobal - dayMinusSevenValue.value
            let result: String = evolutionValue.toString(shortUnit: nil)?.formattingValueWithThousandsSeparatorIfPossible() ?? String(evolutionValue)

            let evolutionValueFormatted: String =  evolutionValue > 0 ? "+\(result)": result
            let isSingular: Bool = abs(evolutionValue) < 2

            let localizedString: String = isSingular ? "keyFigureDetailController.section.comparison.description.percentage.singular".localized : "keyFigureDetailController.section.comparison.description.percentage".localized
            return String(format: localizedString, evolutionInPercentCompare(to: dayMinusSevenValue.value).orEmpty, evolutionValueFormatted, (valueGlobal.toString(shortUnit: shortUnit)?.formattingValueWithThousandsSeparatorIfPossible()).orEmpty)
        case .some,  .none:
            return nil
        }
    }()

    lazy var minSeriesValue: Double? = { series?.min(by: { $0.value < $1.value })?.value }()
    lazy var minSeries: [KeyFigureSeriesItem] = { series?.filter { $0.value == minSeriesValue } ?? [] }()

    lazy var maxSeriesValue: Double? = { series?.max(by: { $0.value < $1.value })?.value }()
    lazy var maxSeries: [KeyFigureSeriesItem] = { series?.filter { $0.value == maxSeriesValue } ?? [] }()

    lazy var dayMinusSevenValue: KeyFigureSeriesItem? = {
        guard let maxDate = series?.max(\.date)?.date else { return nil }

        let dayMinusSevenDate: Double = Double(maxDate - (3600 * 24 * 7))
        return series?.first(where: { $0.date == dayMinusSevenDate })
    }()

    var isLabelReady: Bool { "\(labelKey).label".localizedOrNil != nil }
    var label: String { "\(labelKey).label".localized.trimmingCharacters(in: .whitespaces) }
    var shortUnit: String? { "\(labelKey).shortUnit".localizedOrNil?.trimmingCharacters(in: .whitespaces) }
    var longUnit: String? { "\(labelKey).longUnit".localizedOrNil?.trimmingCharacters(in: .whitespaces) }
    var labelWithLongUnit: String {
        if let unit = longUnit {
            return shortLabel + " (\(unit))"
        } else {
            return shortLabel
        }
    }
    var lastChartValue: Double? { series?.last?.value }
    var shortLabel: String { "\(labelKey).shortLabel".localized.trimmingCharacters(in: .whitespaces) }
    var description: String { "\(labelKey).description".localized.trimmingCharacters(in: .whitespaces) }
    var learnMore: String { "\(labelKey).learnMore".localizedOrEmpty.trimmingCharacters(in: .whitespaces) }
    var limitLineLabel: String { "\(labelKey).limitLine".localizedOrEmpty.trimmingCharacters(in: .whitespaces) }
    var color: UIColor { UIColor.isDarkMode ? darkColor : lightColor }
    var lightColor: UIColor { UIColor(hexString: "\(labelKey).colorCode.light".localized) }
    private var darkColor: UIColor { UIColor(hexString: "\(labelKey).colorCode.dark".localized ) }
    var ascendingSeries: [KeyFigureSeriesItem]? { series?.sorted { $0.date < $1.date } }
    var chartKind: ChartKind { chartType ?? .line }
    
    var formattedDate: String {
        switch category {
        case .vaccine, .health:
            return Date(timeIntervalSince1970: Double(extractDate)).relativelyFormattedDay(prefixStringKey: "keyFigures.update", todayPrefixStringKey: "keyFigures.update.today", yesterdayPrefixStringKey: "keyFigures.update.today")
        case .app:
            return Date(timeIntervalSince1970: Double(extractDate)).relativelyFormatted(prefixStringKey: "keyFigures.update", todayPrefixStringKey: "keyFigures.update.today", yesterdayPrefixStringKey: "keyFigures.update.today")
        default:
            return ""
        }
    }
    
    var currentDepartmentSpecificKeyFigure: KeyFigureDepartment? {
        guard KeyFiguresManager.shared.displayDepartmentLevel else { return nil }
        return valuesDepartments?.first
    }

    private func evolutionInPercentCompare(to value: Double) -> String? {
        guard let valueGlobal else { return nil }
        let evolutionValue: Double = ((valueGlobal - value) / value * 100)
        let result: String = evolutionValue.toString(isPercent: true, digits: 1)?.formattingValueWithThousandsSeparatorIfPossible() ?? String(evolutionValue)
        return evolutionValue > 0 ? "+\(result)": result
    }
}

extension KeyFigure: Hashable {
    func hash(into hasher: inout Hasher) {
        hasher.combine(labelKey)
        hasher.combine(category)
        hasher.combine(valueGlobal)
        hasher.combine(valueGlobalToDisplay)
        hasher.combine(isFeatured)
        hasher.combine(isHighlighted)
        hasher.combine(extractDate)
        hasher.combine(valuesDepartments)
        hasher.combine(displayOnSameChart)
        hasher.combine(avgSeries)
        hasher.combine(limitLine)
        hasher.combine(magnitude)
        hasher.combine(series)
    }
    
    static func == (lhs: KeyFigure, rhs: KeyFigure) -> Bool {
        lhs.labelKey == rhs.labelKey
    }
}

extension Array where Element == KeyFigure {
    var haveSameMagnitude: Bool {
        Dictionary(grouping: self) { keyFigure in
            keyFigure.magnitude
        }.keys.count == 1
    }
}
