// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  KeyFigureMapDepartment.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 18/01/2023 - for the TousAntiCovid project.
//

import Foundation

struct KeyFigureMapDepartment: Codable {
    let number: String
    let name: String
    let serieItems: [KeyFigureSeriesItem]?
    
    enum CodingKeys: String, CodingKey {
        case number = "dptNb"
        case name = "dptLabel"
        case serieItems = "series"
    }
}
