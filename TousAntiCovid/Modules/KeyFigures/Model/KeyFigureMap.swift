// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  KeyFigureMap.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 18/01/2023 - for the TousAntiCovid project.
//

import Foundation

struct KeyFigureMap: Codable {
    let labelKey: String
    let valuesDepartments: [KeyFigureMapDepartment]?
}
