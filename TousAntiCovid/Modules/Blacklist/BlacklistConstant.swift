// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  BlacklistConstant.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 12/10/2022 - for the TousAntiCovid project.
//

enum BlacklistConstant {
    static let version: BlacklistManager.Version = .v5
}
