// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  BlacklistV5Constant.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 12/10/2022 - for the TousAntiCovid project.
//

import ServerSDK

enum BlacklistV5Constant {
    
    enum SupportedFileType: String {
        case pbgz = "pb.gz"
        case pb = "pb"
        case json = "json"
    }
    
    static let fileType: SupportedFileType = SupportedFileType.pb
    
    static var blacklistFreqInSec: Double { ParametersManager.shared.blacklistFreqInSec }
    private static let directoryName: String = "CertList"
    
    static let baseUrl: String = "https://\(Constant.Server.staticResourcesRootDomain)/json/blacklist/v5/\(directoryName)/prefix"
    static let indexesFilename: String = "index.\(fileType.rawValue)"
    
    static let hashesLength: Int = 32
}
