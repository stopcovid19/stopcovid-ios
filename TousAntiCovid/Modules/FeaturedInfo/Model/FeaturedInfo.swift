// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  FeaturedInfo.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 06/01/2023 - for the TousAntiCovid project.
//

struct FeaturedInfo: Codable {
    let id: String
    let title: String
    let thumbnail: String
    let hd: String
    let hint: String
    let url: String?
}
