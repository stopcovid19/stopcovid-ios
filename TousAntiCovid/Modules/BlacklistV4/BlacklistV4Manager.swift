// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  BlacklistV4Manager.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 01/03/2022 - for the TousAntiCovid project.
//

final class BlacklistV4Manager: BlacklistManager {
    static let shared: BlacklistV4Manager = .init(baseUrl: BlacklistV4Constant.baseUrl, indexesFilename: BlacklistV4Constant.indexesFilename)
}
