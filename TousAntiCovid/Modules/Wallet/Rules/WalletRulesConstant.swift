// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  WalletRulesConstant.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 04/02/2022 - for the TousAntiCovid project.
//

import Foundation
import ServerSDK

enum WalletRulesConstant {
    enum WalletRulesType: String {
        case validity
        case eligibility
        
        var remoteUrl: URL? {
            switch self {
            case .validity:
                return URL(string: "https://\(Constant.Server.staticResourcesRootDomain)/json/smartwallet/v2/\(rawValue).json")
                
            case .eligibility:
                return URL(string: "https://\(Constant.Server.staticResourcesRootDomain)/json/smartwallet/v2/\(rawValue).json")
                
            }
        }
    }
    
    static let workingDirectoryUrl: URL = FileManager.libraryDirectory().appendingPathComponent("Rules")
    
    static let minDurationBetweenUpdatesInSeconds: Double = ParametersManager.shared.minFilesRefreshInterval
}
