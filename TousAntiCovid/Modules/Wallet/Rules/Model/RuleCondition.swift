// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  RuleCondition.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 04/02/2022 - for the TousAntiCovid project.
//

import Foundation
import ServerSDK

struct RuleCondition: Decodable {
    
    var doses: VaccineDoses?
    private var products: [String]?
    var eligibility: Int?
    var prefix: [String]?
    var validity: Validity?
    var labelId: String?
    
    enum CodingKeys: String, CodingKey {
        case doses
        case products
        case eligibility = "elg"
        case prefix
        case validity = "valid"
        case labelId
    }
    
    var eligibilityInSec: Double? {
        guard let eligibility = eligibility else { return nil }
        return Double(eligibility * 24 * 3600)
    }
    
    func vaccineTypes(from config: Vaccins?) -> [String] {
        var rawCodes: [String] = products?.filter { $0.first != "#" } ?? []
        let familyCodes: [String]? = products?.filter { $0.first == "#" }
        let vaccineTypes: [WalletConstant.VaccineType]? = familyCodes?.compactMap { WalletConstant.VaccineType(familyCode: $0) }
        let vaccineCodes: [String] = vaccineTypes?.compactMap { $0.stringValues(from: config) }.flatMap { $0 } ?? []
        rawCodes.append(contentsOf: vaccineCodes)
        return rawCodes
    }

}

struct VaccineDoses: Decodable {
    var current: Int
    var target: Int
    
    enum CodingKeys: String, CodingKey {
        case current = "c"
        case target = "t"
    }
}

struct Validity: Decodable {
    var startInDays: Int?
    var endInDays: Int?
    
    enum CodingKeys: String, CodingKey {
        case startInDays = "s"
        case endInDays = "e"
    }
    
    var endInSec: Double? {
        guard let endInDays = endInDays else { return nil }
        return Double(endInDays * 24 * 3600)
    }
    
    var startInSec: Double? {
        guard let startInDays = startInDays else { return nil }
        return Double(startInDays * 24 * 3600)
    }
}

// MARK: - extension on array of RuleConditions
extension Array where Element == RuleCondition {
    func firstVerified(certificateType: WalletConstant.CertificateType,
                       certificateMedicalProductCode: String?,
                       certificateCurrentDosesNumber: Int?,
                       certificateTargetDosesNumber: Int?,
                       certificatePrefix: String?,
                       certificateIsTestNegative: Bool?,
                       vaccinsConfig: Vaccins?) -> RuleCondition? {
        switch certificateType {
        case .vaccinationEurope:
            guard let vaccineType = certificateMedicalProductCode else { return nil }
            guard let currentDose = certificateCurrentDosesNumber else { return nil }
            guard let targetDose = certificateTargetDosesNumber else { return nil }
            return first {
                if let doses = $0.doses {
                    return $0.vaccineTypes(from: vaccinsConfig).contains(vaccineType) &&
                    doses.current == currentDose &&
                    doses.target == targetDose
                } else {
                    return $0.vaccineTypes(from: vaccinsConfig).contains(vaccineType)
                }
            }
        case .recoveryEurope:
            return prefixMatchingElement(certificatePrefix) ?? first(where: { $0.prefix == nil })
        case .sanitaryEurope:
            guard certificateIsTestNegative == false else { return nil }
            return prefixMatchingElement(certificatePrefix) ?? first(where: { $0.prefix == nil })
        default:
            return nil
        }
    }
    
    private func prefixMatchingElement(_ certificatePrefix: String?) -> Element? {
        first { $0.prefix?.first { certificatePrefix?.starts(with: $0) ?? false } != nil }
    }
}

// MARK: - Utils
private extension WalletConstant.VaccineType {
    var familyCode: String {
        switch self {
        case .arnm: return "#AR"
        case .astraZeneca: return "#AZ"
        case .janssen: return "#JA"
        }
    }
    
    init?(familyCode: String) {
        switch familyCode {
        case WalletConstant.VaccineType.arnm.familyCode: self = .arnm
        case WalletConstant.VaccineType.astraZeneca.familyCode: self = .astraZeneca
        case WalletConstant.VaccineType.janssen.familyCode: self = .janssen
        default: return nil
        }
    }
}


