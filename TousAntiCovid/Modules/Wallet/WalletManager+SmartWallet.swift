// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  WalletManager+SmartWallet.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 29/11/2021 - for the TousAntiCovid project.
//

import ServerSDK

// MARK: - ELG and EXP management
extension WalletManager {
    enum WalletState: Int {
        case normal
        case eligibleSoon // At least one certificate associated to a user will be eligible soon
        case eligible // At least one certificate associated to a user is eligible to vaccination
        case expiredSoon // At least one certificate associated to a user will expire soon
        case expired // At least one certificate associated to a user is expired
        
        var severity: Int { rawValue }
    }
    
    var shouldUseSmartWallet: Bool { ParametersManager.shared.smartWalletFeatureActivated && smartWalletActivated }
    var shouldUseSmartWalletNotifications: Bool { shouldUseSmartWallet && ParametersManager.shared.smartWalletNotificationsActivated }
    var smartWalletEngine: SmartWalletEngine { ParametersManager.shared.smartWalletEngine }
    
    var walletSmartState: WalletState {
        var states: [WalletState] = [.normal]
        lastRelevantCertificates?.forEach { certificate in
            if isPassExpired(for: certificate) {
                states.append(.expired)
            } else if isPassExpiredSoon(for: certificate) {
                states.append(.expiredSoon)
            } else if isEligibleToVaccination(for: certificate) {
                states.append(.eligible)
            } else if isEligibleToVaccinationSoon(for: certificate) {
                states.append(.eligibleSoon)
            }
        }
        return Set<WalletState>(states).sorted(by: { $0.severity > $1.severity })[0] // Array has at least one element (.normal)
    }
    
    func validity(_ certificate: EuropeanCertificate?, today: Date = Date()) -> ValidityInfo? {
        guard let certificate = certificate else { return nil }
        // Exception for exemption
        if certificate.type == .exemptionEurope {
            return ValidityInfo(startTimestamp: certificate.hCert.exemptionStatement?.validFrom.timeIntervalSince1970, endTimestamp: certificate.hCert.exemptionStatement?.validUntil.timeIntervalSince1970)
        }
        // If type certificate, only applicable on complete certificates
        if certificate.type == .vaccinationEurope && certificate.isLastDose == false { return nil }
        return validity(now: today,
                        certificateType: certificate.type,
                        certificateAlignedTimestamp: certificate.alignedTimestamp,
                        certificateAlignedDateOfBirth: Date(timeIntervalSince1970: certificate.alignedBirthdate),
                        certificateMedicalProductCode: certificate.medicalProductCode,
                        certificateCurrentDosesNumber: certificate.dosesNumber,
                        certificateTargetDosesNumber: certificate.dosesTotal,
                        certificatePrefix: certificate.hCertPrefix,
                        certificateIsTestNegative: certificate.isNegativeTest)
    }
    
    func expiryTimestamp(_ certificate: EuropeanCertificate?, today: Date = Date()) -> Double? {
        return validity(certificate, today: today)?.endTimestamp
    }
    
    func eligibility(_ certificate: EuropeanCertificate?, today: Date = Date()) -> EligibilityInfo? {
        guard let certificate = certificate else { return nil }
        // If type certificate, only applicable on complete certificates
        if certificate.type == .vaccinationEurope && certificate.isLastDose == false { return nil }
        return eligibility(now: today,
                           certificateType: certificate.type,
                           certificateAlignedTimestamp: certificate.alignedTimestamp,
                           certificateAlignedDateOfBirth: Date(timeIntervalSince1970: certificate.alignedBirthdate),
                           certificateMedicalProductCode: certificate.medicalProductCode,
                           certificateCurrentDosesNumber: certificate.dosesNumber,
                           certificateTargetDosesNumber: certificate.dosesTotal,
                           certificatePrefix: certificate.hCertPrefix,
                           certificateIsTestNegative: certificate.isNegativeTest)
    }
    
    func isRelevantCertificate(_ certificate: EuropeanCertificate) -> Bool {
        lastRelevantCertificates?.contains(certificate) == true
    }

    func isPassExpired(for certificate: EuropeanCertificate?) -> Bool {
        passExpirationTimestamp(for: certificate) != nil
    }
    
    func isPassExpiredSoon(for certificate: EuropeanCertificate) -> Bool {
        passExpirationSoonTimestamp(for: certificate) != nil
    }
    
    func eligibleSoonDescription(for certificate: EuropeanCertificate) -> String? {
        guard let info = passEligibleSoon(for: certificate), let timestamp = info.eligibilityTimestamp else { return nil }
        let date: Date = Date(timeIntervalSince1970: timestamp)
        let prefixKey: String = "smartWallet.eligibility.soon.info"
        let labelIdKey: String? = info.eligibilityLabelId == nil ? nil : prefixKey + "." + info.eligibilityLabelId!
        let name: String = certificate.firstname?.capitalized ?? certificate.formattedName
        return description(for: localizationKey(for: prefixKey, and: certificate), firstname: name, date: date, labelIdLocalizationKey: labelIdKey)
        
    }
    
    func eligibilityDescription(for certificate: EuropeanCertificate) -> String? {
        guard let info = passEligibility(for: certificate), let timestamp = info.eligibilityTimestamp else { return nil }
        let date: Date = Date(timeIntervalSince1970: timestamp)
        let prefixKey: String = "smartWallet.eligibility.info"
        let labelIdKey: String? = info.eligibilityLabelId == nil ? nil : prefixKey + "." + info.eligibilityLabelId!
        let name: String = certificate.firstname?.capitalized ?? certificate.formattedName
        return description(for: localizationKey(for: prefixKey, and: certificate), firstname: name, date: date, labelIdLocalizationKey: labelIdKey)
        
    }
    
    func expirationSoonDescription(for certificate: EuropeanCertificate) -> String? {
        guard let timestamp = passExpirationSoonTimestamp(for: certificate) else { return nil }
        let date: Date = Date(timeIntervalSince1970: timestamp)
        return description(for: localizationKey(for: "smartWallet.expiration.soon.warning", and: certificate), firstname: nil, date: date, labelIdLocalizationKey: nil)
        
    }
    
    func expirationDescription(for certificate: EuropeanCertificate) -> String? {
        guard let timestamp = passExpirationTimestamp(for: certificate) else { return nil }
        let date: Date = Date(timeIntervalSince1970: timestamp)
        return description(for: localizationKey(for: "smartWallet.expiration.error", and: certificate), firstname: nil, date: date, labelIdLocalizationKey: nil)
        
    }
}

extension WalletManager {
    func eligibility(now: Date,
                     certificateType: WalletConstant.CertificateType,
                     certificateAlignedTimestamp: Double,
                     certificateAlignedDateOfBirth: Date,
                     certificateMedicalProductCode: String?,
                     certificateCurrentDosesNumber: Int?,
                     certificateTargetDosesNumber: Int?,
                     certificatePrefix: String?,
                     certificateIsTestNegative: Bool?,
                     vaccinsConfig: Vaccins? = ParametersManager.shared.vaccinTypes) -> EligibilityInfo? {
        guard let eligibilityInfo: EligibilityInfo = WalletElgRulesManager.shared.eligibility(now: now,
                                                                                              certificateType: certificateType,
                                                                                              certificateAlignedTimestamp: certificateAlignedTimestamp,
                                                                                              certificateAlignedDateOfBirth: certificateAlignedDateOfBirth,
                                                                                              certificateMedicalProductCode: certificateMedicalProductCode,
                                                                                              certificateCurrentDosesNumber: certificateCurrentDosesNumber,
                                                                                              certificateTargetDosesNumber: certificateTargetDosesNumber,
                                                                                              certificatePrefix: certificatePrefix,
                                                                                              certificateIsTestNegative: certificateIsTestNegative,
                                                                                              vaccinsConfig: vaccinsConfig) else { return nil }
        return EligibilityInfo(eligibilityTimestamp: eligibilityInfo.eligibilityTimestamp.map { Date(timeIntervalSince1970: $0).roundingToMidnightPastOne().timeIntervalSince1970 },
                               eligibilityLabelId: eligibilityInfo.eligibilityLabelId)
    }
    
    func validity(now: Date,
                  certificateType: WalletConstant.CertificateType,
                  certificateAlignedTimestamp: Double,
                  certificateAlignedDateOfBirth: Date,
                  certificateMedicalProductCode: String?,
                  certificateCurrentDosesNumber: Int?,
                  certificateTargetDosesNumber: Int?,
                  certificatePrefix: String?,
                  certificateIsTestNegative: Bool?,
                  vaccinsConfig: Vaccins? = ParametersManager.shared.vaccinTypes) -> ValidityInfo? {
        guard let validity: ValidityInfo = WalletValidityRulesManager.shared.validity(now: now,
                                                                                      certificateType: certificateType,
                                                                                      certificateAlignedTimestamp: certificateAlignedTimestamp,
                                                                                      certificateAlignedDateOfBirth: certificateAlignedDateOfBirth,
                                                                                      certificateMedicalProductCode: certificateMedicalProductCode,
                                                                                      certificateCurrentDosesNumber: certificateCurrentDosesNumber,
                                                                                      certificateTargetDosesNumber: certificateTargetDosesNumber,
                                                                                      certificatePrefix: certificatePrefix,
                                                                                      certificateIsTestNegative: certificateIsTestNegative,
                                                                                      vaccinsConfig: vaccinsConfig) else { return nil }
        return ValidityInfo(startTimestamp: validity.startTimestamp.map { Date(timeIntervalSince1970: $0).roundingToMidnightPastOne().timeIntervalSince1970 },
                            endTimestamp: validity.endTimestamp.map { Date(timeIntervalSince1970: $0).roundingToMidnightPastOne().timeIntervalSince1970 })
    }
}

// MARK: Smart notifications
extension WalletManager {
    private var notifyEligibilityDelayThreshold: Double {
        Double(ParametersManager.shared.smartWalletNotificationsElgDays * 24 * 3600)
        
    }
    private var notifyDelayThreshold: Double {
        12*3600
        
    } // 12 hours
    private var calculationDelayThreshold: Double {
        3600
        
    } // 12 hours
    private var today: Double { Date().roundingToMidnightPastOne().timeIntervalSince1970 }
    private var now: Double { Date().timeIntervalSince1970 }
    private var expiringSoonRanges: [Range<Int>] { [(-21 ..< -10), (-6 ..< -3), (-1 ..< 0)] }
    private var eligibleSoonRanges: [Range<Int>] { [(-15 ..< 0)] }
    
    var smartNotificationIdPrefix: String { "SmartWalletNotification" }
    
    enum NotificationType: String {
        case eligibility
        case eligible
        case expiry
    }
    
    func showSmartNotificationIfNeeded() {
        // Check last time we made the calculation. If to recent -> skip
        guard abs(smartWalletLastNotificationCalculationTimestamp - Date().timeIntervalSince1970) >= calculationDelayThreshold else { return }
        let todayTimestamp: Double = Date().roundingToMidnightPastOne().timeIntervalSince1970
        let expiringSoonCertificates: [(certificate: EuropeanCertificate, expiryTimestamp: Double, range: Range<Int>)] = lastRelevantCertificates?.compactMap {
            guard isPassExpiredSoon(for: $0) else { return nil }
            guard let expiryTimestamp = expiryTimestamp($0) else { return nil }
            let diffDays: Int = (todayTimestamp - expiryTimestamp).secondsToDays()
            guard let range = expiringSoonRanges.first(where: { $0.contains(diffDays) }) else { return nil }
            return ($0, expiryTimestamp, range)
        } ?? []
        
        if !expiringSoonCertificates.isEmpty {
            guard let expiringCertificate = expiringSoonCertificates.sorted(by: { $0.expiryTimestamp < $1.expiryTimestamp }).first else { return }
            let id: String = notificationId(type: .expiry, hash: expiringCertificate.certificate.uniqueHash, range: expiringCertificate.range)
            sendSmartNotificationIfNeeded(for: .expiry, with: id, args: [expiringCertificate.certificate.formattedName])
            // Update last calculation timestamp
            smartWalletLastNotificationCalculationTimestamp = Date().timeIntervalSince1970
            return
        }
        
        let eligibleSoonCertificates: [(certificate: EuropeanCertificate, eligibilityTimestamp: Double, range: Range<Int>)] = lastRelevantCertificates?.compactMap {
            guard isEligibleToVaccinationSoon(for: $0) else { return nil }
            guard let eligibilityTimestamp = eligibility($0)?.eligibilityTimestamp else { return nil }
            let diffDays: Int = (todayTimestamp - eligibilityTimestamp).secondsToDays()
            guard let range = eligibleSoonRanges.first(where: { $0.contains(diffDays) }) else { return nil }
            return ($0, eligibilityTimestamp, range)
        } ?? []
        
        if !eligibleSoonCertificates.isEmpty {
            guard let expiringCertificate = expiringSoonCertificates.sorted(by: { $0.expiryTimestamp < $1.expiryTimestamp }).first else { return }
            let id: String = notificationId(type: .expiry, hash: expiringCertificate.certificate.uniqueHash, range: expiringCertificate.range)
            sendSmartNotificationIfNeeded(for: .expiry, with: id, args: [expiringCertificate.certificate.formattedName])
            // Update last calculation timestamp
            smartWalletLastNotificationCalculationTimestamp = Date().timeIntervalSince1970
            return
        }
        
        // WARNING: Different management for this type of notification
        let eligibleCertificates: [(certificate: EuropeanCertificate, eligibilityTimestamp: Double)] = lastRelevantCertificates?.compactMap {
            guard isEligibleToVaccination(for: $0) else { return nil }
            guard let eligibilityTimestamp = eligibility($0)?.eligibilityTimestamp else { return nil }
            return ($0, eligibilityTimestamp)
        } ?? []
        if !eligibleCertificates.isEmpty {
            sendEligibilityNotificationsIfNeeded(for: eligibleCertificates.map { $0.certificate })
            // Update last calculation timestamp
            smartWalletLastNotificationCalculationTimestamp = Date().timeIntervalSince1970
        }
    }
}

// MARK: Smart notifications private functions
private extension WalletManager {
    // We have to save the id of the notification, to know, next time, if we already sent a notification for this certificate in this range period. We also save the timestamp of the last send, to repeat the operation only after notifDelayThreshold seconds
    func saveSentNotification(id: String) {
        smartWalletSentNotificationsIds.append(id)
        smartWalletLastNotificationTimestamp = Date().timeIntervalSince1970
    }
    
    func sendSmartNotificationIfNeeded(for type: NotificationType, with id: String, args: [String]) {
        // Check if we have already sent a notification less than notifDelayThreshold seconds ago
        guard abs(smartWalletLastNotificationTimestamp.distance(to: now)) >= notifyDelayThreshold else { return }
        // Check if we have already sent a notification for this certificate and range period
        guard !smartWalletSentNotificationsIds.contains(id) else { return }
        // trigger notification
        NotificationsManager.shared.triggerWalletSmartNotification(type: type, id: id, args: args) { [weak self] success in
            // if notification was delivered -> save id for next time comparison
            if success { self?.saveSentNotification(id: id) }
        }
    }
    
    func sendEligibilityNotificationsIfNeeded(for certificates: [EuropeanCertificate]) {
        updateEligibilityNotificationArray(with: Set<String>(certificates.map { $0.uniqueHash }))
        
        // get from stored data, ids of eligible certificates for which we have to send a notification, e.g the last notification was sent more than notifyEligibilityDelayThreshold ago, or this is the first notification
        let idsToSend: [String] = smartWalletEligibilitySentNotifications.filter {
            $0.value.distance(to: now) >= notifyEligibilityDelayThreshold
        }.map { $0.key }
        
        // For each id get certificate, create notification content, and trigger notification
        idsToSend.forEach { id in
            if let certificate = certificates.first(where: { $0.uniqueHash == id }) {
                let notificationId: String = notificationId(type: .eligible, hash: id, range: nil)
                NotificationsManager.shared.triggerWalletSmartNotification(type: .eligible, id: notificationId, args: [certificate.formattedName]) { [weak self] success in
                    // if success store notification send timestamp for this id
                    if success {
                        self?.smartWalletEligibilitySentNotifications[id] = Date().timeIntervalSince1970
                    }
                }
            }
        }
    }
    
    func updateEligibilityNotificationArray(with ids: Set<String>) {
        var tempSmartWalletEligibilitySentNotifications: [String : Double] = smartWalletEligibilitySentNotifications
        // remove ids that are no more eligible
        let storedIds: Set<String> = .init(tempSmartWalletEligibilitySentNotifications.map { $0.key })
        let idsToRemove: [String] = .init(storedIds.subtracting(ids))
        idsToRemove.forEach { tempSmartWalletEligibilitySentNotifications.removeValue(forKey: $0) }
        // add new ids without timestamp
        let idsToAdd: Set<String> = ids.subtracting(storedIds)
        idsToAdd.forEach { id in
            tempSmartWalletEligibilitySentNotifications[id] = Date.distantPast.timeIntervalSince1970
        }
        smartWalletEligibilitySentNotifications = tempSmartWalletEligibilitySentNotifications
    }
    
    // Identifier to identify previous notification
    func notificationId(type: NotificationType, hash: String, range: Range<Int>?) -> String {
        "\(smartNotificationIdPrefix)_\(type.rawValue)_\(hash)_\(abs(range?.lowerBound ?? 0))_\(abs(range?.upperBound ?? 0))"
    }
    
    func localizationKey(for prefix: String, and certificate: EuropeanCertificate) -> String? {
        let localizationKey: String
        switch certificate.type {
        case .vaccinationEurope:
            if let medicalProductCode = certificate.medicalProductCode,
                [prefix, certificate.type.rawValue, medicalProductCode].joined(separator: ".").localizedOrNil != nil {
                localizationKey = [prefix, certificate.type.rawValue, medicalProductCode].joined(separator: ".")
            } else {
                localizationKey = [prefix, certificate.type.rawValue].joined(separator: ".")
            }
        case .recoveryEurope, .sanitaryEurope:
            localizationKey = [prefix, certificate.type.rawValue].joined(separator: ".")
        default: return nil
        }
        return localizationKey
    }
    
    func description(for localizationKey: String?, firstname: String?, date: Date, labelIdLocalizationKey: String?) -> String? {
        guard let localizationKey = localizationKey else { return nil }
        if let firstname {
            return [String(format: localizationKey.localized, firstname, date.dayShortMonthYearFormatted(timeZoneIndependant: true)), labelIdLocalizationKey?.localizedOrNil].compactMap { $0 }.joined(separator: " ")
        } else {
            return [String(format: localizationKey.localized, date.dayShortMonthYearFormatted(timeZoneIndependant: true)), labelIdLocalizationKey?.localizedOrNil].compactMap { $0 }.joined(separator: " ")
        }
    }
}

// MARK: Pass Elligibillity
private extension WalletManager {
    func passExpirationTimestamp(for certificate: EuropeanCertificate?) -> Double? {
        guard let cert = certificate else { return nil }
        guard let timestamp = expiryTimestamp(cert) else { return nil }
        return timestamp <= Date().timeIntervalSince1970 ? timestamp : nil
    }

    func passExpirationSoonTimestamp(for certificate: EuropeanCertificate) -> Double? {
        guard let expiryTimestamp = expiryTimestamp(certificate) else { return nil }
        let now: Date = Date()
        let expiryDate: Date = Date(timeIntervalSince1970: expiryTimestamp)
        if let dateWithThreshold = Calendar.utc.date(byAdding: .day, value: -smartWalletEngine.displayExpDays, to: expiryDate) {
            return dateWithThreshold <= now && now < expiryDate ? expiryTimestamp : nil
        } else {
            return nil
        }
    }

    func passEligibility(for certificate: EuropeanCertificate) -> EligibilityInfo? {
        guard let eligibility = eligibility(certificate), let eligibilityTimestamp = eligibility.eligibilityTimestamp else { return nil }
        return EligibilityInfo(eligibilityTimestamp: eligibilityTimestamp <= Date().timeIntervalSince1970 ? eligibilityTimestamp : nil, eligibilityLabelId: eligibility.eligibilityLabelId)
    }

    func isEligibleToVaccination(for certificate: EuropeanCertificate) -> Bool {
        passEligibility(for: certificate)?.eligibilityTimestamp != nil
    }

    func passEligibleSoon(for certificate: EuropeanCertificate) -> EligibilityInfo? {
        guard let eligibility = eligibility(certificate), let eligibilityTimestamp = eligibility.eligibilityTimestamp else { return nil }
        let now: Date = Date()
        let eligibilityDate: Date = Date(timeIntervalSince1970: eligibilityTimestamp)
        if let dateWithThreshold = Calendar.utc.date(byAdding: .day, value: -smartWalletEngine.displayElgDays, to: eligibilityDate) {
            let timestamp: Double? = dateWithThreshold <= now && now < eligibilityDate ? eligibilityTimestamp : nil
            return EligibilityInfo(eligibilityTimestamp: timestamp, eligibilityLabelId: eligibility.eligibilityLabelId)
        } else {
            return nil
        }
    }

    func isEligibleToVaccinationSoon(for certificate: EuropeanCertificate) -> Bool {
        passEligibleSoon(for: certificate)?.eligibilityTimestamp != nil
    }
}

// MARK: - Util
private extension Int {
    func daysToSeconds() -> Double { Double(self*24*3600) }
}
