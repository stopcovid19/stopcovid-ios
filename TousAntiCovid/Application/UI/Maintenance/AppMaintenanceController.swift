// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  AppMaintenanceController.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 21/05/2020 - for the TousAntiCovid project.
//


import UIKit
import ServerSDK
import PKHUD

final class AppMaintenanceController: CVTableViewController, MaintenanceController {

    var maintenanceInfo: MaintenanceInfo {
        didSet {
            if isViewLoaded {
                reloadUI()
            }
        }
    }

    private var smileyView: SmileyView?

    private let didTouchAbout: () -> ()
    private let didTouchLater: () -> ()
    
    init(maintenanceInfo: MaintenanceInfo, didTouchAbout: @escaping () -> (), didTouchLater: @escaping () -> ()) {
        self.maintenanceInfo = maintenanceInfo
        self.didTouchLater = didTouchLater
        self.didTouchAbout = didTouchAbout
        super.init(style: .plain)
    }
    
    required init?(coder: NSCoder) {
        fatalError("You must use the standard init() method.")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "app.name".localized
        initUI()
        reloadUI()
        addObserver()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        startSmileyWithHaptic()
    }

    override func createSections() -> [CVSection] {
        makeSections {
            CVSection {
                imageRow()
                textRow()
                
                if let buttonTitle = maintenanceInfo.localizedButtonTitle, let buttonUrl = maintenanceInfo.localizedButtonUrl, maintenanceInfo.mode == .upgrade {
                    buttonRow(title: buttonTitle, url: buttonUrl)
                }

                if maintenanceInfo.mode == .maintenance {
                    retryRow()
                }
                
                if maintenanceInfo.appAvailability != false && !maintenanceInfo.upgradeMandatory {
                    laterRow()
                }
            }
        }
    }

    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        navigationChildController?.scrollViewDidScroll(scrollView)
    }
    
    private func initUI() {
        tableView.separatorStyle = .none
        tableView.showsVerticalScrollIndicator = false
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "common.about".localized, style: .plain, target: self, action: #selector(didTouchAboutButton))
    }

    private func addObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(startSmileyWithHaptic), name: UIApplication.willEnterForegroundNotification, object: nil)
    }

    @objc private func didTouchAboutButton() {
        didTouchAbout()
    }
    
    @objc private func didTouchButton() {
        HUD.show(.progress)
        MaintenanceManager.shared.checkMaintenanceState {
            HUD.hide()
        }
    }

}

// MARK: - Rows -
private extension AppMaintenanceController {
    func imageRow() -> CVRow { CVRow(image: Asset.Images.maintenance.image,
                                     xibName: .onboardingImageCell,
                                     theme: CVRow.Theme(topInset: Appearance.Cell.Inset.extraLarge,
                                                        imageRatio: Appearance.Cell.Image.defaultRatio))
    }
    func textRow() -> CVRow {
        let title: String = maintenanceInfo.localizedTitle ?? ""
        let message: String = maintenanceInfo.localizedMessage ?? ""
        return CVRow(title: title,
                     subtitle: message,
                     xibName: .textCell,
                     theme: CVRow.Theme(topInset: Appearance.Cell.Inset.extraLarge,
                                        titleFont: { Appearance.Cell.Text.headTitleFont }))
    }

    func buttonRow(title: String, url: String) -> CVRow {
        CVRow(title: title,
              xibName: .buttonCell,
              theme: CVRow.Theme(topInset: Appearance.Cell.Inset.extraLarge,
                                 bottomInset: .zero),
              selectionAction: { _ in
            URL(string: url)?.openInSafari()
        })
    }

    func retryRow() -> CVRow {
        CVRow(title: "common.tryAgain".localized,
              xibName: .buttonCell,
              theme: CVRow.Theme(topInset: Appearance.Cell.Inset.extraLarge,
                                 bottomInset: .zero),
              selectionAction: { [weak self] _ in
            self?.didTouchButton()
        })
    }

    func laterRow() -> CVRow {
        CVRow(title: "appMaintenanceController.later.button.title".localized,
              xibName: .buttonCell,
              theme: CVRow.Theme(topInset: .zero,
                                 buttonStyle: .quaternary),
              selectionAction: { [weak self] _ in
            self?.didTouchLater()
        })
    }
}

extension AppMaintenanceController {
    @objc private func startSmileyWithHaptic() {
        guard maintenanceInfo.mode == .upgrade || maintenanceInfo.appAvailability == false else { return }
        guard let smileys = maintenanceInfo.smileysToPop, !smileys.isEmpty else { return }
        stopSmiley()
        smileyView = SmileyView(frame: view.bounds)
        guard let smileyView = smileyView else { return }
        view.window?.addSubview(smileyView)

        smileyView.startSmileys(smileys: smileys, birthRate: Double(660 / smileys.count))
        haptic()
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
            smileyView.stopSmileys()
        }
    }

    private func stopSmiley() {
        smileyView?.removeFromSuperview()
        smileyView = nil
    }

    private func haptic() {
        if #available(iOS 13.0, *) {
            HapticManager.shared.hapticFirework()
        } else {
            let generator: UINotificationFeedbackGenerator = .init()
            generator.notificationOccurred(.success)
        }
    }

    override func motionEnded(_ motion: UIEvent.EventSubtype, with event: UIEvent?) {
        startSmileyWithHaptic()
    }
}
