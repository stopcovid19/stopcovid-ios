// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  FeaturedInfoCardCell.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 05/01/2023 - for the TousAntiCovid project.
//

import UIKit

final class FeaturedInfoCollectionViewCell: CVCardCell {

    @IBOutlet weak var titleBackground: UIView!

    override func setup(with row: CVRow) {
        super.setup(with: row)
        titleBackground.backgroundColor = row.theme.backgroundColor
    }
}
