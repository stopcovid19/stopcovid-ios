// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  KeyFigureCollectionViewCell.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 22/12/2021 - for the TousAntiCovid project.
//

import UIKit

final class KeyFigureCollectionViewCell: CVCardCell {
    @IBOutlet private var placeLabel: UILabel?
    @IBOutlet private var subtitleContainerView: UIView?
    @IBOutlet private var titleContainerView: UIView?
    
    override func setup(with row: CVRow) {
        super.setup(with: row)
        setupTheme(row.theme)
        guard let keyFigure = row.associatedValue as? KeyFigure else { return }
        titleContainerView?.backgroundColor = keyFigure.lightColor.withAlphaComponent(0.8)
        subtitleContainerView?.backgroundColor = keyFigure.lightColor
        placeLabel?.text = row.accessoryText
        placeLabel?.isHidden = row.accessoryText.isNilOrEmpty
    }
    
    override func setupAccessibility() {
        super.setupAccessibility()
        if let row = currentAssociatedRow, let keyFigure = row.associatedValue as? KeyFigure {
            accessibilityLabel = [keyFigure.currentDepartmentSpecificKeyFigure?.label, row.subtitle, row.title].compactMap { $0 }.joined(separator: ":").removingEmojis()
        }
    }
}

// MARK: - Private functions
extension KeyFigureCollectionViewCell {
    func setupTheme(_ theme: CVRow.Theme) {
        placeLabel?.font = Appearance.Cell.Text.subtitleBoldFont
        placeLabel?.textColor = theme.titleColor
        placeLabel?.textAlignment = theme.textAlignment
        placeLabel?.numberOfLines = 1
    }
}
