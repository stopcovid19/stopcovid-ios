// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  HomeBase.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 11/03/2022 - for the TousAntiCovid project.
//

import UIKit
import ServerSDK
import AppModel
import Protocols

struct HomeOutputs {
    var didTouchAppUpdate: () -> ()
    var didTouchManageData: () -> ()
    var didTouchPrivacy: () -> ()
    var didTouchAbout: () -> ()
    var didTouchSupport: (() -> ())?
    var didTouchHealth: () -> ()
    var didTouchInfo: (_ info: Info?) -> ()
    var didTouchKeyFigure: (_ keyFigure: KeyFigure) -> ()
    var didTouchWarningKeyFigures: () -> ()
    var didTouchKeyFigures: () -> ()
    var didTouchFavoriteKeyFigures: () -> ()
    var didTouchComparisonChart: () -> ()
    var didTouchKeyFiguresMap: () -> ()
    var didTouchComparisonChartSharing: (_ shareImage: UIImage?) -> ()
    var didTouchUsefulLinks: () -> ()
    var didTouchVaccination: () -> ()
    var didTouchSanitaryCertificates: (_ url: URL?) -> ()
    var didTouchVerifyWalletCertificate: (() -> ())?
    var didTouchUniversalQrScan: () -> ()
    var didTouchCertificate: (_ certificate: WalletCertificate) -> ()
    var didTouchFeaturedInfo: (_ featuredInfo: FeaturedInfo, _ localUrl: URL?) -> ()
    var didTouchSeeAllFeaturedInfos: () -> ()
    var showUserLanguage: () -> ()
    var didTouchUrgentDgs: () -> ()
    var showNoTracingExplanationsBottomSheet: () -> ()
    var deinitBlock: () -> ()
}

protocol HomeBased: UIViewController,
                    ReloadableContentController,
                    LocalizationsChangesObserver,
                    InfoRepositoryObserver,
                    KeyFiguresChangesObserver,
                    WalletChangesObserver,
                    WalletRulesObserver,
                    FeaturedInfoChangesObserver {
    var viewModel: HomeBasedViewModel { get }
    var popRecognizer: InteractivePopGestureRecognizer? { get set }

    // In UserDefault
    var didAlreadyShowUserLanguage: Bool { get set }
    var latestAvailableBuild: Int? { get set }
    
    var outputs: HomeOutputs { get set }
    var reloadingTimer: Timer? { get set }

    func addHeaderView(height: CGFloat)

    func createReloadingTimer(shouldNextReloadBeAnimated: Bool) -> Timer
}

extension HomeBased {
    func onViewDidLoad() {
        title = "home.title".localized
        navigationChildController?.updateTitle(title)
        addObserver()
        setInteractiveRecognizer()
        reloadUI(animated: false, animatedView: nil, completion: nil)
    }
    
    func onViewDidAppear() {
        showUserLanguageIfNeeded()
    }

    func initUI(scrollView: UIScrollView) {
        addHeaderView(height: navigationChildController?.navigationBarHeight ?? 0.0)
        scrollView.backgroundColor = Appearance.Controller.cardTableViewBackgroundColor
        scrollView.showsVerticalScrollIndicator = false
        scrollView.canCancelContentTouches = true
        navigationController?.setNavigationBarHidden(true, animated: false)
        let scanBarButtonItem: UIBarButtonItem = BlockBarButtonItem(image: Asset.Images.qrScanItem.image, style: .plain, actionHandler: scanQrButtonPressed)
        scanBarButtonItem.accessibilityLabel = "home.qrScan.button.title".localized
        navigationChildController?.updateRightBarButtonItem(scanBarButtonItem)
        navigationChildController?.navigationBar.isAccessibilityElement = false
    }
    
    func buildModel() -> [CVGroup] {
        var groups: [CVGroup] = []
        groups.append(firstGroup())
        if WalletManager.shared.isWalletActivated {
            groups.append(walletGroup())
        }
        if let featuredInfoGroup = featuredInfoGroup() {
            groups.append(featuredInfoGroup)
        }
        if let vaccinationGroup = vaccinationGroup() {
            groups.append(vaccinationGroup)
        }

        if let contactGroup = contactGroup() {
            groups.append(contactGroup)
        }

        if let keyFiguresGroups = keyFiguresGroups() {
            groups.append(contentsOf: keyFiguresGroups)
        }
        groups.append(contentsOf: infoGroups())
        groups.append(moreGroup())
        return groups
    }
}


// MARK: - Observer functions
extension HomeBased {
    func addObserver() {
        LocalizationsManager.shared.addObserver(self)
        viewModel.addObservers(self)
        KeyFiguresManager.shared.addObserver(self)
        WalletManager.shared.addObserver(self)
        WalletElgRulesManager.shared.addObserver(self)
        WalletValidityRulesManager.shared.addObserver(self)
        FeaturedInfoManager.shared.addObserver(self)

        NotificationCenter.default.addObserver(forName: .newWalletCertificateFromDeeplink, object: nil, queue: .main, using: { [weak self] notification in
            self?.newWalletCertificateFromDeeplink(notification)
        })
        NotificationCenter.default.addObserver(forName: .lastAvailableBuildDidUpdate, object: nil, queue: .main, using: { [weak self] _ in
            self?.lastAvailableBuildDidUpdate()
        })
        NotificationCenter.default.addObserver(forName: .openQrScan, object: nil, queue: .main, using: { [weak self] _ in
            self?.openQrScan()
        })
        NotificationCenter.default.addObserver(forName: .didEnterCodeFromDeeplink, object: nil, queue: .main, using: { [weak self] notification in
            self?.didEnterCodeFromDeeplink(notification)
        })
        NotificationCenter.default.addObserver(forName: .didCompletedVaccinationNotification, object: nil, queue: .main, using: { [weak self] _ in
            self?.didCompletedVaccinationNotification()
        })
        NotificationCenter.default.addObserver(forName: .openWallet, object: nil, queue: .main, using: { [weak self] _ in
            self?.openWallet()
        })
        NotificationCenter.default.addObserver(forName: .openCertificateQRCode, object: nil, queue: .main, using: { [weak self] _ in
            self?.openCertificateQRCode()
        })
        NotificationCenter.default.addObserver(forName: UIAccessibility.voiceOverStatusDidChangeNotification, object: nil, queue: .main, using: { [weak self] _ in
            self?.voiceOverStatusDidChange()
        })

        NotificationCenter.default.addObserver(forName: .openSmartWalletFromNotification, object: nil, queue: .main, using: { [weak self] _ in
            self?.openSmartWalletFromNotification()
        })
        NotificationCenter.default.addObserver(forName: UIContentSizeCategory.didChangeNotification, object: nil, queue: .main) { [weak self] _ in
            self?.reloadUI(animated: true, animatedView: nil, completion: nil)
        }
    }
    
    func removeObservers() {
        LocalizationsManager.shared.removeObserver(self)
        viewModel.removeObservers(self)
        KeyFiguresManager.shared.removeObserver(self)
        WalletManager.shared.removeObserver(self)
        WalletElgRulesManager.shared.removeObserver(self)
        WalletValidityRulesManager.shared.removeObserver(self)
        FeaturedInfoManager.shared.removeObserver(self)
        NotificationCenter.default.removeObserver(self)
    }
    
}

// MARK: - Observer private functions
private extension HomeBased {
    func newWalletCertificateFromDeeplink(_ notification: Notification? = nil) {
        guard let url = notification?.object as? URL else { return }
        outputs.didTouchSanitaryCertificates(url)
    }
    
    func lastAvailableBuildDidUpdate(_ notification: Notification? = nil) {
        startReloadingTimer(shouldNextReloadBeAnimated: true)
    }

    func didEnterCodeFromDeeplink(_ notification: Notification? = nil) {
        outputs.showNoTracingExplanationsBottomSheet()
    }
    
    func didCompletedVaccinationNotification(_ notification: Notification? = nil) {
        outputs.didTouchSanitaryCertificates(nil)
    }
    
    func openWallet(_ notification: Notification? = nil) {
        outputs.didTouchSanitaryCertificates(nil)
    }
    
    func openSmartWalletFromNotification(_ notification: Notification? = nil) {
        outputs.didTouchSanitaryCertificates(nil)
    }
    
    func openCertificateQRCode(_ notification: Notification? = nil) {
        guard let certificate = WalletManager.shared.favoriteCertificate else { return }
        outputs.didTouchCertificate(certificate)
    }
    
    func voiceOverStatusDidChange(_ notification: Notification? = nil) {
        startReloadingTimer(shouldNextReloadBeAnimated: false)
    }
}

extension HomeBased {
    func didTouchUpdateLocation() {
        KeyFiguresManager.shared.updateLocation(from: self)
    }
    
    func openQrScan(_ notification: Notification? = nil) {
        CameraAuthorizationManager.requestAuthorization { granted, isFirstTimeRequest in
            if granted {
                self.outputs.didTouchUniversalQrScan()
            } else if !isFirstTimeRequest {
                self.showAlert(title: "scanCodeController.camera.authorizationNeeded.title".localized,
                               message: "scanCodeController.camera.authorizationNeeded.message".localized,
                               okTitle: "common.settings".localized,
                               cancelTitle: "common.cancel".localized, handler:  {
                    UIApplication.shared.openSettings()
                })
            }
        }
    }
}

private extension HomeBased {
    func setInteractiveRecognizer() {
        guard let navigationController = navigationController else { return }
        popRecognizer = InteractivePopGestureRecognizer(controller: navigationController)
        navigationController.interactivePopGestureRecognizer?.delegate = popRecognizer
    }
}


// MARK: - UI private functions
private extension HomeBased {

    func scanQrButtonPressed() {
        UIImpactFeedbackGenerator(style: .light).impactOccurred()
        // This asyncAfter is here not to have a freeze for the Haptic feedback.
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.openQrScan()
        }
    }
    
}

// MARK: - User actions functions
extension HomeBased {
    func showUserLanguageIfNeeded() {
        guard !didAlreadyShowUserLanguage else { return }
        guard !viewModel.isCurrentLanguageSupported else { return }
        didAlreadyShowUserLanguage = true
        outputs.showUserLanguage()
    }

    func didTouchShare() {
        let controller: UIActivityViewController = UIActivityViewController(activityItems: ["sharingController.appSharingMessage".localized], applicationActivities: nil)
        present(controller, animated: true, completion: nil)
    }
}


// MARK: - Reloading timer management
extension HomeBased {
    func startReloadingTimer(shouldNextReloadBeAnimated: Bool) {
        stopReloadingTimer()
        let timer: Timer = createReloadingTimer(shouldNextReloadBeAnimated: shouldNextReloadBeAnimated)
        reloadingTimer = timer
        RunLoop.main.add(timer, forMode: .common)
    }

    func stopReloadingTimer() {
        reloadingTimer?.invalidate()
        reloadingTimer = nil
    }
}

// MARK: - LocalizationsChangesObserver
extension HomeBased {
    func localizationsChanged() {
        startReloadingTimer(shouldNextReloadBeAnimated: false)
    }
}
// MARK: - InfoRepositoryObserver
extension HomeBased {
    func infoCenterDidUpdate() {
        startReloadingTimer(shouldNextReloadBeAnimated: false)
    }
}

// MARK: - KeyFiguresChangesObserver
extension HomeBased {
    func keyFiguresDidUpdate() {
        startReloadingTimer(shouldNextReloadBeAnimated: true)
    }
    
    func postalCodeDidUpdate(_ postalCode: String?) {}
}

// MARK: - WalletChangesObserver
extension HomeBased {
    func walletCertificatesDidUpdate() {}
    
    func walletFavoriteCertificateDidUpdate() {
        startReloadingTimer(shouldNextReloadBeAnimated: true)
    }
    
    func walletSmartStateDidUpdate() {
        startReloadingTimer(shouldNextReloadBeAnimated: false)
    }
    
    func walletBlacklistDidUpdate() {}
}

// MARK: - WalletRulesObserver
extension HomeBased {
    func walletRulesDidUpdate() {
        startReloadingTimer(shouldNextReloadBeAnimated: true)
    }
}

// MARK: - FeatureInfoChangesObserver
extension HomeBased  {
    func featuredInfoDidUpdate() {
        startReloadingTimer(shouldNextReloadBeAnimated: true)
    }
}
