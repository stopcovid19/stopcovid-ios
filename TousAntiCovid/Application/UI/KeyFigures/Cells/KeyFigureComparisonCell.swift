// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  KeyFigureComparisonCell.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 17/01/2023 - for the TousAntiCovid project.
//

import UIKit

final class KeyFigureComparisonCell: CardCell, Xibbed {
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var comparisonLastWeekStackView: UIStackView!
    @IBOutlet weak var comparisonLastWeekValueLabel: UILabel!
    @IBOutlet weak var comparisonLastWeekTitleLabel: UILabel!
    @IBOutlet weak var comparisonLastWeekDateLabel: UILabel!

    @IBOutlet weak var comparisonMinStackView: UIStackView!
    @IBOutlet weak var comparisonMinValueLabel: UILabel!
    @IBOutlet weak var comparisonMinTitleLabel: UILabel!
    @IBOutlet weak var comparisonMinDateLabel: UILabel!

    @IBOutlet weak var comparisonMaxStackView: UIStackView!
    @IBOutlet weak var comparisonMaxValueLabel: UILabel!
    @IBOutlet weak var comparisonMaxTitleLabel: UILabel!
    @IBOutlet weak var comparisonMaxDateLabel: UILabel!


    override func awakeFromNib() {
        super.awakeFromNib()
        comparisonLastWeekTitleLabel.font = { Appearance.Cell.Text.titleFont }()
        comparisonLastWeekTitleLabel.textColor = Appearance.Cell.Text.titleColor

        comparisonLastWeekValueLabel.font = { Appearance.Cell.Text.valueFont }()
        comparisonLastWeekDateLabel.font = { Appearance.Cell.Text.captionTitleFont }()
        comparisonLastWeekDateLabel.textColor = Appearance.Cell.Text.captionTitleColor

        comparisonMinTitleLabel.font = { Appearance.Cell.Text.titleFont }()
        comparisonMinTitleLabel.textColor = Appearance.Cell.Text.titleColor

        comparisonMinValueLabel.font = { Appearance.Cell.Text.valueFont }()
        comparisonMinDateLabel.font = { Appearance.Cell.Text.captionTitleFont }()
        comparisonMinDateLabel.textColor = Appearance.Cell.Text.captionTitleColor

        comparisonMaxTitleLabel.font = { Appearance.Cell.Text.titleFont }()
        comparisonMaxTitleLabel.textColor = Appearance.Cell.Text.titleColor

        comparisonMaxValueLabel.font = { Appearance.Cell.Text.valueFont }()
        comparisonMaxDateLabel.font = { Appearance.Cell.Text.captionTitleFont }()
        comparisonMaxDateLabel.textColor = Appearance.Cell.Text.captionTitleColor
    }

    @IBAction func didTouchMin(_ sender: Any) {
        currentAssociatedRow?.secondarySelectionAction?()
    }

    @IBAction func didTouchMax(_ sender: Any) {
        currentAssociatedRow?.tertiarySelectionAction?()
    }

    override func setup(with row: CVRow) {
        super.setup(with: row)
        setupUI(with: row)
        setupContent(with: row)
        setupAccessibility()
    }

    private func setupUI(with row: CVRow) {
        let comparisonData: KeyFigureComparison? = row.associatedValue as? KeyFigureComparison
        descriptionLabel.textColor = Appearance.Cell.Text.titleColor

        comparisonLastWeekValueLabel.textColor = comparisonData?.comparisonLastWeekValueColor ?? row.theme.titleColor
        comparisonMinValueLabel.textColor = comparisonData?.comparisonMinValueColor ?? row.theme.titleColor
        comparisonMaxValueLabel.textColor = comparisonData?.comparisonMaxValueColor ?? row.theme.titleColor
    }

    private func setupContent(with row: CVRow) {
        guard let comparisonData = row.associatedValue as? KeyFigureComparison else {
            comparisonLastWeekStackView.isHidden = true
            comparisonMinStackView.isHidden = true
            comparisonMaxStackView.isHidden = true
            descriptionLabel.isHidden = true
            return
        }
        descriptionLabel.isHidden = comparisonData.comparisonDescription.isNil
        if let description = comparisonData.comparisonDescription {
            descriptionLabel.attributedText = NSMutableAttributedString(string: description,
                                                                        tag: Constant.BBCodeTag.bold,
                                                                        defaultAttributes: [NSAttributedString.Key.font: Appearance.Cell.Text.subtitleFont],
                                                                        tagAttributes: [NSAttributedString.Key.font: Appearance.Cell.Text.subtitleBoldFont])
        }

        comparisonLastWeekStackView.isHidden = comparisonData.comparisonLastWeekValue.isNil
        comparisonLastWeekTitleLabel.text = comparisonData.comparisonLastWeekTitle
        comparisonLastWeekValueLabel.text = comparisonData.comparisonLastWeekValue
        comparisonLastWeekDateLabel.text = comparisonData.comparisonLastWeekDate

        comparisonMinStackView.isHidden = comparisonData.comparisonMinValue.isNil
        comparisonMinTitleLabel.text = comparisonData.comparisonMinTitle
        comparisonMinValueLabel.text = comparisonData.comparisonMinValue
        comparisonMinDateLabel.text = comparisonData.comparisonMinDate

        comparisonMaxStackView.isHidden = comparisonData.comparisonMaxValue.isNil
        comparisonMaxTitleLabel.text = comparisonData.comparisonMaxTitle
        comparisonMaxValueLabel.text = comparisonData.comparisonMaxValue
        comparisonMaxDateLabel.text = comparisonData.comparisonMaxDate
    }

    override func setupAccessibility() {
        comparisonLastWeekStackView.isAccessibilityElement = true
        comparisonMinStackView.isAccessibilityElement = true
        comparisonMaxStackView.isAccessibilityElement = true
        comparisonLastWeekStackView.accessibilityTraits = .staticText
        comparisonMinStackView.accessibilityTraits = .staticText
        comparisonMaxStackView.accessibilityTraits = .staticText

        comparisonLastWeekStackView.accessibilityLabel = [comparisonLastWeekTitleLabel.text, comparisonLastWeekValueLabel.text?.accessibilityNumberFormattedString()].compactMap({ $0 }).joined(separator: ". ")
        if let date = comparisonLastWeekDateLabel.text {
            comparisonLastWeekStackView.accessibilityHint = String(format: "keyFigures.update".localized, date)
        }

        comparisonMinStackView.accessibilityLabel = [comparisonMinTitleLabel.text, comparisonMinValueLabel.text?.accessibilityNumberFormattedString()].compactMap({ $0 }).joined(separator: ". ")
        if let date = comparisonMinDateLabel.text {
            comparisonMinStackView.accessibilityHint = String(format: "keyFigures.update".localized, date)
        }

        comparisonMaxStackView.accessibilityLabel = [comparisonMaxTitleLabel.text, comparisonMaxValueLabel.text?.accessibilityNumberFormattedString()].compactMap({ $0 }).joined(separator: ". ")
        if let date = comparisonMaxDateLabel.text {
            comparisonMaxStackView.accessibilityHint = String(format: "keyFigures.update".localized, date)
        }
    }

}
