// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  KeyFigureCell.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 21/10/2020 - for the TousAntiCovid project.
//

import UIKit

final class KeyFigureCell: CardCell, Xibbed {
    
    @IBOutlet private var dateLabel: UILabel!
    @IBOutlet private var valueLabel: UILabel!
    @IBOutlet private var departmentLabel: UILabel!

    @IBOutlet private var countryStackView: UIStackView!
    @IBOutlet private var countryLabel: UILabel!
    @IBOutlet private var countryValueLabel: UILabel!
    
    @IBOutlet private var sharingImageView: UIImageView!
    @IBOutlet private var favoriteImageView: UIImageView!
    
    @IBOutlet private var valuesContainerStackView: DynamicContentStackView!
    @IBOutlet private var sharingButton: UIButton!
    @IBOutlet private var favoriteButton: UIButton!
    
    @IBOutlet private var baselineAlignmentConstraint: NSLayoutConstraint?
    
    private var contentSizeCategoryThreashold: UIContentSizeCategory { UIScreen.main.bounds.width >= 375.0 ? .accessibilityMedium : .extraLarge }
    private var isLarge: Bool { UIApplication.shared.preferredContentSizeCategory >= contentSizeCategoryThreashold }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        addObservers()
    }
    
    deinit {
        removeObservers()
    }
    
    override func setup(with row: CVRow) {
        super.setup(with: row)
        setupUI()
        setupContent(with: row)
        setupAccessibility(isFavorite: row.isOn == true)
    }

    override func capture() -> UIImage? {
        sharingImageView.isHidden = true
        let image: UIImage? = containerView.cvScreenshot()
        sharingImageView.isHidden = false
        return image
    }

    private func addObservers() {
        removeObservers()
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(changeInPreferredContentSize),
                                               name: UIContentSizeCategory.didChangeNotification,
                                               object: nil)
    }
    
    private func removeObservers() {
        NotificationCenter.default.removeObserver(self)
    }
    
    private func setupUI() {
        valuesContainerStackView.threshold = contentSizeCategoryThreashold
        valuesContainerStackView.thresholdAxis = .vertical
        valuesContainerStackView.thresholdAlignment = .leading
        valuesContainerStackView.thresholdSpacing = 20.0
        dateLabel.font = Appearance.Cell.Text.captionTitleFont
        dateLabel.textColor = Appearance.Cell.Text.captionTitleColor
        departmentLabel?.textColor = Appearance.Cell.Text.captionTitleColor
        departmentLabel?.font = Appearance.Cell.Text.captionTitleFont2
        countryLabel?.textColor = Appearance.Cell.Text.captionTitleColor
        countryLabel?.font = Appearance.Cell.Text.captionTitleFont2
        countryValueLabel?.font = Appearance.Cell.Text.titleFontExtraBold
        cvSubtitleLabel?.font = Appearance.Cell.Text.subtitleFont
        valueLabel.font = Appearance.Cell.Text.headTitleFont2
        sharingImageView.tintColor = Appearance.tintColor
        sharingImageView.image = Asset.Images.shareIcon.image
        favoriteImageView.tintColor = Appearance.tintColor
        changeInPreferredContentSize()
    }
    
    private func setupContent(with row: CVRow) {
        guard let keyFigure = row.associatedValue as? KeyFigure else { return }
        valueLabel.textColor = keyFigure.color
        cvTitleLabel?.textColor = keyFigure.color
        countryValueLabel?.textColor = keyFigure.color
        favoriteButton.tintColor = Appearance.tintColor
        favoriteButton.isHidden = row.secondarySelectionAction == nil
        if let departmentKeyFigure = keyFigure.currentDepartmentSpecificKeyFigure, KeyFiguresManager.shared.canShowCurrentlyNeededFile {
            dateLabel.text = departmentKeyFigure.formattedDate
            departmentLabel?.text = departmentKeyFigure.label.uppercased()
            departmentLabel?.isHidden = false
            valueLabel.text = departmentKeyFigure.valueToDisplay.formattingValueWithThousandsSeparatorIfPossible()
            countryLabel?.text = "france".localized.uppercased()
            countryValueLabel.text = keyFigure.valueGlobalToDisplay.formattingValueWithThousandsSeparatorIfPossible()
            countryStackView?.isHidden = false
        } else {
            dateLabel.text = keyFigure.formattedDate
            if KeyFiguresManager.shared.currentFormattedDepartmentNameAndPostalCode == nil || keyFigure.category == .app || !KeyFiguresManager.shared.canShowCurrentlyNeededFile {
                departmentLabel?.isHidden = true
            } else {
                departmentLabel?.text = "common.country.france".localized.uppercased()
                departmentLabel?.isHidden = false
            }
            countryStackView?.isHidden = true
            valueLabel.text = keyFigure.valueGlobalToDisplay.formattingValueWithThousandsSeparatorIfPossible()
        }
        favoriteImageView.image = row.isOn == true ? Asset.Images.filledHeart.image : Asset.Images.emptyHeart.image
    }

    override func setupAccessibility() {}

    private func setupAccessibility(isFavorite: Bool) {
        var valueAccessibility: [String?] = [cvTitleLabel?.text]

        if !departmentLabel.isHidden {
            valueAccessibility.append(departmentLabel.text)
        }
        valueAccessibility.append(valueLabel.text)

        valueLabel.accessibilityLabel = valueAccessibility.compactMap { $0 }.joined(separator: ". ")
        valueLabel.accessibilityHint = dateLabel.text

        if let valueLabel = valueLabel {
            accessibilityElements = [valueLabel]
        }

        if let countryValueLabel = countryValueLabel, !departmentLabel.isHidden {
            let valueCountryAccessibility:  [String?] = [cvTitleLabel?.text, countryLabel.text, countryValueLabel.text]
            countryValueLabel.accessibilityLabel = valueCountryAccessibility.compactMap { $0 }.joined(separator: ". ")
            countryValueLabel.accessibilityHint = dateLabel.text
            accessibilityElements?.append(countryValueLabel)
        }

        if let cvSubtitleLabel = cvSubtitleLabel {
            accessibilityElements?.append(cvSubtitleLabel)
        }

        if let favoriteButton = favoriteButton {
        favoriteButton.accessibilityLabel = isFavorite ? "accessibility.wallet.dcc.favorite.remove".localized : "accessibility.hint.addToFavorite".localized
            accessibilityElements?.append(favoriteButton)
        }

        if let sharingButton = sharingButton {
            sharingButton.accessibilityLabel = String(format: "accessibility.hint.keyFigure.share.withLabel".localized, (cvTitleLabel?.text).orEmpty)
            accessibilityElements?.append(sharingButton)
        }
    }
    
    @IBAction private func didTouchSharingButton(_ sender: Any) {
        currentAssociatedRow?.selectionActionWithCell?(self)
    }
    
    @IBAction private func didTouchFavoriteButton(_ sender: Any) {
        UIImpactFeedbackGenerator(style: .light).impactOccurred()
        currentAssociatedRow?.secondarySelectionAction?()
    }
    
    @objc private func changeInPreferredContentSize() {
        if isLarge {
            baselineAlignmentConstraint?.isActive = false
        } else {
            baselineAlignmentConstraint?.isActive = true
        }
    }

}
