// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  KeyFiguresFavoritesListController.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 07/12/2022 - for the TousAntiCovid project.
//

import UIKit

final class KeyFiguresFavoritesListController: CVTableViewController {
    private var keyFigures: [KeyFigure]
    private let didReorder: (_ sortedKeyFigures: [KeyFigure]) -> ()
    private let deinitBlock: () -> ()
        
    init(keyFigures: [KeyFigure], didReorder: @escaping (_ sortedKeyFigures: [KeyFigure]) -> (), deinitBlock: @escaping () -> ()) {
        self.keyFigures = keyFigures
        self.didReorder = didReorder
        self.deinitBlock = deinitBlock
        super.init(style: .plain)
    }
    
    required init?(coder: NSCoder) {
        fatalError("Must use default init() method.")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initUI()
        reloadUI()
    }
    
    deinit {
        deinitBlock()
    }

    override func accessibilityPerformEscape() -> Bool {
        validateReorder()
        return true
    }
    
    override func createSections() -> [CVSection] {
        makeSections {
            CVSection(title: nil,
                      subtitle: "keyfigures.reorder.description".localized,
                      headerTopMargin: Appearance.Cell.Inset.medium,
                      headerBottomMargin: Appearance.Cell.Inset.medium,
                      rows: rows())
        }
    }
}

extension KeyFiguresFavoritesListController {
    override func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .none
    }

    override func tableView(_ tableView: UITableView, shouldIndentWhileEditingRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    override func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let movedObject: KeyFigure = keyFigures[sourceIndexPath.row]
        keyFigures.remove(at: sourceIndexPath.row)
        keyFigures.insert(movedObject, at: destinationIndexPath.row)
    }
}

// MARK: - Utils
private extension KeyFiguresFavoritesListController {
    func initUI() {
        title = "keyfigures.reorder.button.title".localized
        tableView.backgroundColor = Appearance.Controller.cardTableViewBackgroundColor
        tableView.showsVerticalScrollIndicator = false
        tableView.separatorStyle = .singleLine
        tableView.isEditing = true
        navigationItem.leftBarButtonItem = .init(title: "common.cancel".localized, style: .plain, target: self, action: #selector(dismissController))
        navigationItem.rightBarButtonItem = .init(title: "common.ok".localized, style: .done, target: self, action: #selector(validateReorder))
    }
    
    func rows() -> [CVRow] {
        keyFigures.map { keyfigure in
            CVRow(title: keyfigure.label,
                  subtitle: keyfigure.description,
                  xibName: XibName.Row.keyFigureListCell,
                  theme: CVRow.Theme(backgroundColor: Appearance.Cell.cardBackgroundColor,
                                     topInset: Appearance.Cell.Inset.normal,
                                     bottomInset: Appearance.Cell.Inset.normal,
                                     textAlignment: .natural,
                                     titleFont: { Appearance.Cell.Text.standardFont },
                                     titleColor: Appearance.Cell.Text.headerTitleColor,
                                     subtitleColor: Appearance.Cell.Text.subtitleColor ,
                                     subtitleLinesCount: 3,
                                     separatorLeftInset: Appearance.Cell.leftMargin))
        }
    }
    
    @objc func dismissController() {
        dismiss(animated: true)
    }
    
    @objc func validateReorder() {
        didReorder(keyFigures)
        dismiss(animated: true)
    }
}
