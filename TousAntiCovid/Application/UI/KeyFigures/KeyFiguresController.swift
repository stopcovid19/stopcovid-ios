// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  KeyFiguresController.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 18/09/2020 - for the TousAntiCovid project.
//

import UIKit
import PKHUD

final class KeyFiguresController: CVTableViewController {
    
    enum Mode {
        case favorites
        case all
    }
    
    struct Action {
        let title: String
        let icon: UIImage
        let isEnabled: Bool
        let actionBlock: () -> ()

        init(title: String, icon: UIImage, isEnabled: Bool = true, actionBlock: @escaping () -> ()) {
            self.title = title
            self.icon = icon
            self.isEnabled = isEnabled
            self.actionBlock = actionBlock
        }

        @available(iOS 13.0, *)
        func toMenuAction() -> UIAction {
            .init(title: title, image: icon, attributes: isEnabled ? [] : .disabled) { _ in
                actionBlock()
            }
        }
        
        func toAlertAction() -> UIAlertAction {
            let action: UIAlertAction = .init(title: title, style: .default) { _ in
                actionBlock()
            }
            action.isEnabled = isEnabled
            return action
        }
    }
    
    private var menuActions: [Action] {
        var actions: [Action] = [
            Action(title: "keyfigures.comparison.screen.title".localized, icon: Asset.Images.compareOutlined.image, actionBlock: { [weak self] in
                self?.didTouchCompareButton()
            })]

        actions.append(Action(title: "keyfigures.reorder.button.title".localized, icon: Asset.Images.reorderOutlined.image, isEnabled: isReorderAvailable, actionBlock: { [weak self] in
            self?.didTouchReorderButton()
        }))
        if KeyFiguresManager.shared.displayDepartmentLevel {
            actions.append(Action(title: "home.infoSection.updatePostalCode.alert.newPostalCode".localized, icon: Asset.Images.locationOutlined.image, actionBlock: { [weak self] in
                self?.didTouchLocationButton()
            }))
            if KeyFiguresManager.shared.currentPostalCode != nil {
                actions.append(Action(title: "home.infoSection.updatePostalCode.alert.deletePostalCode".localized, icon: Asset.Images.removeLocationOutlined.image, actionBlock: { [weak self] in
                    self?.didTouchDeleteLocationButton()
                }))
            }
        }
        return actions
    }

    private let didTouchKeyFigure: (_ keyFigure: KeyFigure) -> ()
    private let didTouchReadExplanationsNow: () -> ()
    private let didTouchCompare: () -> ()
    private let didTouchReorder: () -> ()
    private let didTouchWarningKeyFigures: () -> ()
    private let deinitBlock: () -> ()
    private var currentMode: Mode

    private var isReorderAvailable: Bool { KeyFiguresManager.shared.favoriteKeyFigures.count >= 2 }
    
    init(mode: Mode,
         didTouchReadExplanationsNow: @escaping () -> (),
         didTouchKeyFigure: @escaping(_ keyFigure: KeyFigure) -> (),
         didTouchCompare: @escaping () -> (),
         didTouchReorder: @escaping () -> (),
         didTouchWarningKeyFigures: @escaping () -> (),
         deinitBlock: @escaping () -> ()) {
        self.didTouchReadExplanationsNow = didTouchReadExplanationsNow
        self.didTouchKeyFigure = didTouchKeyFigure
        self.didTouchCompare = didTouchCompare
        self.didTouchReorder = didTouchReorder
        self.didTouchWarningKeyFigures = didTouchWarningKeyFigures
        self.deinitBlock = deinitBlock
        self.currentMode = mode
        super.init(style: .plain)
    }
    
    required init?(coder: NSCoder) {
        fatalError("Must use default init() method.")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        initUI()
        reloadUI()
        addObservers()
    }
    
    deinit {
        removeObservers()
        deinitBlock()
    }
    
    private func initUI() {
        title = "keyFiguresController.title".localized
        tableView.backgroundColor = Appearance.Controller.cardTableViewBackgroundColor
        tableView.showsVerticalScrollIndicator = false
        tableView.separatorStyle = .singleLine
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "common.close".localized, style: .plain, target: self, action: #selector(didTouchCloseButton))
        navigationItem.leftBarButtonItem?.accessibilityHint = "accessibility.closeModal.zGesture".localized
        updateRightBarButtonItems()
    }
    
    private func updateRightBarButtonItems() {
        if #available(iOS 14.0, *) {
            let menu: UIMenu = .init(title: "", children: menuActions.map { $0.toMenuAction() })
            navigationItem.rightBarButtonItem = UIBarButtonItem(image: Asset.Images.moreColored.image, menu: menu)
        } else {
            navigationItem.rightBarButtonItem = UIBarButtonItem(image: Asset.Images.moreColored.image, style: .plain, target: self, action: #selector(didTouchMoreButton))
        }
        navigationItem.rightBarButtonItem?.accessibilityLabel = "accessibility.hint.otherActions".localized
    }
    
    @objc private func didTouchMoreButton() {
        let alert: UIAlertController = .init(title: nil, message: nil, preferredStyle: .actionSheet)
        menuActions.map { $0.toAlertAction() }.forEach { action in
            alert.addAction(action)
        }
        alert.addAction(UIAlertAction(title: "common.cancel".localized, style: .cancel, handler: { _ in }))
        navigationController?.present(alert, animated: true)
    }
    
    @objc private func didTouchCloseButton() {
        dismiss(animated: true, completion: nil)
    }
    
    @objc private func didTouchCompareButton() {
        didTouchCompare()
    }
    
    private func addObservers() {
        KeyFiguresManager.shared.addObserver(self)
    }
    
    private func removeObservers() {
        KeyFiguresManager.shared.removeObserver(self)
    }
    
    override func createSections() -> [CVSection] {
        makeSections {
            CVSection {
                if !KeyFiguresManager.shared.canShowCurrentlyNeededFile {
                    loadingErrorRows()
                }
                if let warningRow = warningRow() { warningRow }
                let explanationRows: [CVRow] = explanationRows("keyFiguresController.explanations.text".localized, bottomMargin: currentMode == .favorites ? Appearance.Cell.Inset.medium : .zero)
                if !explanationRows.isEmpty {
                    modeSelectionRow(isAloneInHeader: false)
                    explanationRows
                } else {
                    modeSelectionRow(isAloneInHeader: true)
                }
                if currentMode == .favorites {
                    favoritesKeyFiguresRows()
                }
            }
            if currentMode == .all {
                allKeyFiguresSections()
            }
        }
    }
    
}

// MARK: - Rows
private extension KeyFiguresController {
    func loadingErrorRows() -> [CVRow] {
        [CVRow(subtitle: "keyFiguresController.fetchError.message".localized,
              xibName: .cardCell,
              theme: CVRow.Theme(backgroundColor: Appearance.Cell.cardBackgroundColor,
                                 topInset: Appearance.Cell.Inset.normal,
                                 bottomInset: .zero,
                                 textAlignment: .center,
                                 maskedCorners: .top),
              selectionAction: { _ in
            HUD.show(.progress)
            KeyFiguresManager.shared.fetchKeyFigures {
                HUD.hide()
            }
        }),
        CVRow(title: "keyFiguresController.fetchError.button".localized,
              xibName: .standardCardCell,
              theme:  CVRow.Theme(backgroundColor: Appearance.Button.Secondary.backgroundColor,
                                  topInset: .zero,
                                  bottomInset: .zero,
                                  textAlignment: .center,
                                  titleFont: { Appearance.Cell.Text.actionTitleFont },
                                  titleColor: Appearance.Button.Secondary.titleColor,
                                  separatorLeftInset: nil,
                                  separatorRightInset: nil,
                                  maskedCorners: .bottom),
              selectionAction: { _ in
            HUD.show(.progress)
            KeyFiguresManager.shared.fetchKeyFigures {
                HUD.hide()
            }
        })]
    }
    
    func warningRow() -> CVRow? {
        guard !"home.infoSection.warning".localizedOrEmpty.isEmpty else { return nil }
        return CVRow(title: "home.infoSection.warning".localized,
                     xibName: .standardCardCell,
                     theme: .init(backgroundColor: Appearance.Cell.cardBackgroundColor,
                                  topInset: Appearance.Cell.Inset.normal,
                                  bottomInset: .zero,
                                  textAlignment: .natural,
                                  titleFont: { Appearance.Cell.Text.subtitleFont },
                                  titleLinesCount: 3),
                     selectionAction: { [weak self] cell in
            self?.didTouchWarningKeyFigures()
        })
    }
    
    func explanationRows(_ explanations: String?, bottomMargin: CGFloat) -> [CVRow] {
        let explanationsRow: CVRow? = explanations.isNilOrEmpty ? nil : .init(subtitle: explanations,
                                                                              xibName: .textCell,
                                                                              theme: CVRow.Theme(topInset: Appearance.Cell.Inset.normal,
                                                                                                 bottomInset: .zero,
                                                                                                 textAlignment: .natural,
                                                                                                 titleFont: { Appearance.Cell.Text.headTitleFont }))
        let buttonTitle: String? = "keyFiguresController.explanations.button".localized
        let buttonRow: CVRow? = buttonTitle.isNilOrEmpty ? nil : .init(buttonTitle: "keyFiguresController.explanations.button".localized,
                                                                       xibName: .linkButtonCell,
                                                                       theme:  CVRow.Theme(topInset: Appearance.Cell.Inset.small,
                                                                                           bottomInset: bottomMargin),
                                                                       secondarySelectionAction: { [weak self] in
            self?.didTouchReadExplanationsNow()
        })
        return [explanationsRow, buttonRow].compactMap { $0 }
    }
    
    func allKeyFiguresSections() -> [CVSection] {
        let filteredKeyFigures: [KeyFigure] = KeyFiguresManager.shared.keyFigures.filter { $0.isLabelReady }
        let groupedKeyFigures: [(key: KeyFigure.Category, value: [KeyFigure])] = Dictionary(grouping: filteredKeyFigures) { $0.category }.sorted { $0.key.order < $1.key.order }
        return groupedKeyFigures.map { keyFigureCategory in
            CVSection(title: "keyFiguresController.category.\(keyFigureCategory.key.rawValue)".localized) {
                keyFigureCategory.value.enumerated().compactMap { keyFigureRow(for: $0.element, isLastRow: $0.offset == keyFigureCategory.value.count - 1) }
            }
        }
    }
    
    func favoritesKeyFiguresRows() -> [CVRow] {
        let favorites: [KeyFigure] = KeyFiguresManager.shared.favoriteKeyFigures
        if favorites.isEmpty {
            let emptyMessageRow: CVRow = CVRow(title: "keyfigures.noFavorite.cell.title".localized,
                                               subtitle: "keyFiguresController.noFavorite.cell.subtitle".localized,
                                               xibName: .cardCell,
                                               theme: CVRow.Theme(backgroundColor: Appearance.Cell.cardBackgroundColor,
                                                                  bottomInset: .zero,
                                                                  textAlignment: .natural,
                                                                  maskedCorners: .top))
            let resetRow: CVRow = CVRow(title: "keyFiguresController.resetFavorite.button".localized,
                                        xibName: .standardCardCell,
                                        theme:  CVRow.Theme(backgroundColor: Appearance.Button.Secondary.backgroundColor,
                                                            topInset: .zero,
                                                            bottomInset: Appearance.Cell.Inset.normal,
                                                            textAlignment: .center,
                                                            titleFont: { Appearance.Cell.Text.actionTitleFont },
                                                            titleColor: Appearance.Button.Secondary.titleColor,
                                                            separatorLeftInset: nil,
                                                            separatorRightInset: nil,
                                                            maskedCorners: .bottom),
                                        selectionAction: { [weak self] _ in
                KeyFiguresManager.shared.resetFavorites()
                self?.reloadUI()
            })
            return [emptyMessageRow, resetRow]
        } else {
            var favoriteRows: [CVRow] = favorites.enumerated().compactMap { keyFigureRow(for: $0.element, isLastRow: $0.offset == favorites.count - 1) }
            if isReorderAvailable {
                favoriteRows.append(reorderButtonRow())
            }
            return favoriteRows
        }
    }
    
    func keyFigureRow(for keyFigure: KeyFigure, isLastRow: Bool) -> CVRow {
        CVRow(title: keyFigure.label,
              subtitle: keyFigure.description,
              isOn: KeyFiguresManager.shared.isFavorite(keyFigure),
              xibName: .keyFigureCell,
              theme: CVRow.Theme(backgroundColor: Appearance.Cell.cardBackgroundColor,
                                 topInset: .zero,
                                 bottomInset: isLastRow ? .zero : Appearance.Cell.Inset.medium,
                                 textAlignment: .natural,
                                 subtitleLinesCount: 2),
              associatedValue: keyFigure,
              selectionActionWithCell: { [weak self] view in
            guard let cell = view as? CVTableViewCell else { return }
            self?.didTouchSharingFor(cell: cell, keyFigure: keyFigure)
        },
              selectionAction: { [weak self] _ in
            self?.didTouchKeyFigure(keyFigure)
        },
              secondarySelectionAction: { [weak self] in
            guard let self = self else { return }
            KeyFiguresManager.shared.toggleFavorite(keyFigure)
            self.reloadUI(animated: self.currentMode == .favorites)
        })
    }

    func modeSelectionRow(isAloneInHeader: Bool) -> CVRow {
        CVRow(segmentsTitles: ["keyFiguresController.favorites.segment.title".localized, "keyFiguresController.all.segment.title".localized],
              selectedSegmentIndex: currentMode == .favorites ? 0 : 1,
              xibName: .segmentedCell,
              theme:  CVRow.Theme(backgroundColor: .clear,
                                  topInset: Appearance.Cell.Inset.normal,
                                  bottomInset: isAloneInHeader ? Appearance.Cell.Inset.medium : 1.0,
                                  textAlignment: .natural,
                                  titleFont: { Appearance.SegmentedControl.selectedFont },
                                  subtitleFont: { Appearance.SegmentedControl.normalFont }),
              segmentsActions: [favoriteSegmentAction, allSegmentAction])
    }

    func reorderButtonRow() -> CVRow {
        CVRow(title: "keyfigures.reorder.button.title".localized,
              xibName: .buttonCell,
              theme: CVRow.Theme(topInset: Appearance.Cell.Inset.normal, bottomInset: 0.0, buttonStyle: .secondary),
              selectionAction: { [weak self] _ in
            self?.didTouchReorderButton()
        })
    }
}

// MARK: - Actions
private extension KeyFiguresController {
    
    @objc func didTouchReorderButton() {
        didTouchReorder()
    }
    
    @objc func didTouchLocationButton() {
        KeyFiguresManager.shared.defineNewPostalCode(from: self)
    }
    
    @objc func didTouchDeleteLocationButton() {
        KeyFiguresManager.shared.deletePostalCode()
    }

    func didTouchSharingFor(cell: CVTableViewCell, keyFigure: KeyFigure) {
        let sharingText: String
        if let keyFigureDepartment = keyFigure.currentDepartmentSpecificKeyFigure {
            sharingText = String(format: "keyFigure.sharing.department".localized,
                                 keyFigure.label,
                                 keyFigureDepartment.label,
                                 keyFigureDepartment.valueToDisplay,
                                 keyFigure.label,
                                 keyFigure.valueGlobalToDisplay)
        } else {
            sharingText = String(format: "keyFigure.sharing.national".localized,
                                 keyFigure.label,
                                 keyFigure.valueGlobalToDisplay)
        }
        showSharingController(for: [sharingText, KeyFigureCaptureView.captureKeyFigure(keyFigure)])
    }

    func favoriteSegmentAction() {
        guard currentMode != .favorites else { return }
        currentMode = .favorites
        reloadUI(animated: true)
    }
    
    func allSegmentAction() {
        guard currentMode != .all else { return }
        currentMode = .all
        reloadUI(animated: true)
    }
}

extension KeyFiguresController: KeyFiguresChangesObserver {

    func keyFiguresDidUpdate() {
        reloadNextToKeyFiguresUpdate()
    }
    
    func postalCodeDidUpdate(_ postalCode: String?) {}
    
    private func reloadNextToKeyFiguresUpdate() {
        updateRightBarButtonItems()
        reloadUI(animated: true)
    }

}
