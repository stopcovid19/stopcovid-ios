// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  ComparisonDetailCell.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 20/01/2023 - for the TousAntiCovid project.
//

import UIKit

final class ComparisonDetailCell: CardCell, Xibbed {

    override func setup(with row: CVRow) {
        super.setup(with: row)
        setupContent(with: row)
    }

    private func setupContent(with row: CVRow) {
        if let description = row.subtitle {
            cvSubtitleLabel?.attributedText = NSMutableAttributedString(string: description,
                                                                        tag: Constant.BBCodeTag.bold,
                                                                        defaultAttributes: [NSAttributedString.Key.font: Appearance.Cell.Text.subtitleFont],
                                                                        tagAttributes: [NSAttributedString.Key.font: Appearance.Cell.Text.subtitleBoldFont])
        }
    }
    
}
