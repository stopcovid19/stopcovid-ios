// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  KeyFigureMapCell.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 18/01/2023 - for the TousAntiCovid project.
//

import UIKit
import Macaw

@available(iOS 13.0, *)
final class KeyFigureMapCell: CVTableViewCell {
    
    private struct Selection {
        var department: KeyFigureMapDepartment
        var point: CGPoint
    }
    
    var shortUnit: String? { KeyFiguresMapManager.shared.selectedKeyFigureForMap?.shortUnit }
    
    @IBOutlet private weak var playButton: UIButton!
    @IBOutlet private weak var shareButtonImageView: UIImageView!
    @IBOutlet private weak var iconImageView: UIImageView!
    @IBOutlet private weak var mapView: SVGView!
    @IBOutlet private weak var progressionSlider: UISlider!
    
    private let sliderStep: Float = 1.0
    private let feedbackStyle: UIImpactFeedbackGenerator.FeedbackStyle = .light
    
    private var timer: Timer?
    private var currentSerieIndex: Int = 0 {
        didSet {
            updateDepartmentsNodesColor()
            updateMarker()
            updateUI()
        }
    }
    private var selection: Selection? = nil {
        didSet {
            updateDepartmentsNodesColor()
            selectionDidChange()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initUI()
    }
    
    override func removeFromSuperview() {
        super.removeFromSuperview()
        stopTimer()
    }
    
    override func prepareForReuse() {
        stopTimer()
        super.prepareForReuse()
    }
    
    override func setup(with row: CVRow) {
        super.setup(with: row)
        cvTitleLabel?.isHidden = false
        cvSubtitleLabel?.attributedText = NSMutableAttributedString(string: row.subtitle.orEmpty,
                                                                    tag: Constant.BBCodeTag.bold,
                                                                    defaultAttributes: [NSAttributedString.Key.font: Appearance.Cell.Text.captionTitleFont],
                                                                    tagAttributes: [NSAttributedString.Key.font: Appearance.Cell.Text.captionTitleBoldFont])
        
        cvAccessoryLabel?.isHidden = true
        accessoryType = .none
        guard let selectedKeyFigure = row.associatedValue as? KeyFigureMap else { return }
        setupSlider(for: selectedKeyFigure)
        currentSerieIndex = KeyFiguresMapManager.shared.currentSerieIndexForSelectedKeyFigure ?? 0
        manageTimer()
        updateUI()
        initNodes()
        reloadSelectionDepartmentValues(valuesDepartments: selectedKeyFigure.valuesDepartments)
    }
    
    override func capture() -> UIImage? {
        shareButtonImageView.isHidden = true
        progressionSlider.isHidden = true
        playButton.isHidden = true
        cvAccessoryLabel?.isHidden = false
        iconImageView.isHidden = false
        let image: UIImage? = contentView.cvScreenshot()
        shareButtonImageView.isHidden = false
        progressionSlider.isHidden = false
        cvAccessoryLabel?.isHidden = true
        playButton.isHidden = false
        iconImageView.isHidden = true
        
        guard let image else { return nil }
        let captureImageView: UIImageView = UIImageView(image: image)
        captureImageView.frame.size = CGSize(width: image.size.width / UIScreen.main.scale, height: image.size.height / UIScreen.main.scale)
        captureImageView.backgroundColor = Appearance.Cell.cardBackgroundColor
        return captureImageView.cvScreenshot()
    }
    
    override func setupAccessibility() {
        super.setupAccessibility()
        contentView.isAccessibilityElement = true
        contentView.accessibilityTraits = currentAssociatedRow?.selectionAction != nil ? .button : .staticText
        accessibilityElements = [contentView]
        contentView.accessibilityLabel = "keyfigures.map.cell.accessibility.description".localized
    }
    
    func stopPlayingIfNecessary() {
        if timer != nil {
            stopTimer()
        }
    }
    
    func startPlayingIfNecessary() {
        if KeyFiguresMapManager.shared.keyFiguresMapAutoplay, timer == nil {
            startTimer()
        }
    }
    
}

// MARK: Nodes
@available(iOS 13.0, *)
private extension KeyFigureMapCell {
    
    func initNodes() {
        guard let departments = KeyFiguresMapManager.shared.selectedKeyFigureMap?.valuesDepartments else { return }
        departments.forEach { department in
            guard let nodes = nodesFor(department: department) else { return }
            setDepartmentNodesColor(color: department.serieItems?.item(at: currentSerieIndex)?.color, departmentNode: nodes.0, associatedNode: nodes.1)
            setDepartmentNodesAction(department: department, departmentNode: nodes.0, associatedNode: nodes.1)
        }
    }
    
    func nodesFor(department: KeyFigureMapDepartment) -> (Node, Node?)? {
        let node: Node? = mapView.node
        guard let departmentNode = node?.nodeBy(tag: department.number) else { return nil }
        let associatedNode: Node? = Constant.Map.ileDeFranceDeptNbs.contains(department.number) ? node?.nodeBy(tag: department.number + "-zoom") : nil
        return (departmentNode, associatedNode)
    }
}

// MARK: Actions
@available(iOS 13.0, *)
private extension KeyFigureMapCell {
    
    @IBAction func sliderValueDidChange(_ sender: UISlider) {
        let roundedValue: Float = round(sender.value / sliderStep) * sliderStep
        sender.value = roundedValue
        hapticFeeback(style: feedbackStyle)
        currentSerieIndex = KeyFiguresMapManager.shared.updateCurrentSerieIndexForSelectedKeyFigure(with: Int(roundedValue)) ?? 0
    }
    
    @IBAction func didTouchPlayButton(_ sender: Any) {
        KeyFiguresMapManager.shared.keyFiguresMapAutoplay = !KeyFiguresMapManager.shared.keyFiguresMapAutoplay
        manageTimer()
    }
    
    @IBAction private func didTouchSharingButton(_ sender: Any) {
        currentAssociatedRow?.selectionActionWithCell?(self)
    }
    
    @objc private func touchDownSlider(_ sender: UISlider) {
        stopPlayingIfNecessary()
    }
    
    @objc private func touchUpSlider(_ sender: UISlider) {
        startPlayingIfNecessary()
    }
    
    @objc private func didTouchMarker() {
        removeSelection()
    }
}

// MARK: Colors
@available(iOS 13.0, *)
private extension KeyFigureMapCell {
    
    func updateDepartmentsNodesColor() {
        guard let departments = KeyFiguresMapManager.shared.selectedKeyFigureMap?.valuesDepartments else { return }
        departments.forEach { department in
            guard let nodes = nodesFor(department: department) else { return }
            setDepartmentNodesColor(color: department.serieItems?.item(at: currentSerieIndex)?.color, departmentNode: nodes.0, associatedNode: nodes.1)
            if department.number == selection?.department.number {
                let alpha: Double = (department.serieItems?.item(at: currentSerieIndex)?.normalizedOpacity ?? 0.0) / 100
                nodes.0.setFillColor(Appearance.Cell.Map.selectionColor.withAlphaComponent(alpha))
                nodes.1?.setFillColor(Appearance.Cell.Map.selectionColor.withAlphaComponent(alpha))
            }
        }
    }
    
    func setDepartmentNodesColor(color: UIColor?, departmentNode: Node, associatedNode: Node?) {
        guard let color else { return }
        [departmentNode, associatedNode].compactMap { $0 }
            .forEach {
                $0.setFillColor(color)
            }
    }
    
    func setDepartmentNodesAction(department: KeyFigureMapDepartment, departmentNode: Node, associatedNode: Node?) {
        [departmentNode, associatedNode].compactMap { $0 }
            .forEach {
                $0.setTouchAction { [weak self] point in
                    self?.touchAction(department: department, point: point)
                }
            }
    }
    
    func touchAction(department: KeyFigureMapDepartment, point: CGPoint) {
        selection = .init(department: department, point: point)
    }
}

// MARK: Marker
@available(iOS 13.0, *)
private extension KeyFigureMapCell {
    
    func selectionDidChange() {
        removeMarker()
        guard let selection else { return }
        guard let item = selection.department.serieItems?.item(at: currentSerieIndex) else { return }
        addMarker(point: selection.point, title: selection.department.name, subtitle: item.value.toString(shortUnit: shortUnit)?.formattingValueWithThousandsSeparatorIfPossible())
    }
    
    func removeSelection() {
        selection = nil
    }
    
    func updateMarker() {
        guard mapView.subviews.first(where: { $0 is MapMarkerView }) != nil else { return }
        guard let department = selection?.department, let item = department.serieItems?.item(at: currentSerieIndex) else { return }
        (mapView.subviews.first { $0 is MapMarkerView } as? MapMarkerView)?.titleLabel.text = department.name
        (mapView.subviews.first { $0 is MapMarkerView } as? MapMarkerView)?.subtitleLabel.text = item.value.toString(shortUnit: shortUnit)?.formattingValueWithThousandsSeparatorIfPossible()
    }
    
    func addMarker(point: CGPoint, title: String, subtitle: String?) {
        let marker: MapMarkerView = .instantiate()
        let origin: CGPoint = mapView.bounds.origin
        let touchedPoint: CGPoint = .init(x: point.x - origin.x, y: point.y - origin.y)
        marker.setup(title: title, subtitle: subtitle)
        marker.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTouchMarker)))
        removeMarker()
        let offsetForDraw: CGPoint = offsetForDraw(point: touchedPoint, size: marker.size)
        mapView.addCenteredSubview(marker, xOffset: touchedPoint.x - (mapView.frame.width / 2) + offsetForDraw.x, yOffset: touchedPoint.y - (mapView.frame.height / 2) + offsetForDraw.y)
    }
    
    func removeMarker() {
        mapView.subviews.first { $0 is MapMarkerView }?.removeFromSuperview()
    }
    
    func offsetForDraw(point: CGPoint, size: CGSize, padding: CGFloat = 12) -> CGPoint {
        let origin: CGPoint = .init(x: point.x - size.width / 2, y: point.y - size.height / 2)
        var baseOffset: CGPoint = .init(x: .zero, y: size.height / 2 + padding)
        if origin.x + baseOffset.x < 0 {
            baseOffset.x = -origin.x + padding
        } else if origin.x + size.width + baseOffset.x > mapView.bounds.size.width {
            baseOffset.x = mapView.bounds.size.width - origin.x - size.width - padding
        }

        if origin.y + baseOffset.y < 0 {
            baseOffset.y = size.height
        } else if origin.y + size.height + baseOffset.y > mapView.bounds.size.height {
            baseOffset.y = -size.height / 2 - padding
        }
        return baseOffset
    }
    
    func reloadSelectionDepartmentValues(valuesDepartments: [KeyFigureMapDepartment]?) {
        guard let currentSelection = selection else { return }
        if let department = valuesDepartments?.first(where: { $0.name == currentSelection.department.name }) {
            selection = .init(department: department, point: currentSelection.point)
        } else {
            removeSelection()
        }
    }
}

// MARK: Timer
@available(iOS 13.0, *)
private extension KeyFigureMapCell {
    
    func manageTimer() {
        if KeyFiguresMapManager.shared.keyFiguresMapAutoplay {
            startTimer()
        } else {
            stopTimer()
        }
    }
    
    func startTimer() {
        playButton.setImage(UIImage(systemName: "pause.fill"), for: .normal)
        timer?.invalidate()
        timer = Timer(timeInterval: 0.2, target: self, selector: #selector(timerFired), userInfo: nil, repeats: true)
        RunLoop.main.add(timer!, forMode: .common)
    }
    
    @objc func timerFired() {
        currentSerieIndex = KeyFiguresMapManager.shared.updateCurrentSerieIndexForSelectedKeyFigure(with: currentSerieIndex + 1) ?? 0
    }
    
    func stopTimer() {
        playButton.setImage(UIImage(systemName: "play.fill"), for: .normal)
        timer?.invalidate()
        timer = nil
    }
    
}

// MARK: UI components
@available(iOS 13.0, *)
private extension KeyFigureMapCell {
    
    func initUI() {
        mapView.fileName = "France-departments"
        mapView.backgroundColor = .clear
        mapView.contentMode = .scaleAspectFit
        playButton.setTitle("", for: .normal)
        playButton.tintColor = Appearance.tintColor
        progressionSlider.tintColor = Appearance.tintColor
        shareButtonImageView.image = Asset.Images.shareIcon.image
        shareButtonImageView.tintColor = Appearance.tintColor
        iconImageView.image = Asset.Images.tacHorizontalAlert.image
        iconImageView.isHidden = true
        iconImageView.tintColor = Appearance.tintColor
    }
    
    func setupSlider(for keyFigure: KeyFigureMap) {
        guard let items = keyFigure.valuesDepartments?.first?.serieItems else { return }
        progressionSlider.maximumValue = Float(items.count)
        progressionSlider.addTarget(self, action: #selector(touchDownSlider), for: [.touchDown])
        progressionSlider.addTarget(self, action: #selector(touchUpSlider), for: [.touchUpInside, .touchUpOutside, .touchCancel])
    }
    
    func hapticFeeback(style: UIImpactFeedbackGenerator.FeedbackStyle) {
        let impact: UIImpactFeedbackGenerator = .init(style: style)
        impact.impactOccurred()
    }
    
    func updateUI() {
        guard let keyFigure = KeyFiguresMapManager.shared.selectedKeyFigureMap else { return }
        guard let item = keyFigure.valuesDepartments?.first?.serieItems?.item(at: currentSerieIndex) else { return }
        cvTitleLabel?.text = Date(timeIntervalSince1970: item.date).dayShortMonthYearFormatted(timeZoneIndependant: true)
        progressionSlider.value = Float(currentSerieIndex)
    }
    
}

private extension KeyFigureSeriesItem {
    
    var color: UIColor {
        Appearance.Cell.Map.tintColor.withAlphaComponent(normalizedOpacity / 100)
    }
    
}

private extension Node {
    
    func setFillColor(_ color: UIColor) {
        (self as? Shape)?.fill(color)
    }
    
    func setTouchAction(_ action: @escaping (_ point: CGPoint) -> ()) {
        self.onTouchReleased { event in
            action(event.points.first?.location(in: .scene).toCG() ?? .zero)
        }
    }
    
}

private extension Shape {
    
    func fill(_ color: UIColor) {
        fill = Color.rgba(r: Int(color.components().red * 255), g: Int(color.components().green * 255), b: Int(color.components().blue * 255), a: Double(color.components().alpha))
    }
    
}
