// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  KeyFiguresMapController.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 17/01/2023 - for the TousAntiCovid project.
//

import UIKit
import Macaw

@available(iOS 13.0, *)
final class KeyFiguresMapController: CVTableViewController {
    
    private enum State {
        case loading
        case error
        case loaded
        case unknown
    }
    
    // MARK: - Constants
    private let didTouchSelection: (_ currentSelection: KeyFigure, _ selectionDidChange: @escaping (KeyFigure) -> ()) -> ()
    private let didTouchKeyFigureDescription: (_ title: String, _ description: String) -> ()
    private let didTouchMoreInfo: () -> ()
    private let deinitBlock: () -> ()
    private let didTouchClose: () -> ()
    
    private weak var mapCell: KeyFigureMapCell?
    private var selectedValueUnit: String? { selectedKeyFigure?.shortUnit }
    
    // MARK: - Variables
    private var selectedKeyFigure: KeyFigure? { KeyFiguresMapManager.shared.selectedKeyFigureForMap }
    private var viewState: State = .unknown
    
    init(didTouchKeyFigureSelection: @escaping (_ currentSelection: KeyFigure, _ selectionDidChange: @escaping (KeyFigure) -> ()) -> (),
         didTouchKeyFigureDescription: @escaping (_ title: String, _ description: String) -> (),
         didTouchMoreInfo: @escaping () -> (),
         didTouchClose: @escaping () -> (),
         deinitBlock: @escaping () -> ()) {
        self.didTouchSelection = didTouchKeyFigureSelection
        self.didTouchKeyFigureDescription = didTouchKeyFigureDescription
        self.didTouchClose = didTouchClose
        self.didTouchMoreInfo = didTouchMoreInfo
        self.deinitBlock = deinitBlock
        super.init(style: .plain)
    }
    
    required init?(coder: NSCoder) {
        fatalError("Must use default init() method.")
    }
    
    deinit {
        deinitBlock()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        KeyFiguresMapManager.shared.observer = self
        KeyFiguresMapManager.shared.fetchMapKeyFigures()
        initUI()
        reloadUI()
    }
    
    override func reloadUI(animated: Bool = false, animatedView: UIView? = nil, completion: (() -> ())? = nil) {
        if KeyFiguresMapManager.shared.error != nil, KeyFiguresMapManager.shared.keyFiguresMap.isEmpty {
            viewState = .error
        } else if KeyFiguresMapManager.shared.isLoading {
            viewState = .loading
        } else {
            viewState = .loaded
        }
        super.reloadUI(animated: animated, animatedView: animatedView, completion: completion)
    }
    
    override func createSections() -> [CVSection] {
        switch viewState {
        case .loading:
            return makeSections {
                CVSection {
                    CVRow(xibName: .loadingCardCell)
                }
            }
        case .loaded:
            return makeSections {
                if let selectionRows = keyFigureSelectionRows() {
                    CVSection {
                        selectionRows
                    } header: {
                        .groupedHeader
                    }
                }
                CVSection {
                    mapRow()
                    displayInfoRow()
                } header: {
                    .groupedHeader
                }
            }
        case .error:
            return makeSections {
                CVSection {
                    CVRow(title: "keyfigures.map.screen.noDataAvailable".localized, xibName: .standardCell)
                    CVRow(title: "common.retry".localized,
                          xibName: .buttonCell,
                          theme: .init(leftInset: Appearance.Cell.Inset.large,
                                       rightInset: Appearance.Cell.Inset.large,
                                       buttonStyle: .secondary),
                          selectionAction: { _ in
                        KeyFiguresMapManager.shared.fetchMapKeyFigures()
                    })
                }
            }
        case .unknown:
            return []
        }
    }
    
    override func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        mapCell?.stopPlayingIfNecessary()
    }
    
    override func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        mapCell?.startPlayingIfNecessary()
    }
}

@available(iOS 13.0, *)
extension KeyFiguresMapController: KeyFiguresMapChangesObserver {
    func keyFiguresMapDidUpdate() {
        reloadUI()
    }
}

@available(iOS 13.0, *)
private extension KeyFiguresMapController {
    func initUI() {
        title = "keyfigures.map.screen.title".localized
        tableView.backgroundColor = Appearance.Controller.cardTableViewBackgroundColor
        tableView.showsVerticalScrollIndicator = false
        tableView.separatorStyle = .singleLine
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "common.close".localized, style: .plain, target: self, action: #selector(didTouchCloseButton))
        navigationItem.leftBarButtonItem?.accessibilityHint = "accessibility.closeModal.zGesture".localized
    }
}

@available(iOS 13.0, *)
// MARK: - Actions
private extension KeyFiguresMapController {
    @objc func didTouchCloseButton() {
        didTouchClose()
    }
}

@available(iOS 13.0, *)
// MARK: - Rows
private extension KeyFiguresMapController {
    func keyFigureSelectionRows() -> [CVRow]? {
        guard let selectedKeyFigure else { return nil }
        return [sectionHeaderRow(title: selectedKeyFigure.label,
                                 subtitle: selectedKeyFigure.description),
                CVRow(title: "keyfigures.map.screen.changeFigure".localized,
                      image: nil,
                      xibName: .standardCell,
                      theme: CVRow.Theme(backgroundColor: Appearance.Cell.cardBackgroundColor,
                                         topInset: Appearance.Cell.Inset.normal,
                                         bottomInset: Appearance.Cell.Inset.normal,
                                         textAlignment: .natural,
                                         titleFont: { Appearance.Cell.Text.standardFont },
                                         titleColor: Appearance.tintColor,
                                         imageTintColor: Appearance.tintColor,
                                         imageSize: Appearance.Cell.Image.size),
                      selectionAction: { [weak self] _ in
            guard let currentKeyFigure = self?.selectedKeyFigure else { return }
            self?.didTouchSelection(currentKeyFigure) { [weak self] selectedKeyfigure in
                KeyFiguresMapManager.shared.setSelectionForMap(keyFigure: selectedKeyfigure)
                self?.reloadUI()
            }
        },
                      willDisplay: { view in
            guard let cell = view as? CVTableViewCell else { return }
            cell.cvTitleLabel?.accessibilityTraits = .button
        })]
    }
    
    func sectionHeaderRow(title: String, subtitle: String) -> CVRow {
        CVRow(title: title,
              subtitle: subtitle,
              xibName: .textCell,
              theme: CVRow.Theme(backgroundColor: Appearance.Cell.cardBackgroundColor,
                                 topInset: Appearance.Cell.Inset.normal,
                                 bottomInset: Appearance.Cell.Inset.normal,
                                 textAlignment: .natural,
                                 titleFont: { Appearance.Cell.Text.smallHeadTitleFont },
                                 subtitleLinesCount: 3,
                                 separatorLeftInset: Appearance.Cell.leftMargin,
                                 accessoryType: UITableViewCell.AccessoryType.none),
              selectionAction: { [weak self] _ in
            guard let self else { return }
            self.didTouchKeyFigureDescription(title, subtitle)
        })
    }
    
    func mapRow() -> CVRow {
        CVRow(subtitle: createDescriptionString(),
              accessoryText: KeyFiguresMapManager.shared.selectedKeyFigureForMap?.label,
              xibName: .keyFigureMapCell,
              theme: CVRow.Theme(backgroundColor: Appearance.Cell.cardBackgroundColor,
                                 textAlignment: .natural,
                                 titleFont: { Appearance.Cell.Text.smallHeadTitleFont },
                                 subtitleColor: Appearance.Button.Disabled.titleColor,
                                 separatorLeftInset: Appearance.Cell.leftMargin,
                                 accessoryType: UITableViewCell.AccessoryType.none),
              associatedValue: KeyFiguresMapManager.shared.selectedKeyFigureMap,
              selectionActionWithCell: { [weak self] view in
            guard let cell = view as? CVTableViewCell else { return }
            self?.didTouchSharingFor(cell: cell)
        },
              willDisplay: { [weak self] cell in
            guard let cell = cell as? KeyFigureMapCell else { return }
            self?.mapCell = cell
        })
    }
    
    func displayInfoRow() -> CVRow {
        CVRow(title: "keyfigures.map.screen.displayMethod".localized,
              image: nil,
              xibName: .standardCell,
              theme: CVRow.Theme(backgroundColor: Appearance.Cell.cardBackgroundColor,
                                 topInset: Appearance.Cell.Inset.normal,
                                 bottomInset: Appearance.Cell.Inset.normal,
                                 textAlignment: .natural,
                                 titleFont: { Appearance.Cell.Text.standardFont },
                                 titleColor: Appearance.tintColor,
                                 imageTintColor: Appearance.tintColor,
                                 imageSize: Appearance.Cell.Image.size),
              selectionAction: { [weak self] _ in
            self?.didTouchMoreInfo()
        },
              willDisplay: { view in
            guard let cell = view as? CVTableViewCell else { return }
            cell.cvTitleLabel?.accessibilityTraits = .button
            cell.accessoryType = .none
        })
    }
    
    func didTouchSharingFor(cell: CVTableViewCell) {
        var activityItems: [Any?] = []
        activityItems.append(cell.capture())
        showSharingController(for: activityItems)
    }
}

@available(iOS 13.0, *)
// MARK: - String management
private extension KeyFiguresMapController {
    func calculateMinMax () -> (Double, Double) {
        guard let departmentValues: [KeyFigureMapDepartment] = KeyFiguresMapManager.shared.selectedKeyFigureMap?.valuesDepartments else { return (0.0, 0.0) }
        let seriesArray = departmentValues.compactMap { $0.serieItems }.flatMap{ $0 }
        let min: Double = seriesArray.min(by: { $0.value < $1.value })?.value ?? 0.0
        let max: Double = seriesArray.max(by: { $0.value < $1.value })?.value ?? 0.0
        return (min, max)
    }
    
    func createDescriptionString () -> String {
        let departmentValue: (min: Double, max: Double) = calculateMinMax()
        let minValue: String = (departmentValue.min.toString(shortUnit: selectedValueUnit)?.formattingValueWithThousandsSeparatorIfPossible()).orEmpty
        let maxValue: String = (departmentValue.max.toString(shortUnit: selectedValueUnit)?.formattingValueWithThousandsSeparatorIfPossible()).orEmpty
        let arrayDescriptionMinMax: [String] = ["keyfigures.map.screen.description".localized, String(format: "keyfigures.map.screen.minMaxFigures".localized, minValue, maxValue)]
        return arrayDescriptionMinMax.joined(separator: "\n")
    }
}
