// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  SmileyView.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 20/06/2023 - for the TousAntiCovid project.
//

import UIKit
import QuartzCore

class SmileyView: UIView {
    private var emitter: CAEmitterLayer!
    private var smileys: [String]!
    private var intensity: Float!
    private var birthRate: Double = 10.0

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }

    func startSmileys(smileys: [String], birthRate: Double) {
        emitter = CAEmitterLayer()
        emitter.emitterPosition = CGPoint(x: frame.size.width / 2.0, y: 0)
        emitter.emitterShape = CAEmitterLayerEmitterShape.line
        emitter.emitterSize = CGSize(width: frame.size.width, height: 1)
        emitter.beginTime = CACurrentMediaTime()
        self.birthRate = birthRate
        let cells: [CAEmitterCell] = smileys.map { smiley(smileyString: $0) }

        emitter.emitterCells = cells
        layer.addSublayer(emitter)
    }

    func stopSmileys() {
        emitter?.birthRate = 0
    }

    private func setup() {
        intensity = 1.0
        isUserInteractionEnabled = false
    }

    private func smiley(smileyString: String) -> CAEmitterCell {
        let smiley: CAEmitterCell = .init()
        smiley.birthRate = Float(birthRate) * intensity
        smiley.lifetime = 14.0 * intensity
        smiley.lifetimeRange = 0
        smiley.velocity = CGFloat(450.0 * intensity)
        smiley.velocityRange = CGFloat(80.0 * intensity)
        smiley.emissionLongitude = CGFloat(Double.pi)
        smiley.emissionRange = CGFloat(Double.pi/4)
        smiley.spin = CGFloat(3.5 * intensity)
        smiley.spinRange = CGFloat(4.0 * intensity)
        smiley.scaleRange = CGFloat(intensity)
        smiley.scaleSpeed = CGFloat(-0.1 * intensity)
        smiley.contents = smileyString.image().cgImage
        return smiley
    }
}

fileprivate extension String {
    func image(with font: UIFont = UIFont.systemFont(ofSize: 8.0)) -> UIImage {
        let string: NSString = .init(string: "\(self)")
        let attributes: [NSAttributedString.Key: Any] = [.font: font]
        let size: CGSize = string.size(withAttributes: attributes)

        return UIGraphicsImageRenderer(size: size).image { _ in
            string.draw(at: .zero, withAttributes: attributes)
        }
    }
}
