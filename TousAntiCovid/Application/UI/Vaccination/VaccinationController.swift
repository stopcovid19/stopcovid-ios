// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  VaccinationController.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 12/01/2021 - for the TousAntiCovid project.
//

import UIKit

final class VaccinationController: CVTableViewController {
    
    private let deinitBlock: () -> ()
    private let didTouchWebVaccination: () -> ()
    init(didTouchWebVaccination: @escaping () -> (), deinitBlock: @escaping () -> ()) {
        self.didTouchWebVaccination = didTouchWebVaccination
        self.deinitBlock = deinitBlock
        super.init(style: .plain)
    }
    
    required init?(coder: NSCoder) {
        fatalError("Must use the other init method")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateTitle()
        initUI()
        reloadUI()
        addObservers()
    }
    
    deinit {
        removeObservers()
    }
    
    private func updateTitle() {
        title = "vaccinationController.title".localized
    }
    
    private func initUI() {
        tableView.backgroundColor = Appearance.Controller.cardTableViewBackgroundColor
        tableView.showsVerticalScrollIndicator = false
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "common.close".localized, style: .plain, target: self, action: #selector(didTouchCloseButton))
        navigationItem.leftBarButtonItem?.accessibilityHint = "accessibility.closeModal.zGesture".localized
    }
    
    override func createSections() -> [CVSection] {
        makeSections {
            CVSection(rows: [locationRow(), eligibilityRow()])
        }
    }
    
    @objc private func didTouchCloseButton() {
        dismiss(animated: true, completion: nil)
    }
    
    private func addObservers() {
        LocalizationsManager.shared.addObserver(self)
    }
    
    private func removeObservers() {
        LocalizationsManager.shared.removeObserver(self)
    }
    
    // MARK: - Row -
    private func locationRow() -> CVRow {
        CVRow(
            title: "vaccinationController.location.title".localized,
            subtitle: "vaccinationController.location.subtitle".localized,
            image: Asset.Images.centresvaxxDark.image,
            xibName: .vaccinationCenterCell,
            theme: CVRow.Theme(backgroundColor: Appearance.tintColor,
                               topInset: Appearance.Cell.Inset.normal,
                               bottomInset: .zero,
                               textAlignment: .natural,
                               titleColor: Appearance.Button.Primary.titleColor,
                               subtitleColor: Appearance.Button.Primary.titleColor,
                               maskedCorners: .all),
            selectionAction: { [weak self] _ in
                self?.didTouchWebVaccination()
            })
    }
    
    private func eligibilityRow() -> CVRow {
        CVRow(
            title: "vaccinationController.eligibility.title".localized,
            subtitle: "vaccinationController.eligibility.subtitle".localized,
            xibName: .paragraphCell,
            theme: CVRow.Theme(backgroundColor: Appearance.Cell.cardBackgroundColor,
                               topInset: Appearance.Cell.Inset.normal,
                               bottomInset: .zero,
                               textAlignment: .left,
                               titleFont: { Appearance.Cell.Text.headTitleFont })
        )}
}

extension VaccinationController: LocalizationsChangesObserver {
    
    func localizationsChanged() {
        updateTitle()
        reloadUI()
    }
}
