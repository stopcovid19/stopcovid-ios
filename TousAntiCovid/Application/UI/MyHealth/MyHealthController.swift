// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  MyHealthController.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 09/04/2020 - for the TousAntiCovid project.
//

import UIKit
import PKHUD
import StorageSDK
import ServerSDK
import AppModel
import Protocols

final class MyHealthController: CVTableViewController {

    private let viewModel: MyHealthViewModel
    private let didTouchLink: (ParagraphLink) -> ()

    init(viewModel: MyHealthViewModel, didTouchLink: @escaping(ParagraphLink) -> ()) {
        self.viewModel = viewModel
        self.didTouchLink = didTouchLink
        super.init(style: .plain)
    }
    
    required init?(coder: NSCoder) {
        fatalError("Must use the other init method")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.fetchData(forceRefresh: false)
        initUI()
        reloadUI()
        addObservers()
    }
    
    deinit {
        removeObservers()
    }
    
    override func createSections() -> [CVSection] {
        let rows: [CVRow] = [headerRow()] + sectionRows()
        return [CVSection(rows: rows)]
    }

    private func headerRow() -> CVRow {
        CVRow(image: Asset.Images.health.image,
              xibName: .imageCell,
              theme: CVRow.Theme(topInset: Appearance.Cell.Inset.medium,
                                 imageRatio: 375.0 / 116.0))
    }

    private func sectionRows() -> [CVRow] {
        viewModel.sections.map { section in
            CVRow(title: section.title.localized,
                  subtitle: section.description.localized,
                  buttonTitle: section.link?.label.localized,
                  xibName: .paragraphCell,
                  theme: CVRow.Theme(backgroundColor: Appearance.Cell.cardBackgroundColor,
                                     topInset: .zero,
                                     bottomInset: Appearance.Cell.Inset.medium,
                                     textAlignment: .left,
                                     titleFont: { Appearance.Cell.Text.headTitleFont }),
                  secondarySelectionAction: section.link == nil ? nil : { [weak self] in
                self?.didTouchLink(section.link!)
            })
        }
    }

    private func initUI() {
        title =  "myHealthController.title".localized
        tableView.backgroundColor = Appearance.Controller.cardTableViewBackgroundColor
        tableView.showsVerticalScrollIndicator = false
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "common.close".localized, style: .plain, target: self, action: #selector(didTouchCloseButton))
        navigationItem.leftBarButtonItem?.accessibilityHint = "accessibility.closeModal.zGesture".localized
    }
    
    @objc private func didTouchCloseButton() {
        dismiss(animated: true, completion: nil)
    }

    private func addObservers() {
        LocalizationsManager.shared.addObserver(self)
        viewModel.addObservers(self)
    }

    private func removeObservers() {
        LocalizationsManager.shared.removeObserver(self)
        viewModel.removeObservers(self)
    }

    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        navigationChildController?.scrollViewDidScroll(scrollView)
    }

}

extension MyHealthController: LocalizationsChangesObserver {

    func localizationsChanged() {
        reloadUI()
    }

}

extension MyHealthController: MyHealthRepositoryObserver {
    func myHealthDidUpdate() {
        reloadUI()
    }
}
