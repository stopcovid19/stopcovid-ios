// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  FileViewerController.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 06/01/2023 - for the TousAntiCovid project.
//

import UIKit
import PDFKit

final class FileViewerViewController: UIViewController {

    private let featuredInfo: FeaturedInfo
    private let localUrl: URL?
    private var moreInfoUrl: URL? {
        guard let stringUrl = featuredInfo.url else { return nil }
        return URL(string: stringUrl)
    }

    init(featuredInfo: FeaturedInfo, localUrl: URL?) {
        self.featuredInfo = featuredInfo
        self.localUrl = localUrl
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        title = featuredInfo.title
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "common.close".localized, style: .plain, target: self, action: #selector(didTouchCloseButton))
        displayView()
    }

    @objc private func didTouchCloseButton() {
        dismiss(animated: true, completion: nil)
    }

    private func displayView() {
        let pdfView: PDFView = createPdfView(withFrame: self.view.bounds)
        let url: URL
        if let localUrl, FileManager.default.fileExists(atPath: localUrl.path, isDirectory: nil) {
            url = localUrl
        } else if let serverUrl = URL(string: featuredInfo.hd) {
            url = serverUrl
        } else {
            return
        }

        view.addConstrainedSubview(pdfView)

        DispatchQueue.global(qos: .userInitiated).async {
            let document: PDFDocument? = .init(url: url)

            DispatchQueue.main.async { [weak self] in
                pdfView.document = document
                pdfView.minScaleFactor = pdfView.scaleFactorForSizeToFit

                if self?.moreInfoUrl != nil {
                    self?.addMoreInfoButton(in: pdfView)
                }
            }
        }
    }

    private func createPdfView(withFrame frame: CGRect) -> PDFView {
        let pdfView: PDFView = .init(frame: frame)
        pdfView.autoScales = true
        pdfView.backgroundColor = Appearance.Controller.backgroundColor
        if #available(iOS 12.0, *) {
            pdfView.pageShadowsEnabled = false
        }
        return pdfView
    }

    private func addMoreInfoButton(in pdfView: PDFView) {
        guard let documentView = pdfView.documentView else { return }
        let buttonHeight: CGFloat = 30.0 / pdfView.scaleFactor
        let buttonWidth: CGFloat = 150.0 / pdfView.scaleFactor
        let buttonFontSize: CGFloat = 17.0 / pdfView.scaleFactor
        let yOrigin: CGFloat = documentView.bounds.height
        let xOrigin: CGFloat = (documentView.bounds.width - buttonWidth) / 2.0

        let button: UIButton = .init(frame: CGRect(x: xOrigin, y: yOrigin, width: buttonWidth, height: buttonHeight))
        button.titleLabel?.adjustsFontForContentSizeCategory = false
        button.contentHorizontalAlignment = .center
        button.setTitleColor(Appearance.Button.Tertiary.titleColor, for: .normal)
        button.backgroundColor = Appearance.Button.Tertiary.backgroundColor
        button.titleLabel?.font = Appearance.Button.font.withSize(buttonFontSize)
        button.addTarget(self, action: #selector(didTouchPdfButton), for: .touchUpInside)

        if #available(iOS 13.0, *), let image = UIImage(systemName: "arrow.up.right.square.fill")?.withRenderingMode(.alwaysTemplate) {
            let imageAttachment: NSTextAttachment = .init()
            let imageSize: CGSize = .init(width: image.size.width / pdfView.scaleFactor, height: image.size.height / pdfView.scaleFactor)
            imageAttachment.image = image.resize(to: imageSize)
            let fullString: NSMutableAttributedString = .init(string: "\("common.readMore".localized) ")
            fullString.append(NSAttributedString(attachment: imageAttachment))
            button.setAttributedTitle(fullString, for: .normal)
        } else {
            button.setTitle("common.readMore".localized, for: .normal)
        }

        pdfView.documentView?.addSubview(button)
        pdfView.documentView?.frame = CGRect(origin: documentView.frame.origin, size: CGSize(width: documentView.frame.width, height: documentView.frame.height + buttonHeight))
    }

    @objc private func didTouchPdfButton() {
        moreInfoUrl?.openInSafari()
    }
}
