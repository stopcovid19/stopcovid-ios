// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  FeaturedInfosViewController.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 09/01/2023 - for the TousAntiCovid project.
//

import UIKit

final class FeaturedInfosViewController: CVTableViewController {

    private var featuredInfos: [FeaturedInfo] { FeaturedInfoManager.shared.featuredInfos }
    private var thumbnails: [String: UIImage] { FeaturedInfoManager.shared.thumbnails }
    private var ressources: [String: URL] { FeaturedInfoManager.shared.ressources }
    private let deinitBlock: () -> ()
    private let didTouchFeaturedInfo: (_ featuredInfo: FeaturedInfo, _ localUrl: URL?) -> ()

    init(didTouchFeaturedInfo: @escaping (_ featuredInfo: FeaturedInfo, _ localUrl: URL?) -> (), deinitBlock: @escaping () -> ()) {
        self.didTouchFeaturedInfo = didTouchFeaturedInfo
        self.deinitBlock = deinitBlock
        super.init(style: .plain)
    }

    required init?(coder: NSCoder) {
        fatalError("Must use default init() method.")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        initUI()
        updateTitle()
        reloadUI()
        addObservers()
    }

    deinit {
        removeObservers()
        deinitBlock()
    }

    private func initUI() {
        addHeaderView(height: 10.0)
        tableView.backgroundColor = Appearance.Controller.cardTableViewBackgroundColor
        tableView.showsVerticalScrollIndicator = false
        tableView.separatorStyle = .singleLine
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "common.close".localized, style: .plain, target: self, action: #selector(didTouchCloseButton))
        navigationItem.leftBarButtonItem?.accessibilityHint = "accessibility.closeModal.zGesture".localized
    }

    @objc private func didTouchCloseButton() {
        dismiss(animated: true, completion: nil)
    }

    private func updateTitle() {
        title = "home.featuredInfoSection.title".localized
    }

    private func addObservers() {
        FeaturedInfoManager.shared.addObserver(self)
    }

    private func removeObservers() {
        FeaturedInfoManager.shared.removeObserver(self)
    }

    override func createSections() -> [CVSection] {
        makeSections {
            if let featuredInfoSection = featuredInfoSection() {
                featuredInfoSection
            } else {
                CVSection.Empty()
            }
        }
    }
}

extension FeaturedInfosViewController: FeaturedInfoChangesObserver {

    func featuredInfoDidUpdate() {
        reloadUI()
    }

}

// MARK: Feature Info Section
private extension FeaturedInfosViewController {

    func featuredInfoSection() -> CVSection? {
        guard let featuredInfoRows = featuredInfoRows() else { return nil }
        return CVSection { featuredInfoRows }
    }

    func featuredInfoRows() -> [CVRow]? {
        guard !featuredInfos.isEmpty else { return nil }
        return featuredInfos.map { featuredInfo in
            featuredInfoRow(featuredInfo, thumbnail: thumbnails.first(where: { featuredInfo.id == $0.key })?.value, ressource: ressources.first(where: { featuredInfo.id == $0.key })?.value)
        }
    }

    func featuredInfoRow(_ featuredInfo: FeaturedInfo, thumbnail: UIImage?, ressource: URL?) -> CVRow {
        CVRow(title: featuredInfo.title,
              subtitle: featuredInfo.hint,
              image:  thumbnail ?? UIImage.imageWithColor(color: Appearance.tintSecondaryColor, size: CGSize(width: 1.0, height: 1.0)),
              xibName: .featuredInfoViewCell,
              theme: .init(backgroundColor: Appearance.Cell.cardBackgroundColor,
                           topInset: Appearance.Cell.Inset.small,
                           bottomInset: Appearance.Cell.Inset.small,
                           leftInset: Appearance.Cell.Inset.normal,
                           rightInset: Appearance.Cell.Inset.normal,
                           textAlignment: .natural,
                           titleFont: { Appearance.Cell.Text.titleFont },
                           titleColor: Appearance.Cell.Text.titleColor,
                           titleLinesCount: 2,
                           imageRatio: Appearance.Cell.Image.a4Ratio),
              selectionAction: { [weak self] _ in
            self?.didTouchFeaturedInfo(featuredInfo, ressource)
        })
    }

}
