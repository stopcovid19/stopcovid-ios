// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  WalletCertificateVerificationCoordinator.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 17/03/2021 - for the TousAntiCovid project.
//

import UIKit
import PKHUD

final class WalletCertificateVerificationCoordinator: Coordinator {

    weak var parent: Coordinator?
    var childCoordinators: [Coordinator] = []

    private weak var navigationController: UINavigationController?
    private weak var presentingController: UIViewController?

    init(presentingController: UIViewController?, parent: Coordinator) {
        self.presentingController = presentingController
        self.parent = parent
        start()
    }

    private func start() {
        let controller: UIViewController = FlashWalletCodeController.controller(didFlash: { [weak self] doc in
            guard let url = doc else { throw WalletError.parsing.error }
            guard let certificate = try WalletManager.shared.extractEuropeanCertificateFrom(url: url) as? EuropeanCertificate else { throw WalletError.parsing.error }
            let jsonController: JsonViewerController = .init(certificate: certificate)
            self?.navigationController?.pushViewController(jsonController, animated: true)
        }, didGetCertificateError: { _, _ in }, deinitBlock: { [weak self] in
            self?.didDeinit()
        })
        let navigationController: CVNavigationController
        navigationController = CVNavigationController(rootViewController: controller)
        self.navigationController = navigationController
        presentingController?.topPresentedController.present(navigationController, animated: true)
    }
}
