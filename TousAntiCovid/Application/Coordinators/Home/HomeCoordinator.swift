// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  HomeCoordinator.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 07/10/2020 - for the TousAntiCovid project.
//

import UIKit
import PKHUD
import StoreKit
import LBBottomSheet
import AppModel

final class HomeCoordinator: NSObject, WindowedCoordinator {

    weak var parent: Coordinator?
    var childCoordinators: [Coordinator]
    var window: UIWindow!
    
    private weak var navigationController: UINavigationController?
    private var isLoadingAppUpdate: Bool = false

    private var animationWindow: UIWindow?

    init(parent: Coordinator) {
        self.parent = parent
        self.childCoordinators = []
        super.init()
        start()
        addObservers()
    }
    
    deinit {
        removeObservers()
    }
    
    private func start() {
        let outputs: HomeOutputs = .init(didTouchAppUpdate: { [weak self] in
            self?.updateApp()
        }, didTouchManageData: { [weak self] in
            self?.showManageData()
        }, didTouchPrivacy: { [weak self] in
            self?.showPrivacy()
        }, didTouchAbout: { [weak self] in
            self?.showAbout()
        }, didTouchHealth: { [weak self] in
            self?.showMyHealth()
        }, didTouchInfo: { [weak self] info in
            self?.showInfo(info)
        }, didTouchKeyFigure: { [weak self] keyFigure in
            self?.showKeyFigureDetailFor(keyFigure: keyFigure)
        }, didTouchWarningKeyFigures: { [weak self] in
            self?.showWarningKeyFigureBottomSheet()
        }, didTouchKeyFigures: { [weak self] in
            self?.showKeyFigures(openOnFavorites: !KeyFiguresManager.shared.favoriteKeyFigures.isEmpty)
        }, didTouchFavoriteKeyFigures: { [weak self] in
            self?.showKeyFigures(openOnFavorites: false)
        }, didTouchComparisonChart: { [weak self] in
            self?.showKeyFiguresComparison()
        }, didTouchKeyFiguresMap: { [weak self] in
            if #available(iOS 13.0, *) {
                self?.showKeyFiguresMap()
            }
        }, didTouchComparisonChartSharing: { [weak self] image in
            self?.showSharingScreen(for: image)
        }, didTouchUsefulLinks: { [weak self] in
            self?.showUsefulLinks()
        }, didTouchVaccination: { [weak self] in
            self?.showVaccination()
        }, didTouchSanitaryCertificates: { [weak self] url in
            self?.showSanitaryCertificates(url)
        }, didTouchUniversalQrScan: { [weak self] in
            self?.showUniversalQrScan()
        }, didTouchCertificate: { [weak self] certificate in
            self?.showCodeFullscreen(certificate)
        }, didTouchFeaturedInfo: { [weak self] featuredInfo, localUrl in
            self?.showFeaturedInfo(featuredInfo: featuredInfo, localUrl: localUrl)
        }, didTouchSeeAllFeaturedInfos: { [weak self] in
            self?.showFeaturedInfos()
        }, showUserLanguage: { [weak self] in
            self?.showUserLanguage()
        }, didTouchUrgentDgs: { [weak self] in
            self?.showUrgentDgs()
        }, showNoTracingExplanationsBottomSheet: { [weak self] in
            self?.showNoTracingExplanationsBottomSheet()
        }, deinitBlock: { [weak self] in
            self?.didDeinit()
        })

        let navigationChildController: UIViewController = CVNavigationChildController.controller(homeController(outputs: outputs))
        let navigationController: UINavigationController = CVNavigationController(rootViewController: navigationChildController)
        self.navigationController = navigationController
        createWindow(for: navigationController)
        window?.accessibilityViewIsModal = false
    }
    
    private func homeController(outputs: HomeOutputs) -> UIViewController {
        let viewModel: HomeBasedViewModel = .init()
        if #available(iOS 13.0, *) {
            return HomeCollectionViewController(viewModel: viewModel, outputs: outputs)
        } else {
            return HomeViewController(viewModel: viewModel, outputs: outputs)
        }
    }
    
    private func updateApp() {
        guard !isLoadingAppUpdate else { return }
        isLoadingAppUpdate = true
        #if targetEnvironment(simulator)
        URL(string: "https://apps.apple.com/in/app/TousAntiCovid/id\(Constant.appStoreId)")?.openInSafari()
        isLoadingAppUpdate = false
        #else
        let storeViewController = SKStoreProductViewController()
        storeViewController.delegate = self
        let parameters: [String: String] = [SKStoreProductParameterITunesItemIdentifier: Constant.appStoreId]
        HUD.show(.progress)
        storeViewController.loadProduct(withParameters: parameters) { [weak self] loaded, error in
            HUD.hide()
            self?.isLoadingAppUpdate = false
            guard loaded && error == nil else {
                URL(string: "itms-apps://apple.com/app/id\(Constant.appStoreId)")?.openInSafari()
                return
            }
            self?.navigationController?.present(storeViewController, animated: true)
        }
        #endif
    }
    
    private func showUserLanguage() {
        let userLanguageCoordinator: UserLanguageCoordinator = UserLanguageCoordinator(presentingController: navigationController?.topPresentedController, parent: self)
        addChild(coordinator: userLanguageCoordinator)
    }
    
    private func showAbout() {
        let aboutCoordinator: AboutCoordinator = AboutCoordinator(presentingController: navigationController?.topPresentedController, parent: self)
        addChild(coordinator: aboutCoordinator)
    }
    
    private func showUsefulLinks() {
        let linksCoordinator: LinksCoordinator = LinksCoordinator(presentingController: navigationController?.topPresentedController, parent: self)
        addChild(coordinator: linksCoordinator)
    }
    
    private func showPrivacy() {
        let privacyCoordinator: PrivacyCoordinator = PrivacyCoordinator(presentingController: navigationController?.topPresentedController, parent: self)
        addChild(coordinator: privacyCoordinator)
    }
    
    private func showSanitaryCertificates(_ url: URL?) {
        if let coordinator = DeepLinkingManager.shared.walletCoordinator {
            coordinator.processUrl(url: url)
        } else {
            dismissAllModalsIfNeeded { [weak self] in
                guard let self = self else { return }
                let sanitaryCertificatesCoordinator: WalletCoordinator = WalletCoordinator(presentingController: self.navigationController?.topPresentedController,
                                                                                           url: url,
                                                                                           parent: self)
                self.addChild(coordinator: sanitaryCertificatesCoordinator)
            }
        }
    }

    private func dismissAllModalsIfNeeded(_ completion: @escaping () -> ()) {
        guard navigationController?.presentedViewController != nil else {
            completion()
            return
        }
        navigationController?.dismiss(animated: true) { completion() }
    }

    private func showManageData() {
        let manageDataController: UIViewController = ManageDataController(viewModel: ManageDataViewModel())
        let navigationController: UIViewController = CVNavigationController(rootViewController: manageDataController)
        self.navigationController?.present(navigationController, animated: true)
    }
    
    private func showMyHealth() {
        let myHealthCoordinator: MyHealthCoordinator = MyHealthCoordinator(presentingController: navigationController?.topPresentedController, parent: self)
        addChild(coordinator: myHealthCoordinator)
    }
    
    private func showInfo(_ info: Info?) {
        if let info = info {
            let controller: HomeInfoBottomSheetController = .init(content: info) { [weak self] in
                self?.showAllInfo()
            }
            navigationController?.presentAsBottomSheet(controller,
                                                       theme: controller.bottomSheetTheme,
                                                       behavior: controller.bottomSheetBehavior)
        } else {
            showAllInfo()
        }
    }
    
    func showAllInfo() {
        let infoCenterCoordinator: InfoCenterCoordinator = InfoCenterCoordinator(presentingController: navigationController?.topPresentedController, parent: self)
        addChild(coordinator: infoCenterCoordinator)
    }
    
    private func showKeyFigures(openOnFavorites: Bool) {
        let keyFiguresCoordinator: KeyFiguresCoordinator = .init(mode: openOnFavorites ? KeyFiguresController.Mode.favorites : KeyFiguresController.Mode.all, presentingController: navigationController?.topPresentedController, parent: self)
        addChild(coordinator: keyFiguresCoordinator)
    }
    
    private func showKeyFiguresComparison() {
        let comparisonCoordinator: KeyFiguresComparisonCoordinator = .init(presentingController: navigationController, parent: self)
        addChild(coordinator: comparisonCoordinator)
    }

    @available(iOS 13.0,*)
    private func showKeyFiguresMap() {
        let mapCoordinator: KeyFiguresMapCoordinator = .init(presentingController: navigationController, parent: self)
        addChild(coordinator: mapCoordinator)
    }
    
    private func showKeyFigureDetailFor(keyFigure: KeyFigure) {
        let detailCoordinator: KeyFigureDetailCoordinator = KeyFigureDetailCoordinator(presentingController: navigationController, parent: self, keyFigure: keyFigure)
        addChild(coordinator: detailCoordinator)
    }
    
    private func showVaccination() {
        let vaccinationCoordinator: VaccinationCoordinator = VaccinationCoordinator(presentingController: navigationController?.topViewController, parent: self)
        addChild(coordinator: vaccinationCoordinator)
    }

    private func showFeaturedInfo(featuredInfo: FeaturedInfo, localUrl: URL?) {
        let controller: UIViewController = FileViewerViewController(featuredInfo: featuredInfo, localUrl: localUrl)
        let navigationController: UINavigationController = CVNavigationController(rootViewController: controller)
        self.navigationController?.present(navigationController, animated: true)
    }

    private func showFeaturedInfos() {
        let featuredInfoCoordinator: FeaturedInfoCoordinator = .init(presentingController: navigationController?.topPresentedController, parent: self)
        addChild(coordinator: featuredInfoCoordinator)
    }
    
    private func showNoTracingExplanationsBottomSheet() {
        let bottomSheet: BottomSheetAlertController = .init(
            title: "noTracingExplanations.title".localized,
            message: "noTracingExplanations.message".localized,
            okTitle: "noTracingExplanations.button".localized,
            okButtonStyle: .secondary)
        bottomSheet.show()
    }
    
    private func showWarningKeyFigureBottomSheet() {
        let bottomSheet: BottomSheetAlertController = .init(title: "",
                                                            message: "home.infoSection.warning".localized,
                                                            okTitle: "common.ok".localized,
                                                            okButtonStyle: .secondary)
        bottomSheet.show()
    }

    private func createAnimationWindow(for controller: UIViewController) -> UIWindow {
        let animationWindow = UIWindow(frame: UIScreen.main.bounds)
        animationWindow.backgroundColor = .clear
        animationWindow.rootViewController = controller
        animationWindow.alpha = 0.0
        animationWindow.makeKeyAndVisible()
        return animationWindow
    }

    private func showUniversalQrScan() {
        guard childCoordinators.first(where: { $0 is UniversalQrScanCoordinator }).isNil else { return }
        let coordinator: UniversalQrScanCoordinator = UniversalQrScanCoordinator(presentingController: navigationController?.topPresentedController, parent: self)
        addChild(coordinator: coordinator)
    }

    private func showCodeFullscreen(_ certificate: WalletCertificate) {
        if let coordinator = childCoordinators.first(where: { $0 is FullscreenCertificateCoordinator }) as? FullscreenCertificateCoordinator {
            coordinator.updateCertificate(certificate)
        } else if let walletCoordinator = childCoordinators.first(where: { $0 is WalletCoordinator }) as? WalletCoordinator,
                  let coordinator = walletCoordinator.childCoordinators.first(where: { $0 is FullscreenCertificateCoordinator }) as? FullscreenCertificateCoordinator {
            coordinator.updateCertificate(certificate)
        } else  {
            let coordinator: FullscreenCertificateCoordinator = FullscreenCertificateCoordinator(presentingController: navigationController?.topPresentedController, parent: self, certificate: certificate)
            addChild(coordinator: coordinator)
        }
    }
    
    private func showUrgentDgs() {
        let urgentDgsCoordinator: UrgentDgsCoordinator = UrgentDgsCoordinator(presentingController: navigationController?.topViewController, parent: self)
        addChild(coordinator: urgentDgsCoordinator)
    }
}

extension HomeCoordinator {
    
    private func addObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(dismissAllAndShowRecommandations), name: .dismissAllAndShowRecommandations, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(shouldShowStorageAlert(_:)), name: .shouldShowStorageAlert, object: nil)
    }

    private func removeObservers() {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc private func dismissAllAndShowRecommandations() {
        navigationController?.dismiss(animated: true) {
            self.showMyHealth()
        }
    }
    
    @objc private func shouldShowStorageAlert(_ notification: Notification) {
        guard let alertType = notification.object as? StorageAlertManager.StorageAlertType else { return }
        let bottomSheetAlert: BottomSheetAlertController = .init(
            title: nil,
            message: alertType.localizedDescription,
            image: Asset.Images.tacHorizontalAlert.image,
            imageTintColor: Appearance.tintColor,
            okTitle: alertType.localizedConfirmationButtonTitle)
        bottomSheetAlert.show()
    }
}

extension HomeCoordinator: SKStoreProductViewControllerDelegate {
    
    func productViewControllerDidFinish(_ viewController: SKStoreProductViewController) {
        navigationController?.dismiss(animated: true)
    }
    
}

// MARK: - sharing related functions
private extension HomeCoordinator {
    func showSharingScreen(for chartImage: UIImage?) {
        navigationController?.showSharingController(for: [chartImage])
    }
}
