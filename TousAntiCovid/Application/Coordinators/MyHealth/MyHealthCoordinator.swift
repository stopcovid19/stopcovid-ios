// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  MyHealthCoordinator.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 09/04/2020 - for the TousAntiCovid project.
//

import UIKit
import AppModel

final class MyHealthCoordinator: Coordinator {

    weak var parent: Coordinator?
    var childCoordinators: [Coordinator] = []
    
    private weak var navigationController: UINavigationController?
    private weak var presentingController: UIViewController?
    
    init(presentingController: UIViewController?, parent: Coordinator) {
        self.presentingController = presentingController
        self.parent = parent
        start()
    }
    
    private func start() {
        let navigationController: CVNavigationController = CVNavigationController(rootViewController: MyHealthController(viewModel: MyHealthViewModel(), didTouchLink: { [weak self] link in
            self?.showLinkAction(for: link)
        }))
        self.navigationController = navigationController
        presentingController?.present(navigationController, animated: true)
    }

    private func showLinkAction(for link: ParagraphLink) {
        switch link.action {
        case let .web(urlToLocalized):
            URL(string: urlToLocalized.localized)?.openInSafari()
        case .gesturesController:
            showGestures()
        }
    }
    
    private func showGestures() {
        let gesturesCoordinator: GesturesCoordinator = GesturesCoordinator(presentingController: navigationController, parent: self)
        addChild(coordinator: gesturesCoordinator)
    }
    
}
