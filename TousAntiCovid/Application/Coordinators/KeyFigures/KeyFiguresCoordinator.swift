// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  KeyFiguresCoordinator.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 09/04/2020 - for the TousAntiCovid project.
//

import UIKit

final class KeyFiguresCoordinator: Coordinator {
    
    weak var parent: Coordinator?
    var childCoordinators: [Coordinator] = []
    let mode: KeyFiguresController.Mode
    
    private weak var navigationController: UINavigationController?
    private weak var presentingController: UIViewController?
    
    init(mode: KeyFiguresController.Mode, presentingController: UIViewController?, parent: Coordinator) {
        self.presentingController = presentingController
        self.parent = parent
        self.mode = mode
        start()
    }
    
    private func start() {
        let innerController: UIViewController = KeyFiguresController(mode: mode, didTouchReadExplanationsNow: { [weak self] in
            self?.showKeyFiguresExplanations()
        }, didTouchKeyFigure: { [weak self] keyFigure in
            self?.showKeyFigureDetailFor(keyFigure: keyFigure)
        }, didTouchCompare: { [weak self] in
            self?.showKeyFigureComparison()
        }, didTouchReorder: { [weak self] in
            self?.showKeyFigureFavoritesList()
        }, didTouchWarningKeyFigures: { [weak self] in
            self?.showWarningKeyFigures()
        }, deinitBlock: { [weak self] in
            self?.didDeinit()
        })
        let navigationController: CVNavigationController = CVNavigationController(rootViewController: innerController)
        self.navigationController = navigationController
        presentingController?.present(navigationController, animated: true)
        RatingsManager.shared.didOpenKeyFigures()
    }
    
    private func showKeyFiguresExplanations() {
        let explanationsController: KeyFiguresExplanationsController = KeyFiguresExplanationsController()
        navigationController?.pushViewController(explanationsController, animated: true)
    }
    
    private func showKeyFigureDetailFor(keyFigure: KeyFigure) {
        let detailCoordinator: KeyFigureDetailCoordinator = KeyFigureDetailCoordinator(presentingController: navigationController, parent: self, keyFigure: keyFigure)
        addChild(coordinator: detailCoordinator)
    }
    
    private func showKeyFigureComparison() {
        let comparisonCoordinator: KeyFiguresComparisonCoordinator = .init(presentingController: navigationController, parent: self)
        addChild(coordinator: comparisonCoordinator)
    }

    private func showKeyFigureFavoritesList() {
        let controller: KeyFiguresFavoritesListController = .init(keyFigures: KeyFiguresManager.shared.favoriteKeyFigures) { sortedKeyFigures in
            KeyFiguresManager.shared.reorderFavoriteKeyFigures(sortedKeyFigures)
        } deinitBlock: {}
        let navController: UINavigationController = .init(rootViewController: controller)
        navigationController?.present(navController, animated: true)
    }

    private func showWarningKeyFigures() {
        let bottomSheet: BottomSheetAlertController = .init(title: "",
                                                            message: "home.infoSection.warning".localized,
                                                            okTitle: "common.ok".localized,
                                                            okButtonStyle: .secondary)
        bottomSheet.show()
    }
}
