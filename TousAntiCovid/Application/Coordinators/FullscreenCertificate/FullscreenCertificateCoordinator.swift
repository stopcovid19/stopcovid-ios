// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  FullscreenCertificateCoordinator.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 09/04/2020 - for the TousAntiCovid project.
//

import UIKit
import ServerSDK
import LBBottomSheet

final class FullscreenCertificateCoordinator: Coordinator {
    
    weak var parent: Coordinator?
    var childCoordinators: [Coordinator] = []
    
    private var certificate: WalletCertificate
    private weak var presentingController: UIViewController?
    private weak var navigationController: UINavigationController?
    private weak var fullscreenController: FullscreenCertificateViewController?

    init(presentingController: UIViewController?, parent: Coordinator, certificate: WalletCertificate) {
        self.presentingController = presentingController
        self.parent = parent
        self.certificate = certificate
        start()
    }

    func updateCertificate(_ certificate: WalletCertificate) {
        self.certificate = certificate
        fullscreenController?.updateCertificate(certificate)
    }

    private func start() {
        let controller: FullscreenCertificateViewController = FullscreenCertificateViewController(certificate: certificate, didTouchShareCertificate: { [weak self] confirmationHandler in
            self?.showSharingConfirmationController(confirmationHandler: confirmationHandler)
        }, didTouchShowOptions: { [weak self] brightnessParamDidChange, shareConfirmationHandler in
            self?.showOptionsController(brightnessParamDidChange: brightnessParamDidChange, shareConfirmationHandler: shareConfirmationHandler)
        }, dismissBlock: { [weak self] in
            self?.presentingController?.dismiss(animated: true)
        }, deinitBlock: { [weak self] in
            self?.didDeinit()
        })
        fullscreenController = controller
        let navigationController: CVNavigationController = CVNavigationController(rootViewController: controller)
        navigationController.modalTransitionStyle = .crossDissolve
        navigationController.modalPresentationStyle = .fullScreen
        self.navigationController = navigationController
        presentingController?.present(navigationController, animated: true)
    }
    
    private func showSharingConfirmationController(confirmationHandler: @escaping () -> ()) {
        let bottomSheet: BottomSheetAlertController = .init(
            title: "certificateSharingController.title".localized,
            message: "certificateSharingController.message".localized,
            okTitle: "common.confirm".localized,
            cancelTitle: "common.cancel".localized,
            interfaceStyle: .light) {
                confirmationHandler()
            }
        bottomSheet.show()
    }
    
    private func showOptionsController(brightnessParamDidChange: @escaping (_ activated: Bool) -> (), shareConfirmationHandler: @escaping () -> ()) {
        let optionsController: FullscreenOptionsController = .init() { activated in
            brightnessParamDidChange(activated)
        } didTouchShareCertificateButton: { [weak self] in
            self?.showSharingConfirmationController {
                shareConfirmationHandler()
            }
        }
        var behavior: BottomSheetController.Behavior = .init()
        behavior.bottomSheetAnimationConfiguration = Appearance.BottomSheet.bottomSheetAnimationConfiguration
        
        if #available(iOS 13.0, *) {
            navigationController?.presentAsBottomSheet(optionsController, theme: optionsController.bottomSheetTheme, behavior: behavior).overrideUserInterfaceStyle = .light
        } else {
            navigationController?.presentAsBottomSheet(optionsController, theme: optionsController.bottomSheetTheme, behavior: behavior)
        }
    }
}
