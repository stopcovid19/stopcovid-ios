// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  Constant.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 08/04/2020 - for the TousAntiCovid project.
//

import UIKit
import ServerSDK

enum Constant {
    static let appStoreId: String = "1511279125"

    enum BottomSheet {
        enum AnimationConfiguration {
            static let duration: Double = 0.6
            static let damping: Double = 0.8
            static let velocity: Double = 0.5
            static let options: [UIView.AnimationOptions] = [.curveEaseInOut]
        }
    }
    // Every 6 hours
    static let timeIntervalToFetchConfig: TimeInterval = 6 * 3600
    

    #if targetEnvironment(simulator)
    static let isSimulator: Bool = true
    #else
    static let isSimulator: Bool = false
    #endif

    #if DEBUG
    static let isDebug: Bool = true
    #else
    static let isDebug: Bool = false
    #endif

    enum BBCodeTag {
        static let bold: String = "b"
        static let underlined: String = "u"
        static let highlighted: String = "h"
    }

    enum ShortcutItem: String {
        case qrScan = "appShortcut.qrScan"
        case favoriteDcc = "appShortcut.favoriteDcc"
        case warningDeleteApp = "appShortcut.WarningDeleteApp"
    }
    
    enum Server {
        static let staticResourcesRootDomain: String = "app-static.tousanticovid.gouv.fr"
        
        static var certificates: [Data] { ["certigna-root", "certigna-services"].compactMap { Bundle.main.fileDataFor(fileName: $0, ofType: "pem") } }
        
        static var dccCertsUrl: URL = .init(string: "https://\(Constant.Server.staticResourcesRootDomain)/json/dsc/dcc-certs.json")!
        
        
        static var euTagsUrl: URL = .init(string: "https://\(Constant.Server.staticResourcesRootDomain)/json/ref-data/tags-ue-list-codes.json")!
        static var multiPassAggregateUrl: URL? { URL(string: "https://dcclight.tousanticovid.gouv.fr/api/v1/aggregate")! }
        

        // TODO: Will be deleted after migration fullfill
        // TODO: Update Server/Constant.swift jsonVersion as well
        static let jsonVersion: Int = 45
        static let baseJsonUrl: String = "https://\(staticResourcesRootDomain)/json/version-\(jsonVersion)/Config" // This file must be fetched from the app server to get the correct time in the answer as we use it to check if the device has correct time settings.
        static let configUrl: URL = URL(string: "\(baseJsonUrl)/config.json")!
        
        
    }
    
    enum Map {
        static let ileDeFranceDeptNbs: [String] = ["92", "93", "94", "75"]
    }
}

typealias JSON = [String: Any]
typealias Headers = [String: String]
