// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  NSMutableAttributedString+Extension.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 15/02/2021 - for the TousAntiCovid project.
//

import UIKit

extension NSMutableAttributedString {
    func color(text: String, color: UIColor) {
        let paramsRange: NSRange = (string as NSString).range(of: text)
        addAttributes([.foregroundColor: color], range: paramsRange)
    }
}

extension NSMutableAttributedString {
    convenience init(string: String, tag: String, defaultAttributes: [NSAttributedString.Key: Any]? = nil, tagAttributes: [NSAttributedString.Key: Any]) {
        var text: String = string
        let ranges: [NSRange] = text.parseBBCode(tag)
        self.init(string: text, attributes: defaultAttributes)
        for range in ranges {
            addAttributes(tagAttributes, range: range)
        }
    }
}

extension String {
    func ranges(of substring: String, options: CompareOptions = [], locale: Locale? = nil) -> [Range<Index>] {
        var ranges: [Range<Index>] = []
        while let range = range(of: substring, options: options, range: (ranges.last?.upperBound ?? self.startIndex)..<self.endIndex, locale: locale) {
            ranges.append(range)
        }
        return ranges
    }
}

private extension String {
    mutating func parseBBCode(_ tag: String) -> [NSRange] {
        let openingTag: String = "<\(tag)>"
        let closingTag: String = "</\(tag)>"
        if contains(openingTag) && contains(closingTag) {
            let scanner: Scanner = .init(string: self)
            scanner.charactersToBeSkipped = CharacterSet(charactersIn: "")
            var words: [String] = [String]()
            var ranges: [NSRange] = [NSRange]()
            while scanBBCodeOpeningTag(scanner, tag: tag) {
                if let string = scanBBCodeClosingTag(scanner, tag: tag) {
                    words.append(string)
                }
            }
            self = replacingOccurrences(of: openingTag, with: "").replacingOccurrences(of: closingTag, with: "")
            for word in words {
                ranges.append(contentsOf: self.ranges(of: word).map { NSRange($0, in: self) })
            }
            return ranges
        } else {
            return []
        }
    }

    func scanBBCodeOpeningTag(_ scanner: Scanner, tag: String) -> Bool {
        let openingTag: String = "<\(tag)>"
        scanner.scanUpTo(openingTag, into: nil)
        if !scanner.scanString(openingTag, into: nil) { return false }
        return true
    }

    func scanBBCodeClosingTag(_ scanner: Scanner, tag: String) -> String? {
        let closingTag: String = "</\(tag)>"
        var readString: NSString?
        scanner.scanUpTo(closingTag, into: &readString)
        if !scanner.scanString(closingTag, into: nil) { return nil }
        return readString as String?
    }
}
