// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  CVGroup.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 14/03/2022 - for the TousAntiCovid project.
//

import Foundation
import UIKit

struct CVGroup {
    enum HorizontalLayoutMode {
        case adaptive
        case fixed(_ widthFraction: Double)
    }
    
    enum Orientation {
        case vertical(_ sectionInsets: NSDirectionalEdgeInsets, _ headerInsets: UIEdgeInsets)
        case horizontal(_ mode: HorizontalLayoutMode)
        
        @available(iOS 13.0, *)
        func layout(margin: CGFloat) -> NSCollectionLayoutSection {
            switch self {
            case .vertical(let insets, _):
                return .fullWidthLayout(insets: insets)
            case .horizontal(let mode):
                return .horizontalScrollableLayout(margin: margin, mode: mode)
            }
        }
        
        var insets: UIEdgeInsets {
            switch self {
            case .vertical(_, let headerInsets):
                return headerInsets
            case .horizontal:
                return .zero
            }
        }
        
        var isVertical: Bool {
            switch self {
            case .vertical : return true
            default: return false
            }
        }
    }
    
    let id: String
    var title: String? = nil
    var subtitle: String? = nil
    var orientation: Orientation = .vertical(.zero, UIEdgeInsets(top: .zero, left: Appearance.Cell.leftMargin, bottom: .zero, right: Appearance.Cell.rightMargin))
    let rows: [CVRow]
    var primaryActionTitle: String? = nil
    var primaryAction: (() -> ())? = nil
    var willDisplay: ((_ view: UIView) -> ())?
}

@available(iOS 13.0, *)
extension CVGroup {
    func mapToCollectionSection(layoutMargin: CGFloat) -> CVCollectionSection? {
        guard !rows.isEmpty else { return nil }
        if let actionTitle = primaryActionTitle {
            let header: CVFooterHeaderSection = .init(isHeader: true,
                                                      title: title,
                                                      subtitle: actionTitle,
                                                      xibName: .actionCollectionSectionHeader,
                                                      theme: .init(leftInset: orientation.insets.left,
                                                                   rightInset: orientation.insets.right,
                                                                   subtitleFont: { Appearance.Cell.Text.headerButtonFont },
                                                                   axis: UIApplication.shared.preferredContentSizeCategory >= .accessibilityExtraLarge || UIApplication.shared.preferredContentSizeCategory >= .extraLarge ? .vertical : .horizontal
                                                                  )) {
                self.primaryAction?()
            } willDisplay: { view in
                self.willDisplay?(view)
            }
            return CVCollectionSection(id: id, header: header, rows: rows, orientation: orientation, layoutMargin: layoutMargin)
        } else {
            return CVCollectionSection(id: id, title: title, subtitle: subtitle, rows: rows, orientation: orientation, layoutMargin: layoutMargin)
        }
    }
}

extension CVGroup {
    func mapToTableSection() -> CVSection? {
        guard !rows.isEmpty else { return nil }
        if let actionTitle = primaryActionTitle {
            let header: CVFooterHeaderSection = .init(isHeader: true,
                                                      title: title,
                                                      subtitle: actionTitle,
                                                      xibName: .actionSectionHeader,
                                                      theme: .init(subtitleFont: { Appearance.Cell.Text.headerButtonFont })) {
                self.primaryAction?()
            }
            return CVSection(header: header, rows: rows)
        } else {
            return CVSection(title: title, subtitle: subtitle, rows: rows)
        }
    }
}
