// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  HeaderWithActionView.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 23/12/2021 - for the TousAntiCovid project.
//

import UIKit

final class HeaderWithActionView: CVHeaderFooterSectionView {
    @IBOutlet private weak var actionButton: UIButton!
    @IBOutlet weak var contentStackView: UIStackView!
    
    @IBAction private func buttonPressed() {
        currentAssociatedHeaderSection?.selectionAction?()
    }
    
    override func setup(with headerSection: CVFooterHeaderSection) {
        if let subtitle = headerSection.subtitle {
            let attributedSubtitle: NSAttributedString = .init(string: subtitle, attributes: [.font: headerSection.theme.subtitleFont()])
            actionButton.setAttributedTitle(attributedSubtitle, for: .normal)
        }
        super.setup(with: headerSection)
        switch headerSection.theme.axis {
        case .vertical:
            contentStackView.axis = .vertical
            contentStackView.alignment = .leading
        default:
            contentStackView.axis = .horizontal
            contentStackView.alignment = .fill
        }
        setupTheme()
    }
    
    override func setupAccessibility(isHeader: Bool) {
        if let buttonTitle = actionButton.attributedTitle(for: .normal)?.string, !buttonTitle.isEmpty {
            cvTitleLabel?.isAccessibilityElement = true
            cvTitleLabel?.accessibilityTraits = isHeader ? .header : .staticText
            cvSubtitleLabel?.isAccessibilityElement = false
            actionButton?.isAccessibilityElement = true
        } else {
            super.setupAccessibility(isHeader: isHeader)
        }
    }
}

// MARK: - Private functions
private extension HeaderWithActionView {
    func setupTheme() {
        actionButton.tintColor = Appearance.tintColor
    }
}
 
