// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  CVCollectionSection.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 11/03/2022 - for the TousAntiCovid project.
//

import UIKit

@available(iOS 13.0, *)
struct CVCollectionSection {
    private(set) var id: String
    private(set) var rows: [CVRow]
    private(set) var header: CVFooterHeaderSection? = nil
    private(set) var layout: NSCollectionLayoutSection?
    
    init(id: String, title: String? = nil, subtitle: String? = nil, rows: [CVRow], orientation: CVGroup.Orientation, layoutMargin: CGFloat, willDisplay: ((_ headerView: UIView) -> ())? = nil) {
        self.id = id
        self.rows = rows
        if title != nil || subtitle != nil {
            header = CVFooterHeaderSection.collectionHeader(title: title, subtitle: subtitle)
        }
        layout = sectionLayout(for: orientation, with: header, margin: layoutMargin)
        header?.willDisplay = willDisplay
    }
    
    init(id: String, header: CVFooterHeaderSection, rows: [CVRow], orientation: CVGroup.Orientation, layoutMargin: CGFloat) {
        self.id = id
        self.rows = rows
        self.header = header
        layout = sectionLayout(for: orientation, with: header, margin: layoutMargin)
    }
    
    func supplementarySection(for elementKind: String) -> CVFooterHeaderSection? {
        switch elementKind {
        case UICollectionView.elementKindSectionHeader:
            return header
        default:
            return nil
        }
    }
    
    func willDisplay(for elementKind: String, view: CVCollectionFooterHeaderView) {
        supplementarySection(for: elementKind)?.willDisplay?(view)
    }
}


@available(iOS 13.0, *)
extension CVCollectionSection: Hashable {
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
        hasher.combine(header)
    }
    static func == (lhs: CVCollectionSection, rhs: CVCollectionSection) -> Bool { lhs.id == rhs.id }
}

@available(iOS 13.0, *)
private extension CVCollectionSection {
    func sectionLayout(for orientation: CVGroup.Orientation, with header: CVFooterHeaderSection?, margin: CGFloat) -> NSCollectionLayoutSection {
        let supplementarySize: NSCollectionLayoutSize = .init(
            widthDimension: .fractionalWidth(1.0),
            heightDimension: .estimated(44.0)
        )
        let layout: NSCollectionLayoutSection = orientation.layout(margin: margin)
        if header != nil {
            let header: NSCollectionLayoutBoundarySupplementaryItem = .init(
                layoutSize: supplementarySize,
                elementKind: UICollectionView.elementKindSectionHeader,
                alignment: .topLeading)
            layout.boundarySupplementaryItems = [header]
        }
        return layout
    }
}
