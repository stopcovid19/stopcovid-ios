// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  CVCollectionCell.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 11/03/2022 - for the TousAntiCovid project.
//

import UIKit

@available(iOS 13.0, *)
class CVCollectionCell: UICollectionViewCell {
    var contentCell: CVCell? { contentView.subviews.first as? CVCell }
    
    private var shouldBeSquareWithFullHeight: Bool = false
    private var separator: UIView?
    
    override var isHighlighted: Bool {
        didSet { contentCell?.setHighlighted(isHighlighted) }
    }
    
    func setup(with row: CVRow) {
        guard let view = Bundle.main.loadNibNamed(row.xibName.rawValue, owner: nil, options: nil)?.first as? CVCell else { return }
        view.setup(with: row)
        shouldBeSquareWithFullHeight = row.xibName == .seeAllCardCell
        contentView.subviews.first?.removeFromSuperview()
        contentView.addConstrainedSubview(view)
        isUserInteractionEnabled = row.enabled
        setupSeparator(with: row.theme)
    }
    
    override func systemLayoutSizeFitting(_ targetSize: CGSize, withHorizontalFittingPriority horizontalFittingPriority: UILayoutPriority, verticalFittingPriority: UILayoutPriority) -> CGSize {
        let size: CGSize = super.systemLayoutSizeFitting(targetSize, withHorizontalFittingPriority: horizontalFittingPriority, verticalFittingPriority: verticalFittingPriority)
        if shouldBeSquareWithFullHeight {
            return CGSize(width: superview?.frame.height ?? size.width, height: superview?.frame.height ?? size.height)
        } else {
            return size
        }
    }
}

@available(iOS 13.0, *)
private extension CVCollectionCell {
    func setupSeparator(with theme: CVRow.Theme) {
        let leftInset: CGFloat? = theme.separatorLeftInset
        let rightInset: CGFloat? = theme.separatorRightInset
        if leftInset == nil && rightInset == nil {
            separator?.isHidden = true
        } else if separator == nil {
            let subview: UIView = .init()
            addSubview(subview)
            subview.translatesAutoresizingMaskIntoConstraints = false
            subview.bottomAnchor.constraint(equalTo: bottomAnchor, constant: .zero).isActive = true
            subview.leadingAnchor.constraint(equalTo: leadingAnchor, constant: leftInset ?? .zero).isActive = true
            subview.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -(rightInset ?? .zero)).isActive = true
            subview.heightAnchor.constraint(equalToConstant: 0.5).isActive = true
            subview.backgroundColor = .separator
            separator = subview
        } else {
            separator?.isHidden = false
        }
    }
}
