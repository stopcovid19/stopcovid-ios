// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  CVRow.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 08/04/2020 - for the TousAntiCovid project.
//

import UIKit

struct CVRow {

    struct Theme: Hashable {
        var backgroundColor: UIColor?
        var topInset: CGFloat?
        var bottomInset: CGFloat?
        var leftInset: CGFloat?
        var rightInset: CGFloat?
        var textAlignment: NSTextAlignment = .center
        var titleFont: (() -> UIFont) = { Appearance.Cell.Text.titleFont }
        var titleHighlightFont: (() -> UIFont) = { Appearance.Cell.Text.titleFont }
        var titleColor: UIColor = Appearance.Cell.Text.titleColor
        var titleLinesCount: Int?
        var titleHighlightColor: UIColor = Asset.Colors.tint.color
        var subtitleFont: (() -> UIFont) = { Appearance.Cell.Text.subtitleFont }
        var subtitleColor: UIColor = Appearance.Cell.Text.subtitleColor
        var subtitleLinesCount: Int?
        var placeholderColor: UIColor = Appearance.Cell.Text.placeholderColor
        var accessoryTextFont: (() -> UIFont?)?
        var accessoryTextColor: UIColor = Appearance.Cell.Text.captionTitleColor
        var imageTintColor: UIColor?
        var imageSize: CGSize?
        var imageRatio: CGFloat?
        var interLabelSpacing: CGFloat?
        var separatorLeftInset: CGFloat?
        var separatorRightInset: CGFloat?
        var buttonStyle: CVButton.Style = .primary
        var maskedCorners: CACornerMask = .all
        var showImageBottomEdging: Bool = false
        var accessoryType: UITableViewCell.AccessoryType? = nil
        
        static func == (lhs: CVRow.Theme, rhs: CVRow.Theme) -> Bool {
            lhs.hashValue == rhs.hashValue
        }
        
        func hash(into hasher: inout Hasher) {
            hasher.combine(backgroundColor)
            hasher.combine(topInset)
            hasher.combine(bottomInset)
            hasher.combine(leftInset)
            hasher.combine(rightInset)
            hasher.combine(textAlignment)
            hasher.combine(titleColor)
            hasher.combine(titleHighlightColor)
            hasher.combine(subtitleColor)
            hasher.combine(subtitleLinesCount)
            hasher.combine(placeholderColor)
            hasher.combine(accessoryTextColor)
            hasher.combine(imageTintColor)
            hasher.combine(imageSize?.height)
            hasher.combine(imageSize?.width)
            hasher.combine(imageRatio)
            hasher.combine(interLabelSpacing)
            hasher.combine(separatorLeftInset)
            hasher.combine(separatorRightInset)
            hasher.combine(buttonStyle)
            hasher.combine(maskedCorners.rawValue)
            hasher.combine(showImageBottomEdging)
            hasher.combine(accessoryType)
        }
    }
    
    var title: String?
    var shortTitle: String?
    var subtitle: String?
    var placeholder: String?
    var accessoryText: String?
    var footerText: String?
    var titleHighlightText: String?
    var image: UIImage?
    var secondaryImage: UIImage?
    var buttonTitle: String?
    var isOn: Bool?
    var segmentsTitles: [String]?
    var selectedSegmentIndex: Int?
    var xibName: XibName.Row
    var contentDesc: String = ""
    var theme: Theme = Theme()
    var enabled: Bool = true
    var associatedValue: Any? = nil
    var textFieldKeyboardType: UIKeyboardType?
    var textFieldReturnKeyType: UIReturnKeyType?
    var textFieldContentType: UITextContentType?
    var textFieldCapitalizationType: UITextAutocapitalizationType?
    var minimumDate: Date?
    var maximumDate: Date?
    var initialDate: Date?
    var datePickerMode: UIDatePicker.Mode?
    var accessibilityDidFocusCell: ((_ cell: CVTableViewCell) -> ())?
    var selectionActionWithCell: ((_ cell: UIView) -> ())?
    var selectionAction: ((_ cell: CVTableViewCell?) -> ())?
    var secondarySelectionAction: (() -> ())?
    var tertiarySelectionAction: (() -> ())?
    var quaternarySelectionAction: (() -> ())?
    var quinarySelectionAction: (() -> ())?
    var senarySelectionAction: (() -> ())?
    var segmentsActions: [() -> ()]?
    var willDisplay: ((_ cell: UIView) -> ())?
    var valueChanged: ((_ value: Any?) -> ())?
    var didValidateValue: ((_ value: Any?, _ cell: CVTableViewCell) -> ())?
    var displayValueForValue: ((_ value: Any?) -> String?)?
    var width: CGFloat?

    var urlsInLabels: [URL] {
        let text: String = [title, subtitle, placeholder, accessoryText, footerText, titleHighlightText, contentDesc].compactMap { $0 }.joined(separator: " ").removingEmojis()
        let types: NSTextCheckingResult.CheckingType = .link
        let detector: NSDataDetector? = try? .init(types: types.rawValue)
        let matches: [NSTextCheckingResult] = detector?.matches(in: text, options: [], range: NSRange(location: 0, length: text.utf16.count)) ?? []
        let matchingStrings: [String] = matches.compactMap { text.substring(in: $0.range) }
        let cleanedMatchingStrings: [String] = matchingStrings.compactMap{ $0.trimmingCharacters(in: CharacterSet(charactersIn: " .")) }
        let urlMatches: [String] = cleanedMatchingStrings.filter { $0 ~> "(?i)https?:\\/.*" }
        return Array(Set(urlMatches.compactMap { URL(string: $0) })).sorted(by: { $0.absoluteString < $1.absoluteString })
    }
    
    static func emptyFor(topInset: CGFloat, bottomInset: CGFloat, showSeparator: Bool = false) -> CVRow {
        CVRow(xibName: .emptyCell,
              theme: CVRow.Theme(topInset: topInset,
                                 bottomInset: bottomInset,
                                 separatorLeftInset: showSeparator ? .zero : nil,
                                 separatorRightInset: showSeparator ? .zero : nil))
    }
    
}

extension CVRow: Hashable {
    func hash(into hasher: inout Hasher) {
        hasher.combine(title)
        hasher.combine(shortTitle)
        hasher.combine(subtitle)
        hasher.combine(placeholder)
        hasher.combine(accessoryText)
        hasher.combine(footerText)
        hasher.combine(titleHighlightText)
        hasher.combine(image)
        hasher.combine(secondaryImage)
        hasher.combine(buttonTitle)
        hasher.combine(isOn)
        hasher.combine(segmentsTitles)
        hasher.combine(selectedSegmentIndex)
        hasher.combine(xibName)
        hasher.combine(contentDesc)
        hasher.combine(theme)
        hasher.combine(enabled)
        hasher.combine(initialDate)
        hasher.combine(datePickerMode)
        hasher.combine(associatedValue as? KeyFigure)
        hasher.combine(associatedValue as? ComparisonChartAssociatedValue)
        hasher.combine(width)
    }
    
    static func == (lhs: CVRow, rhs: CVRow) -> Bool { lhs.hashValue == rhs.hashValue }
}
