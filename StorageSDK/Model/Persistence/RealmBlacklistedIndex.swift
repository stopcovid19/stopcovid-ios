// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  RealmBlacklistedIndex.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 01/03/2022 - for the TousAntiCovid project.
//

import RealmSwift

final class RealmBlacklistedIndex: Object {
    @Persisted(primaryKey: true) var indexString: String // Primary key indexed by default
    
    convenience init(index: String) {
        self.init()
        indexString = index
    }
}
