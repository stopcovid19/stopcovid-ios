// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  StatusWidget.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 21/02/2023 - for the TousAntiCovid project.
//

import SwiftUI
import WidgetKit


// Manage to avoid white widget on previous status widget
struct StatusWidget: Widget {

    private let kind: String = "fr.gouv.stopcovid.ios.Widget.status"

    public var body: some WidgetConfiguration {
        StaticConfiguration(kind: kind, provider: DccProvider()) { entry in
            VStack(alignment: .center) {
                Image("icon_tousAntiCovid")
                    .resizable()
                    .aspectRatio(contentMode: .fit)
            }
        }
        .configurationDisplayName("TousAntiCovid")
        .supportedFamilies([])
    }

}

