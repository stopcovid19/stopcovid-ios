// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  Provides.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 03/03/2023 - for the TousAntiCovid project.
//

@propertyWrapper
public struct Provides<DependencyType> {
    public var wrappedValue: DependencyType? = nil

    public init(wrappedValue: DependencyType? = nil, _ dependencyBuilder: @autoclosure @escaping () -> DependencyType) {
        DependenciesContainer.shared.register { dependencyBuilder() as DependencyType }
    }
}
