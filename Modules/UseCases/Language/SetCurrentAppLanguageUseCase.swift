// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  SetCurrentAppLanguageUseCase.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 20/02/2023 - for the TousAntiCovid project.
//

import AppModel

extension UseCases {
    public static func setCurrentAppLanguage(to language: Language) {
        languageRepository.currentAppLanguage = language
    }
}
