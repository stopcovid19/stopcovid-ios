// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  GetCurrentAppLanguage.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 20/02/2023 - for the TousAntiCovid project.
//

import AppModel

extension UseCases {
    public static func getCurrentAppLanguage(localeLanguageCode: String) -> Language? {
        let supportedLanguages: [Language] = languageRepository.supportedLanguages
        return supportedLanguages.first { $0 == languageRepository.currentAppLanguage } ?? supportedLanguages.first { $0.rawValue == localeLanguageCode }
    }
}
