// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  ReloadLanguageUseCase.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 12/05/2023 - for the TousAntiCovid project.
//


extension UseCases {
    public static func reloadLanguage(_ completion: ((Error?) -> ())? = nil) {
        infoRepository.reloadLanguage() { error in
            completion?(error)
        }
    }
}
