// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  InfoTag.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 12/01/2023 - for the TousAntiCovid project.
//


public struct InfoTag {
    public let id: String
    public let label: String
    public let colorCode: String

    public init(id: String, label: String, colorCode: String) {
        self.id = id
        self.label = label
        self.colorCode = colorCode
    }
}
