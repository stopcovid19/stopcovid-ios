// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  ParagraphSection.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 27/02/2023 - for the TousAntiCovid project.
//

public struct ParagraphSection {
    public let title: String
    public let description: String
    public let link: ParagraphLink?

    public init(title: String, description: String, link: ParagraphLink?) {
        self.title = title
        self.description = description
        self.link = link
    }
}
