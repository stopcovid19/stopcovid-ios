// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  MyHealthStorage.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 27/02/2023 - for the TousAntiCovid project.
//

public class MyHealthStorage: FileStorage {
    public var lastUpdatedDate: Date {
        get { Date(timeIntervalSince1970: lastUpdatedDateMyHealth) }
        set { lastUpdatedDateMyHealth = newValue.timeIntervalSince1970 }
    }

    public let directoryName: String = "MyHealth"
    public let fileName: String = "Risks"

    @UserDefault(key: .lastUpdatedDateMyHealth)
    private var lastUpdatedDateMyHealth: Double = 0.0

    public init() {}
}
