// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  InfoCenterStorage.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 02/02/2023 - for the TousAntiCovid project.
//

import Foundation
import AppModel
import ExtensionsModule

public enum InfoCenterStorage {
    public static var updatedAfter: Int {
        get { updatedAfterInfoCenter }
        set { updatedAfterInfoCenter = newValue }
    }
    
    public static var didReceiveNewInfo: Bool {
        get { didReceiveNewInfoCenter }
        set { didReceiveNewInfoCenter = newValue }
    }

    public static var lastLanguageCode: String? {
        get { lastLanguageCodeInfoCenter }
        set { lastLanguageCodeInfoCenter = newValue }
    }
    
    public static func saveLocalTags(data: Data) throws {
        let localUrl: URL = try localTagsUrl()
        try data.write(to: localUrl)
    }
    
    public static func saveLocalInfoCenter(data: Data) throws {
        let localUrl: URL = try localInfoCenterUrl()
        try data.write(to: localUrl)
    }
    
    public static func saveLocalLabels(data: Data, languageCode: String) throws {
        let localUrl: URL = try localLabelsUrl(for: languageCode)
        try data.write(to: localUrl)
    }
    
    public static func loadLocalTags() throws -> Data {
        let localUrl: URL = try localTagsUrl()
        guard FileManager.default.fileExists(atPath: localUrl.path) else {
            throw TACError.fileMissing
        }
       return try Data(contentsOf: localUrl)
    }
    
    public static func loadLocalInfoCenter() throws -> Data {
        let localUrl: URL = try localInfoCenterUrl()
        guard FileManager.default.fileExists(atPath: localUrl.path) else {
            throw TACError.fileMissing
        }
        return try Data(contentsOf: localUrl)
    }
    
    public static func loadLocalLabels(for languageCode: String) throws -> Data {
        var localUrl: URL = try localLabelsUrl(for: languageCode)
        if !FileManager.default.fileExists(atPath: localUrl.path) {
            localUrl = try localLabelsUrl(for: AppContextDataProvider.defaultLanguage.rawValue)
        }
        return try Data(contentsOf: localUrl)
    }
    
    private static func localTagsUrl() throws -> URL {
        let directoryUrl: URL = try createWorkingDirectoryIfNeeded()
        return directoryUrl.appendingPathComponent("info-tags.json")
    }
    
    private static func localInfoCenterUrl() throws -> URL {
        let directoryUrl: URL = try createWorkingDirectoryIfNeeded()
        return directoryUrl.appendingPathComponent("info-center.json")
    }
    
    private static func localLabelsUrl(for languageCode: String) throws -> URL {
        let directoryUrl: URL = try createWorkingDirectoryIfNeeded()
        return directoryUrl.appendingPathComponent("info-labels-\(languageCode).json")
    }
    
    private static func createWorkingDirectoryIfNeeded() throws -> URL {
        let directoryUrl: URL = FileManager.libraryDirectory().appendingPathComponent("InfoCenter")
        if !FileManager.default.fileExists(atPath: directoryUrl.path, isDirectory: nil) {
            try FileManager.default.createDirectory(at: directoryUrl, withIntermediateDirectories: false, attributes: nil)
        }
        return directoryUrl
    }
}

private extension InfoCenterStorage {
    @UserDefault(key: .lastLanguageCodeInfoCenter) static var lastLanguageCodeInfoCenter: String?
    @UserDefault(key: .didReceiveNewInfoCenter) static var didReceiveNewInfoCenter: Bool = false
    @UserDefault(key: .updatedAfterInfoCenter) static var updatedAfterInfoCenter: Int = 0
}
