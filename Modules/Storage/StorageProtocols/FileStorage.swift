// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  FileStorage.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 27/02/2023 - for the TousAntiCovid project.
//

import ExtensionsModule
import AppModel
import Foundation

public protocol FileStorage {
    var directoryName: String { get }
    var fileName: String { get }
    var lastUpdatedDate: Date { get set }
    func saveLocalData(_ data: Data, for languageCode: String?) throws
    func loadLocalData(for languageCode: String?) throws -> Data
}

public extension FileStorage {
    func saveLocalData(_ data: Data, for languageCode: String?) throws {
        let localUrl: URL = try localUrl(for: languageCode)
        try data.write(to: localUrl)
    }

    func loadLocalData(for languageCode: String?) throws -> Data {
        let localUrl: URL = try localUrl(for: languageCode)
        guard FileManager.default.fileExists(atPath: localUrl.path) else {
            throw TACError.fileMissing
        }
        return try Data(contentsOf: localUrl)
    }
}

private extension FileStorage {
    func localUrl(for languageCode: String?) throws -> URL {
        let directoryUrl: URL = try createWorkingDirectoryIfNeeded()
        let lastPathComponent: String = [fileName, languageCode, ".json"]
            .compactMap { $0 }
            .joined(separator: "")
        return directoryUrl.appendingPathComponent(lastPathComponent)
    }

    func createWorkingDirectoryIfNeeded() throws -> URL {
        let directoryUrl: URL = FileManager.libraryDirectory().appendingPathComponent(directoryName)
        if !FileManager.default.fileExists(atPath: directoryUrl.path, isDirectory: nil) {
            try FileManager.default.createDirectory(at: directoryUrl, withIntermediateDirectories: false, attributes: nil)
        }
        return directoryUrl
    }
}
