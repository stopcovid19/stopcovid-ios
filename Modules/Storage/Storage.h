// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  Storage.h
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 25/01/2023 - for the TousAntiCovid project.
//

#import <Foundation/Foundation.h>

//! Project version number for Storage.
FOUNDATION_EXPORT double StorageVersionNumber;

//! Project version string for Storage.
FOUNDATION_EXPORT const unsigned char StorageVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <Storage/PublicHeader.h>


