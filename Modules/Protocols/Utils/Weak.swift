// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  Weak.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 17/02/2023 - for the TousAntiCovid project.
//

public struct Weak<T: AnyObject> {
    public var value: T? { _value }

    weak var _value: T?

    init(_ value: T) {
        self._value = value
    }
}
