// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  ObservableRepository.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 03/02/2023 - for the TousAntiCovid project.
//

import Foundation

public protocol ObservableRepository: AnyObject {
    var observers: [Weak<AnyObject>] { get set }
    func notifyObservers()
}
extension ObservableRepository {
    public func addObserver(_ observer: AnyObject) {
        observers.append(.init(observer))
    }

    public func removeObserver(_ observer: AnyObject) {
        // Value will already be nil cause this func must be called in deinit of instance
        // So we just need to clean the array
        observers.removeAll(where: { weakObserver in
            weakObserver.value == nil
        })
    }
}
