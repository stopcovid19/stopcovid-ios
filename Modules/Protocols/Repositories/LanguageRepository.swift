// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  LanguageRepository.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 20/02/2023 - for the TousAntiCovid project.
//

import AppModel

public protocol LanguageRepository: ObservableRepository {
    var defaultAppLanguage: Language { get }
    var currentAppLanguage: Language? { get set }
    var supportedLanguages: [Language] { get }
}
