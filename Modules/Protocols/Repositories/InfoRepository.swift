// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  InfoRepository.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 08/02/2023 - for the TousAntiCovid project.
//

import AppModel

public protocol InfoRepository: ObservableRepository {
    var objects: [Info] { get }
    var didReceiveNewInfo: Bool { get set }
    func fetchObjects(forceRefresh: Bool, _ completion: @escaping (Error?) -> ())
    func reloadLanguage(_ completion: @escaping (Error?) -> ())
}
