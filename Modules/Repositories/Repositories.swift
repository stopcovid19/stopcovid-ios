// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  Repositories.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 03/03/2023 - for the TousAntiCovid project.
//

import Protocols
import DependenciesInjection

public struct Repositories: DependenciesProvider {
    public init() {
        @Provides(InfoAppRepositoryImpl()) var infoRepository: InfoRepository?
        @Provides(MyHealthAppRepositoryImpl()) var myHealthRepository: MyHealthRepository?
        @Provides(LanguageAppRepositoryImpl()) var languageRepository: LanguageRepository?
    }
}
