// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  LanguageAppRepository.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 20/02/2023 - for the TousAntiCovid project.
//

import AppModel
import Protocols
import Storage

final class LanguageAppRepositoryImpl: LanguageRepository {

    typealias AppModel = Language

    var currentAppLanguage: AppModel? {
        get { Language(rawValue: AppContextDataProvider.currentAppLanguageCode.orEmpty) }
        set { AppContextDataProvider.currentAppLanguageCode = newValue?.rawValue }
    }

    let defaultAppLanguage: Language = .english
    let supportedLanguages: [AppModel] = Language.allCases

    var observers: [Weak<AnyObject>] = .init()

    init() {}

}

// MARK: - Observer -
extension LanguageAppRepositoryImpl {
    func notifyObservers() {
        observers.forEach { weakObserver in
            (weakObserver.value as? LanguageRepositoryObserver)?.currentAppLanguageDidUpdate()
        }
    }
}
