// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  Constant.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 01/03/2023 - for the TousAntiCovid project.
//

enum Constant {
    enum Files {
        case risks

        var fileName: String {
            switch self {
            case .risks:
                return "risks"
            }
        }
        var fileExtension: String {
            "json"
        }
    }
}
