// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  InfoAppRepository.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 12/01/2023 - for the TousAntiCovid project.
//

import Foundation
import AppModel
import Server
import Storage
import ExtensionsModule
import Protocols

final class InfoAppRepositoryImpl: InfoRepository {
    typealias AppModel = Info
    
    var objects: [AppModel] {
        restInfos
            .compactMap { try? toAppModel($0) }
            .filter { !$0.title.isEmpty && !$0.description.isEmpty && $0.date <= Date() }
    }
    
    var observers: [Weak<AnyObject>] = .init()
    
    var didReceiveNewInfo: Bool  {
        get { InfoCenterStorage.didReceiveNewInfo }
        set { InfoCenterStorage.didReceiveNewInfo = newValue }
    }
    
    private var restInfos: [RESTInfo] = []
    private var restTags: [RESTInfoTag] = []
    private var labelsDict: [String: String] = [:]
   
    private var lastUpdatedAt: Int {
        get { InfoCenterStorage.updatedAfter }
        set { InfoCenterStorage.updatedAfter = newValue }
    }

    private var lastLanguageCode: String? {
        get { InfoCenterStorage.lastLanguageCode }
        set { InfoCenterStorage.lastLanguageCode = newValue }
    }
    
    private var currentAppLanguageCode: String {
        get { AppContextDataProvider.currentAppLanguageCode ?? AppContextDataProvider.defaultLanguage.rawValue }
        set { AppContextDataProvider.currentAppLanguageCode = newValue }
    }
    
    init() {
        try? loadAllFiles()
    }
    
    func fetchObjects(forceRefresh: Bool, _ completion: @escaping (Error?) -> ()) {
        fetchAllFiles(force: forceRefresh, completion)
    }

    func reloadLanguage(_ completion: @escaping (Error?) -> ()) {
        let currentAppLanguageCode: String = self.currentAppLanguageCode
        try? loadLocalLabels(for: currentAppLanguageCode)
        notifyObservers()
        fetchLabelsFile(languageCode: currentAppLanguageCode) { [weak self] error in
            if error == nil {
                self?.notifyObservers()
            }
            completion(error)
        }
    }
}

// MARK: - Observer -
extension InfoAppRepositoryImpl {
    func notifyObservers() {
        observers.forEach { weakObserver in
            (weakObserver.value as? InfoRepositoryObserver)?.infoCenterDidUpdate()
        }
    }
}

// MARK: - AppModel -
private extension InfoAppRepositoryImpl {
    func toAppModel(_ restModel: RESTInfo) throws -> AppModel {
        guard let title = labelsDict[restModel.titleKey] else { throw TACError.malformedOrMissingData }
        guard let description = labelsDict[restModel.descriptionKey] else { throw TACError.malformedOrMissingData }
        var buttonLabel: String?
        if let buttonLabelKey = restModel.buttonLabelKey {
            buttonLabel = labelsDict[buttonLabelKey]
        }
        var url: URL?
        if let urlKey = restModel.urlKey, let stringUrl = labelsDict[urlKey] {
            url = URL(string: stringUrl)
        }
        return Info(title: title, description: description, buttonLabel: buttonLabel, url: url, date: Date(timeIntervalSince1970: Double(restModel.timestamp)), tags: tagsForIds(restModel.tagIds ?? []))
    }
    
    func toAppModel(_ restTag: RESTInfoTag) throws -> InfoTag {
        guard let label =  labelsDict[restTag.labelKey] else { throw TACError.malformedOrMissingData }
        return InfoTag(id: restTag.id, label: label, colorCode: restTag.colorCode)
    }

    func tagsForIds(_ ids: [String]) -> [InfoTag] {
        restTags
            .filter { ids.contains($0.id) }
            .compactMap { try? toAppModel($0) }
    }
}

// MARK: - All loading methods -
private extension InfoAppRepositoryImpl {
    func loadAllFiles() throws {
        try? loadLocalTags()
        try? loadLocalInfoCenter()
        try? loadLocalLabels(for: currentAppLanguageCode)
    }

    func loadLocalTags() throws {
        let data: Data = try InfoCenterStorage.loadLocalTags()
        let value = try JSONDecoder().decode([RESTInfoTag].self, from: data)
        restTags = value
    }
    
    func loadLocalInfoCenter() throws {
        let data: Data = try InfoCenterStorage.loadLocalInfoCenter()
        let value: [RESTInfo] = try JSONDecoder().decode([RESTInfo].self, from: data)
        restInfos = value
    }
    
    func loadLocalLabels(for languageCode: String) throws {
        let data: Data = try InfoCenterStorage.loadLocalLabels(for: languageCode)
        let value: [String: String] = try JSONDecoder().decode([String: String].self, from: data)
        labelsDict = value
    }
}

// MARK: - All fetching methods -
private extension InfoAppRepositoryImpl {
    func fetchAllFiles(force: Bool, _ completion: @escaping (Error?) -> ()) {
        if force { lastUpdatedAt = 0 }
        let languageChanged: Bool = self.lastLanguageCode != self.currentAppLanguageCode
        let lastUpdateWasMoreThan5MinsAgo: Bool = Date().timeIntervalSince1970 - Double(self.lastUpdatedAt) > 5.0 * 60.0
        guard lastUpdateWasMoreThan5MinsAgo || languageChanged else {
            completion(nil)
            return
        }

        let group: DispatchGroup = .init()
        var errors: [Error?] = []
        group.enter()
        fetchTagsFile { error in
            errors.append(error)
            group.leave()
        }
        group.enter()
        fetchInfoCenterFile { error in
            errors.append(error)
            group.leave()
        }
        group.enter()
        fetchLabelsFile(languageCode: currentAppLanguageCode) { error in
            errors.append(error)
            group.leave()
        }

        group.notify(queue: .main) { [weak self] in
            if let error = errors.compactMap({ $0 }).first {
                self?.notifyObservers()
                completion(error)
            } else {
                self?.lastUpdatedAt = Int(Date().timeIntervalSince1970)
                guard !force else {
                    self?.notifyObservers()
                    completion(nil)
                    return
                }
                self?.didReceiveNewInfo = true
                self?.notifyObservers()
                completion(nil)
            }
        }
    }
        
    func fetchTagsFile(_ completion: @escaping (Error?) -> ()) {
        InfoCenterServer.fetchTagsFile { [weak self] result in
            switch result {
            case let .success(tags):
                do {
                    let data: Data = try JSONEncoder().encode(tags)
                    self?.restTags = tags
                    try InfoCenterStorage.saveLocalTags(data: data)
                    completion(nil)
                } catch {
                    completion(error)
                }
            case let .failure(error):
                completion(error)
            }
        }
    }
    
    func fetchInfoCenterFile(_ completion: @escaping (Error?) -> ()) {
        InfoCenterServer.fetchInfoCenterFile { [weak self] result in
            switch result {
            case let .success(infos):
                do {
                    let data: Data = try JSONEncoder().encode(infos)
                    self?.restInfos = infos.sorted(by: \.timestamp, ascending: false)
                    try InfoCenterStorage.saveLocalInfoCenter(data: data)
                    completion(nil)
                } catch {
                    completion(error)
                }
            case let .failure(error):
                completion(error)
            }
        }
    }
    
    func fetchLabelsFile(languageCode: String, completion: @escaping (Error?) -> ()) {
        InfoCenterServer.fetchLabelsFile(languageCode: languageCode) { [weak self] result in
            switch result {
            case let .success(labelsDict):
                do {
                    let data: Data = try JSONEncoder().encode(labelsDict)
                    self?.labelsDict = labelsDict
                    try InfoCenterStorage.saveLocalLabels(data: data, languageCode: languageCode)
                    self?.lastLanguageCode = languageCode
                    completion(nil)
                } catch {
                    completion(error)
                }
            case let .failure(error):
                completion(error)
            }
        }
    }
}
