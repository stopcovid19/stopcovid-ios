// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  Repositories.h
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 12/01/2023 - for the TousAntiCovid project.
//

#import <Foundation/Foundation.h>

//! Project version number for Repositories.
FOUNDATION_EXPORT double RepositoriesVersionNumber;

//! Project version string for Repositories.
FOUNDATION_EXPORT const unsigned char RepositoriesVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <Repositories/PublicHeader.h>


