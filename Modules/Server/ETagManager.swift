// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  ETAGManager.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 02/02/2023 - for the TousAntiCovid project.
//


final class ETagManager {
    static let shared: ETagManager = .init()
    private init() {}
    
    @UserDefault(key: .eTags)
    private var eTags: [String: String] = [:]
    
    func eTag(for url: String) -> String? {
        eTags["\(url.hash)"]
    }

    func save(eTag: String, for url: String) {
        eTags["\(url.hash)"] = eTag
        self.eTags = eTags
    }
    
    func clearEtag(for url: String) {
        eTags.removeValue(forKey: "\(url.hash)")
    }
    
    func clearAllData() {
        eTags = [:]
    }
}
