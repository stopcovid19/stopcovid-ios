// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  URLResponse+Extension.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 02/02/2023 - for the TousAntiCovid project.
//

import Foundation

extension URLResponse {
    var eTag: String? { (self as? HTTPURLResponse)?.allHeaderFields[Constant.ETag.responseHeaderField] as? String }
    var isNotModified: Bool { responseStatusCode == 304 }
    
    private var responseStatusCode: Int? { (self as? HTTPURLResponse)?.statusCode }
}
