// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  RESTInfoTag.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 12/01/2023 - for the TousAntiCovid project.
//

public struct RESTInfoTag: Codable {
    public let id: String
    public let labelKey: String
    public let colorCode: String
}
