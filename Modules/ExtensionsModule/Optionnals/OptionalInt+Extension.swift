// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  OptionalInt+Extension.swift
//  ExtensionsModule
//
//  Created by Lunabee Studio / Date - 12/01/2023 - for the TousAntiCovid project.
//

import Foundation

public extension Optional where Wrapped == Int {
    var orZero: Int {
        switch self {
        case .some(let value):
            return value
        case .none:
            return 0
        }
    }

    var orMax: Int {
        switch self {
        case .some(let value):
            return value
        case .none:
            return .max
        }
    }
}
