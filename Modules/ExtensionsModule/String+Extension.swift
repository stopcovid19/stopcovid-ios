// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  String+Extension.swift
//  ExtensionsModule
//
//  Created by Lunabee Studio / Date - 12/01/2023 - for the TousAntiCovid project.
//

import Foundation

public extension String {
    var isValidEmailAddress: Bool {
        let emailRegex: String = "^[a-zA-Z0-9.+_-]{1,64}@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,125}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,125}[a-zA-Z0-9])?)+$"
        let emailPredicate: NSPredicate = NSPredicate(format: "SELF MATCHES %@", emailRegex)
        return emailPredicate.evaluate(with: self)
    }

    var hasLowerCasedLetter: Bool {
        let regex: String = ".*[a-z]+.*"
        return NSPredicate(format: "SELF MATCHES %@", regex).evaluate(with: self)
    }

    var hasUpperCasedLetter: Bool {
        let regex: String = ".*[A-Z]+.*"
        return NSPredicate(format: "SELF MATCHES %@", regex).evaluate(with: self)
    }

    var hasNumber: Bool {
        let regex: String = ".*[0-9]+.*"
        return NSPredicate(format: "SELF MATCHES %@", regex).evaluate(with: self)
    }

    var hasSpecialCharacter: Bool {
        // Special characters: " !"#$%&'()*+,-./:;<=>?@[\]^_`{|}~"
        let regex: String = ".*[-\\^\\=$*.\\[\\]{}()?\"!@+#%&/\\\\,><':;|_~`]+.*"
        return NSPredicate(format: "SELF MATCHES %@", regex).evaluate(with: self)
    }

    func hasRequiredLenght(min: Int, max: Int) -> Bool {
        (min...max).contains(self.count)
    }

    var trimmed: String {
        trimmingCharacters(in: .whitespacesAndNewlines)
    }
}
