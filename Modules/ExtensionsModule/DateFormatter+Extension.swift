// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  DateFormatter+Extension.swift
//  ExtensionsModule
//
//  Created by Lunabee Studio / Date - 12/01/2023 - for the TousAntiCovid project.
//

import Foundation

public extension DateFormatter {
    static let mediumDateFormatter: DateFormatter = {
        let df: DateFormatter = .init()
        df.dateStyle = .medium
        df.timeStyle = .none
        return df
    }()

    static let mediumTimeFormatter: DateFormatter = {
        let df: DateFormatter = .init()
        df.dateStyle = .none
        df.timeStyle = .medium
        return df
    }()

    static let mediumDateTimeFormatter: DateFormatter = {
        let df: DateFormatter = .init()
        df.dateStyle = .medium
        df.timeStyle = .medium
        return df
    }()

    static let serverDateTimeFormatter: ISO8601DateFormatter = {
        let df: ISO8601DateFormatter = .init()
        df.formatOptions = [.withInternetDateTime, .withFractionalSeconds]
        return df
    }()
}
