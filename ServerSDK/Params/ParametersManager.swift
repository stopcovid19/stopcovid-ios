// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  ParametersManager.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 14/05/2020 - for the TousAntiCovid project.
//


import UIKit

public final class ParametersManager: NSObject {
    
    public typealias RequestCompletion = (_ result: Result<(Double), Error>) -> ()
    public typealias TaskLoggingHandler = (_ task: URLSessionTask?, _ responseData: Data?, _ error: Error?) -> ()
    
    public static let shared: ParametersManager = .init()
    private override init() { super.init() }

    var url: URL!
    var certificateFiles: [Data]!

    public var displayHomeUsefulLinks: Bool { valueFor(name: "app.displayHomeUsefulLinks") as? Bool ?? false }
    public var displayHomeVaccination: Bool { valueFor(name: "app.displayHomeVaccination") as? Bool ?? false }
    public var displayInTheSpotlight: Bool { valueFor(name: "app.displayInTheSpotlight") as? Bool ?? false }

    public var minFilesRefreshInterval: Double { valueFor(name: "app.minFilesRefreshInterval") as? Double ?? 60.0 }
    public var updateFilesAfterDays: Int { valueFor(name: "app.updateFilesAfterDays") as? Int ?? 30 }

    public var displaySanitaryCertificatesWallet: Bool { valueFor(name: "app.displaySanitaryCertificatesWallet") as? Bool ?? false }
    public var walletTestCertificateValidityThresholds: [Int] { valueFor(name: "app.wallet.testCertificateValidityThresholds") as? [Int] ?? [48, 72] }
    public var activityPassGenerationServerPublicKey: String? { valueFor(name: "app.activityPass.generationServerPublicKey") as? String }
    public var activityPassSkipNegTestHours: Int { valueFor(name: "app.activityPass.skipNegTestHours") as? Int ?? 48}

    public var ratingsKeyFiguresOpeningThreshold: Int { valueFor(name: "app.ratingsKeyFiguresOpeningThreshold") as? Int ?? 10 }
    public var displayVaccination: Bool { valueFor(name: "app.displayVaccination") as? Bool ?? false }

    public var displayHomeKeyFigures: Bool { valueFor(name: "app.displayHomeKeyFigures") as? Bool ?? false }
    public var displayHomeRecommendations: Bool { valueFor(name: "app.displayHomeRecommendations") as? Bool ?? false }

    public var maxCertBeforeWarning: Int { valueFor(name: "app.wallet.maxCertBeforeWarning") as? Int ?? 15 }

    public var shouldDisplayUrgentDgs: Bool { valueFor(name: "app.displayUrgentDgs") as? Bool ?? false }

    var appAvailability: Bool? { valueFor(name: "app.appAvailability") as? Bool }

    public var displayDepartmentLevel: Bool { valueFor(name: "app.keyfigures.displayDepartmentLevel") as? Bool ?? false }

    public var dccKids: DccKids? {
        guard let value = valueFor(name: "app.wallet.dccKids") as? [String: Any] else {
            return nil
        }
        do {
            let jsonData: Data = try JSONSerialization.data(withJSONObject: value)
            return try JSONDecoder().decode(DccKids.self, from: jsonData)
        } catch {
            return nil
        }
    }
    
    public var confettiBirthRate: Double { valueFor(name: "app.wallet.confettiBirthRate") as? Double ?? 10.0 }

    public var homeNotification: HomeNotification? {
        guard let value = valueFor(name: "app.notif") as? [String: Any] else {
            return nil
        }
        do {
            let jsonData: Data = try JSONSerialization.data(withJSONObject: value)
            return try JSONDecoder().decode(HomeNotification.self, from: jsonData)
        } catch {
            return nil
        }
    }
    
    public var defaultInitialKeyFiguresSelectionKeys: [String]? {
        guard let value = valueFor(name: "app.keyfigures.compare") as? [[String: String]] else {
            return nil
        }
        guard let keys = value.first, let key1 = keys["k1"], let key2 = keys["k2"] else { return nil }
        return ["keyfigure." + key1, "keyfigure." + key2]
    }
    
    public var predefinedKeyFiguresSelection: [KeyfiguresCombination]? {
        guard let value = valueFor(name: "app.keyfigures.compare") as? [[String: String]] else {
            return nil
        }
        do {
            let jsonData: Data = try JSONSerialization.data(withJSONObject: value)
            return try JSONDecoder().decode([KeyfiguresCombination].self, from: jsonData)
        } catch {
            return nil
        }
    }
    
    public var keyFiguresComparisonColors: [UIColor]? {
        guard let value = valueFor(name: "app.keyfigures.compareColors") as? [[String: String]] else {
            return nil
        }
        return value.compactMap { color in
            guard let lightColorCode: String = color["l"], let darkColorCode: String = color["d"] else { return nil }
            let lightColor: UIColor = UIColor(hexString: lightColorCode)
            let darkColor: UIColor = UIColor(hexString: darkColorCode)
            if #available(iOS 13.0, *) {
                return UIColor { trait in
                    trait.userInterfaceStyle == .dark ? darkColor : lightColor
                }
            } else {
                return lightColor
            }
        }
    }
    
    public var displayKeyFiguresMap: Bool { valueFor(name: "app.displayKeyFiguresFrMap") as? Bool ?? false }

    // Blacklist
    public var blacklistFreqInSec: Double {
        return (valueFor(name: "app.blacklistFreqInMinutes") as? Double ?? 24 * 60) * 60
    }
    
    // SmartWallet
    public var smartWalletEngine: SmartWalletEngine {
        guard let value = valueFor(name: "app.smartwallet.engine") as? [String: Any] else {
            return SmartWalletEngine()
        }
        do {
            let jsonData: Data = try JSONSerialization.data(withJSONObject: value)
            return try JSONDecoder().decode(SmartWalletEngine.self, from: jsonData)
        } catch {
            return SmartWalletEngine()
        }
    }
    public var vaccinTypes: Vaccins? {
        guard let value = valueFor(name: "app.smartwallet.vacc") as? [String: Any] else {
            return nil
        }
        do {
            let jsonData: Data = try JSONSerialization.data(withJSONObject: value)
            return try JSONDecoder().decode(Vaccins.self, from: jsonData)
        } catch {
            return nil
        }
    }
    public var smartWalletFeatureActivated: Bool { valueFor(name: "app.isSmartwalletOn") as? Bool ?? false }
    public var smartWalletNotificationsActivated: Bool { valueFor(name: "app.smartwallet.notif") as? Bool ?? false }
    public var smartWalletNotificationsElgDays: Int { valueFor(name: "app.smartwallet.notifElgDays") as? Int ?? 20 }

    private var config: [[String: Any]] = []
    
    public var isMultiPassActivated: Bool { valueFor(name: "app.displayMultipass") as? Bool ?? true }
    public var multiPassConfiguration: MultiPassConfiguration {
        guard let value = valueFor(name: "app.multipass.list") as? [String: Any] else { return MultiPassConfiguration() }
        do {
            let jsonData: Data = try JSONSerialization.data(withJSONObject: value)
            return try JSONDecoder().decode(MultiPassConfiguration.self, from: jsonData)
        } catch {
            return MultiPassConfiguration()
        }
    }

    private var receivedData: [String: Data] = [:]
    private var completions: [String: RequestCompletion] = [:]
    private var taskLoggingHandler: TaskLoggingHandler = { _, _, _ in}
    
    private lazy var session: URLSession = {
        let backgroundConfiguration: URLSessionConfiguration = URLSessionConfiguration.background(withIdentifier: "fr.gouv.tousanticovid.ios.ServerSDK-Config")
        backgroundConfiguration.timeoutIntervalForRequest = ServerConstant.timeout
        backgroundConfiguration.timeoutIntervalForResource = ServerConstant.timeout
        backgroundConfiguration.httpShouldUsePipelining = true
        return URLSession(configuration: backgroundConfiguration, delegate: self, delegateQueue: .main)
    }()
    
    public func start(configUrl: URL, configCertificateFiles: [Data], taskLoggingHandler: @escaping TaskLoggingHandler) {
        url = configUrl
        certificateFiles = configCertificateFiles
        self.taskLoggingHandler = taskLoggingHandler
        writeInitialFileIfNeeded()
        loadLocalConfig()
    }
    
    public func walletPublicKey(authority: String, certificateId: String) -> String? {
        guard let allPubKeys = valueFor(name: "app.walletPubKeys") as? [[String: Any]] else { return nil }
        guard let authPubKeysDict = allPubKeys.first(where: { $0["auth"] as? String == authority }) else { return nil }
        guard let authPubKeys = authPubKeysDict["pubKeys"] as? [String: String] else { return nil }
        return authPubKeys[certificateId]
    }
    
    public func fetchConfig(completion: @escaping RequestCompletion) {
        let requestId: String = UUID().uuidString
        completions[requestId] = completion
        var request: URLRequest = URLRequest(url: url)
        let eTag: String? = SVETagManager.shared.eTag(for: url.absoluteString)
        eTag.map { request.addValue($0, forHTTPHeaderField: ServerConstant.Etag.requestHeaderField) }
        let task: URLSessionDataTask = session.dataTask(with: request)
        task.taskDescription = requestId
        task.resume()
    }
    
    private func loadLocalConfig() {
        guard let data = try? Data(contentsOf: localFileUrl()) else { return }
        guard let json = (try? JSONSerialization.jsonObject(with: data, options: [])) as? [String: Any] else { return }
        self.config = json["config"] as? [[String: Any]] ?? []
    }
    
    private func valueFor(name: String) -> Any? {
        config.first { $0["name"] as? String == name }?["value"]
    }

    private func writeInitialFileIfNeeded() {
        let fileUrl: URL = Bundle(for: ParametersManager.self).url(forResource: url.deletingPathExtension().lastPathComponent, withExtension: url.pathExtension)!
        let destinationFileUrl: URL = createWorkingDirectoryIfNeeded().appendingPathComponent("config.json")
        if !FileManager.default.fileExists(atPath: destinationFileUrl.path) {
            try? FileManager.default.removeItem(at: destinationFileUrl)
            try? FileManager.default.copyItem(at: fileUrl, to: destinationFileUrl)
        }
    }

    private func createWorkingDirectoryIfNeeded() -> URL {
        let directoryUrl: URL = FileManager.svLibraryDirectory().appendingPathComponent("config")
        if !FileManager.default.fileExists(atPath: directoryUrl.path, isDirectory: nil) {
            try? FileManager.default.createDirectory(at: directoryUrl, withIntermediateDirectories: false, attributes: nil)
        }
        return directoryUrl
    }

    private func localFileUrl() -> URL {
        createWorkingDirectoryIfNeeded().appendingPathComponent("config.json")
    }

}

extension ParametersManager: URLSessionDelegate, URLSessionDataDelegate {
    
    public func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive data: Data) {
        let requestId: String = dataTask.taskDescription ?? ""
        guard let completion = completions[requestId] else { return }
        DispatchQueue.main.async {
            if dataTask.response?.svIsError == true {
                let statusCode: Int = dataTask.response?.svStatusCode ?? 0
                let message: String = data.isEmpty ? "No logs received from the server" : (String(data: data, encoding: .utf8) ?? "Unknown error")
                let error: Error = NSError.svLocalizedError(message: "Unknown error (\(statusCode)). (\(message))", code: statusCode)
                self.taskLoggingHandler(dataTask, nil, error)
                completion(.failure(error))
                self.completions[dataTask.taskDescription ?? ""] = nil
            } else {
                var receivedData: Data = self.receivedData[requestId] ?? Data()
                receivedData.append(data)
                self.taskLoggingHandler(dataTask, receivedData, nil)
                self.receivedData[requestId] = receivedData
            }
        }
    }

    public func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        let requestId: String = task.taskDescription ?? ""
        guard let completion = completions[requestId] else { return }
        DispatchQueue.main.async {
            if let error = error {
                self.taskLoggingHandler(task, nil, error)
                completion(.failure(error))
            } else if task.response?.svIsNotModified == true {
                // Response not updated since last fetch (ETag feature)
                self.taskLoggingHandler(task, nil, nil)
                completion(.success((task.response!.serverTime)))
            } else {
                let receivedData: Data = self.receivedData[requestId] ?? Data()
                if task.response!.svIsError == true {
                    let statusCode: Int = task.response?.svStatusCode ?? 0
                    let message: String = receivedData.isEmpty ? "No data received from the server" : (String(data: receivedData, encoding: .utf8) ?? "Unknown error" )
                    let error: Error = NSError.svLocalizedError(message: "Unknown error (\(statusCode)). (\(message))", code: statusCode)
                    self.taskLoggingHandler(task, nil, error)
                    completion(.failure(error))
                    self.completions[task.taskDescription ?? ""] = nil
                } else {
                    do {
                        let json: [String: Any] = (try JSONSerialization.jsonObject(with: receivedData, options: [])) as? [String: Any] ?? [:]
                        guard !json.isEmpty else {
                            DispatchQueue.main.async {
                                let error: Error = NSError.svLocalizedError(message: "Malformed json config. Using the last known version instead.", code: 0)
                                self.taskLoggingHandler(task, nil, error)
                                completion(.failure(error))
                            }
                            return
                        }
                        try receivedData.write(to: self.localFileUrl())
                        self.config = json["config"] as? [[String: Any]] ?? []
                        // Save eTag if present
                        if let eTag = task.response?.svETag, let url = task.currentRequest?.url?.absoluteString {
                            // We don't need to persiste received data in ETagsManager
                            SVETagManager.shared.save(eTag: eTag, data: Data(), for: url)
                        }
                        self.taskLoggingHandler(task, receivedData, nil)
                        DispatchQueue.main.async {
                            completion(.success((task.response!.serverTime)))
                        }
                    } catch {
                        self.taskLoggingHandler(task, nil, error)
                        DispatchQueue.main.async {
                            completion(.failure(error))
                        }
                    }
                }
            }
            self.completions[task.taskDescription ?? ""] = nil
        }
    }
    
    public func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        CertificatePinning.validateChallenge(challenge, certificateFiles: certificateFiles) { validated, credential in
            validated ? completionHandler(.useCredential, credential) : completionHandler(.cancelAuthenticationChallenge, nil)
        }
    }
    
}
