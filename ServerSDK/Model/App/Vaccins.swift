// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  Vaccins.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 11/02/2022 - for the TousAntiCovid project.
//

import Foundation

public struct Vaccins: Decodable {
    public var arnm: [String]
    public var janssen: [String]
    public var astraZeneca: [String]

    enum CodingKeys: String, CodingKey {
        case arnm = "ar"
        case janssen = "ja"
        case astraZeneca = "az"
    }
}
